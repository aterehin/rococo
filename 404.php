<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Nobrand
 */

get_header();?>

<hr class="delimiter"/>
<div class="container">
	<div class="error">
		<div class="error__header">
			<strong class="error__number">404</strong>
			<div class="error__desc"><?php printf( esc_html__( 'Page %s not found. %s', 'rococo' ), '<span class="different-text">', '</span>' )  ?></div>
		</div>
		<div class="error__text">
			<?php echo wp_kses_data( get_theme_mod( 'messages=>404-text', sprintf( esc_html__( 'It looks like nothing was found at this location.%s Maybe try a search?', 'rococo' ), '</br>' ) ) ) ?>
		</div>
		<?php if ( get_theme_mod( 'messages=>404-search', true ) ) : ?>
			<hr class="_in-404"/>
			<div class="error__search">
				<?php get_search_form() ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
