<?php
/**
 * The template for displaying the footer.
 *
 * @package Nobrand
 */

?>
<footer class="footer">
	<?php if ( is_active_sidebar( 'footer' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' ) ) : ?>
		<div class="footer__widget-area">
			<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer' ) ) : ?>
						<div class="col-md-<?php echo esc_attr( 12 / get_theme_mod( 'footer=>grid', 3 ) ) ?> col-sm-6">
							<?php dynamic_sidebar( 'footer' ); ?>
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="col-md-<?php echo esc_attr( 12 / get_theme_mod( 'footer=>grid', 3 ) ) ?> col-sm-6">
							<?php dynamic_sidebar( 'footer-2' ); ?>
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="col-md-<?php echo esc_attr( 12 / get_theme_mod( 'footer=>grid', 3 ) ) ?> col-sm-6">
							<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<div class="col-md-<?php echo esc_attr( 12 / get_theme_mod( 'footer=>grid', 3 ) ) ?> col-sm-6">
							<?php dynamic_sidebar( 'footer-4' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ( get_theme_mod( 'footer=>social-bar', true ) ) : ?>
		<div class="footer__social-bar" data-js="bottom-panel">
			<ul class="social-list _in-footer-social-bar">
				<?php if ( get_theme_mod( 'footer=>social-bar-facebook', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://www.facebook.com/' . get_theme_mod( 'footer=>social-bar-facebook', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-facebook"></i><span>facebook</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-twitter', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://twitter.com/' . get_theme_mod( 'footer=>social-bar-twitter', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-twitter"></i><span>twitter</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-instagram', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://instagram.com/' . get_theme_mod( 'footer=>social-bar-instagram', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-instagram"></i><span>instagram</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-pinterest', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://pinterest.com/' . get_theme_mod( 'footer=>social-bar-pinterest', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-pinterest"></i><span>pinterest</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-google', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://plus.google.com/' . get_theme_mod( 'footer=>social-bar-google', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-google-plus"></i><span>google+</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-youtube', false ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://youtube.com/' . get_theme_mod( 'footer=>social-bar-youtube', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-youtube"></i><span>youtube</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-tumblr', false ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( 'http://' . get_theme_mod( 'footer=>social-bar-tumblr', 'nobrand' ) . '.tumblr.com/' ) ?>"
						   target="_blank"><i class="fa fa-tumblr"></i><span>tumblr</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-linkedin', false ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( get_theme_mod( 'footer=>social-bar-linkedin', 'nobrand' ) ) ?>"
						   target="_blank"><i class="fa fa-linkedin"></i><span>linkedin</span></a>
					</li>
				<?php endif; ?>

				<?php if ( get_theme_mod( 'footer=>social-bar-rss', true ) ) : ?>
					<li class="social-list__item">
						<a href="<?php echo esc_url( get_theme_mod( 'footer=>social-bar-rss', '' ) ) ?>"
						   target="_blank"><i class="fa fa-rss"></i><span>rss</span></a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	<?php endif; ?>

	<div class="footer__instagram" data-js="bottom-panel">
		<?php if ( is_active_sidebar( 'footer_instagram' ) ) : ?>
			<?php dynamic_sidebar( 'footer_instagram' ); ?>
		<?php endif; ?>
	</div>
	<div class="footer__bottom-panel">
		<div class="container">
			<?php if ( get_theme_mod( 'footer=>left-copyright', true ) ) :
				echo '<p class="copyright">' . wp_kses_data( get_theme_mod( 'footer=>left-copyright', sprintf( __( '%s %d All Rights Reserved.', 'rococo' ), '&#169;', date( 'Y' ) ) ) ) . '</p>';
			endif; ?>
			<?php if ( get_theme_mod( 'footer=>right-copyright', true ) ) :
				echo '<p class="designed">' . wp_kses_data( get_theme_mod( 'footer=>right-copyright', "Designed by <a href='http://www.nobrand.team/'>Nobrand</a>" ) ) . '</p>';
			endif; ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
