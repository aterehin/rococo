<?php
/**
 * The template for displaying image attachments
 *
 * @package Nobrand
 * @version 1.1.0
 */

get_header(); ?>

<div class="container">
	<div <?php if ( get_theme_mod( 'layout=>single-sidebar', true ) ) : ?> class="content" <?php else : ?> class="content _full-width" <?php endif; ?>>
		<div id="main" class="content__primary">
			<?php
			while ( have_posts() ) : the_post(); ?>

				<article class="post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="post__header">
						<h1 class="post__title"><?php the_title() ?></h1>
					</div>

					<div class="post__content">
						<?php echo wp_get_attachment_image( get_the_ID(), 'base_thumb' ); ?>

						<?php if ( has_excerpt() ) : ?>
							<div class="post__caption">
								<?php the_excerpt(); ?>
							</div>
						<?php endif; ?>
					</div>

					<?php if ( get_theme_mod( 'post->socials=>enable', true ) ) : ?>
						<div class="post__share">
							<ul class="social-list _in-post">
								<?php if ( get_theme_mod( 'post->socials=>facebook-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'http://www.facebook.com/share.php?u=' . get_the_permalink() . '&title=' . get_the_title() ) ?>"
										   target="_blank"><i class="fa fa-facebook"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>twitter-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'http://twitter.com/home?status=' . get_the_title() . '+' . get_the_permalink() ) ?>"
										   target="_blank"><i class="fa fa-twitter"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>pinterest-link', true ) ) : ?>
									<li class="social-list__item">
										<?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) );
										$thumbnail_utl   = $thumbnail['0']; ?>
										<a href="<?php echo esc_url( 'http://pinterest.com/pin/create/bookmarklet/?media=' . $thumbnail_utl . '&url=' . get_the_permalink() . '&is_video=false&description=' . get_the_title() ) ?>"
										   target="_blank"><i class="fa fa-pinterest"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>google-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'https://plus.google.com/share?url=' . get_the_permalink() ) ?>"
										   target="_blank"><i class="fa fa-google-plus"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>tumblr-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'http://www.tumblr.com/share?v=3&u=' . get_the_permalink() . '&t=' . get_the_title() ) ?>"
										   target="_blank"><i class="fa fa-tumblr"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>linkedin-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'http://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() . '&title=' . get_the_title() . '&source=' . home_url( '/' ) ) ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( get_theme_mod( 'post->socials=>stumbleupon-link', true ) ) : ?>
									<li class="social-list__item">
										<a href="<?php echo esc_url( 'http://www.stumbleupon.com/submit?url=' . get_the_permalink() . '&title=' . get_the_title() ) ?>"
										   target="_blank"><i class="fa fa-stumbleupon"></i></a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>

					<?php if ( is_single() && get_theme_mod( 'post->parts=>author-bio', true ) && get_the_author_meta( 'description' ) !== '' ) :
						get_template_part( 'templates/author-bio' );
					endif; ?>
				</article>

			<?php endwhile; ?>
			<?php if ( get_theme_mod( 'post->parts=>post-nav', true ) ) : ?>
				<div class="image-navigation">
					<div class="nav-links">
						<div class="nav-previous">
							<?php previous_image_link( false, sprintf( esc_html__( '%s Prev Image', 'rococo' ), '<i class="fa fa-angle-double-left"></i>' ) ) ?>
						</div>
						<div class="nav-next">
							<?php next_image_link( false, sprintf( esc_html__( 'Next Image %s', 'rococo' ), '<i class="fa fa-angle-double-right"></i>' ) ); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( get_theme_mod( 'post->parts=>related', true ) ) :
				rococo_related_post();
			endif;
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif; ?>
		</div>
		<?php if ( get_theme_mod( 'layout=>single-sidebar', true ) ) : ?>
			<div class="sidebar"><?php get_sidebar(); ?></div>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
