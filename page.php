<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Nobrand
 */

get_header(); ?>
<hr class="delimiter">
<div class="container">
	<div class="content">
		<div id="main" class="content__primary">
			<?php if ( have_posts() ) :
				while ( have_posts() ) {
					the_post();
					get_template_part( 'templates/content', 'page' );
				}
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			endif; ?>
		</div>
		<div class="sidebar"><?php get_sidebar(); ?></div>
	</div>
</div>
<?php get_footer(); ?>
