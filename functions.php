<?php
/**
 * Nobrand functions and definitions
 *
 * @package Nobrand
 * @version 1.1.0
 */

define( 'ROCOCO_VERSION', 1.1 );

if ( ! isset( $content_width ) ) {
	$content_width = 750;
}

remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
 * Remove recent comments style
 */
function rococo_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array(
		$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
		'recent_comments_style',
	) );
}
add_action( 'widgets_init', 'rococo_recent_comments_style' );

if ( ! function_exists( 'rococo_setup' ) ) :
	/**
	 * Rococo setup
	 */
	function rococo_setup() {
		load_theme_textdomain( 'rococo', get_template_directory() . '/languages' );
		load_theme_textdomain( 'tgm', get_template_directory() . '/inc/tgm/languages' );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		register_nav_menu( 'main-menu', esc_html__( 'Primary menu', 'rococo' ) );

		/**
		 * Add post type
		 */
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio', 'quote' ) );

		/**
		 * Add post thumbnail support
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add image size
		 */
		add_image_size( 'rococo_base_thumb', 1080, 700, true );  // Slider post format.
		add_image_size( 'rococo_medium_thumb', 360, 250, true ); // Grid post featured.
		add_image_size( 'rococo_small_thumb', 100, 83, true );   // Widget post format.

		/**
		 * Add feed links
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Add title tag
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Add custom header
		 */
		$custom_header_args = array(
			'flex-width'  => true,
			'width'       => 1920,
			'flex-height' => true,
			'height'      => 400,
		);
		add_theme_support( 'custom-header', $custom_header_args );
	}
endif;
add_action( 'after_setup_theme', 'rococo_setup' );

if ( ! function_exists( 'rococo_load_scripts' ) ) :
	/**
	 * Register and enqueue styles/scripts
	 */
	function rococo_load_scripts() {
		/**
		 * Load styles
		 */
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), ROCOCO_VERSION );
		wp_enqueue_style( 'owl-styles', get_template_directory_uri() . '/css/owl.carousel.css', array(), ROCOCO_VERSION );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), ROCOCO_VERSION );
		wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), ROCOCO_VERSION );

		/**
		 * Load custom fonts
		 */
		wp_enqueue_style( 'rococo-fonts', rococo_fonts_url(), array(), null );

		/**
		 * Load scripts
		 */
		wp_enqueue_script( 'skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), ROCOCO_VERSION, true );
		wp_enqueue_script( 'owl-script', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), ROCOCO_VERSION, true );
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array( 'jquery' ), ROCOCO_VERSION, true );
		wp_enqueue_script( 'gridrotator', get_template_directory_uri() . '/js/jquery.gridrotator.js', array( 'modernizr' ), ROCOCO_VERSION, true );
		wp_enqueue_script( 'application', get_template_directory_uri() . '/js/application.js', array( 'jquery' ), ROCOCO_VERSION, true );
		wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array( 'application' ), ROCOCO_VERSION, true );

		$sticky_menu_value = get_theme_mod( 'layout=>sticky-menu', true ) ? true : false;
		wp_localize_script( 'main', 'localize_array', array(
			'sticky_menu' => $sticky_menu_value,
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		) );

		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
endif;
add_action( 'wp_enqueue_scripts', 'rococo_load_scripts' );

if ( ! function_exists( 'rococo_fonts_url' ) ) :
	/**
	 * Register Google fonts for Rococo.
	 */
	function rococo_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';
		/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== esc_html_x( 'on', 'Merriweather font: on or off', 'rococo' ) ) {
			$fonts[] = 'Merriweather:400,700,400italic';
		}
		/* translators: If there are characters in your language that are not supported by Lato, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== esc_html_x( 'on', 'Lato font: on or off', 'rococo' ) ) {
			$fonts[] = 'Lato:400,400italic,700';
		}
		if ( $fonts ) {
			$fonts_url = add_query_arg( array(
				'family' => urlencode( implode( '|', $fonts ) ),
				'subset' => urlencode( $subsets ),
			), 'https://fonts.googleapis.com/css' );
		}
		return $fonts_url;
	}
endif;

/**
 * Add media support
 */
function rococo_enqueue_media() {
	wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'rococo_enqueue_media' );

if ( ! function_exists( 'rococo_register_sidebars' ) ) :
	/**
	 * Register sidebars
	 */
	function rococo_register_sidebars() {
		register_sidebar(
			array(
				'id'            => 'main_sidebar',
				'name'          => esc_html__( 'Right Sidebar', 'rococo' ),
				'description'   => esc_html__( 'Drag widget', 'rococo' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget__title">',
				'after_title'   => '</h3>',
			)
		);
		$col = get_theme_mod( 'footer=>grid', 3 );
		$args = array(
			'id'            => 'footer',
			'name'          => esc_html__( 'Footer Widgets Area %d', 'rococo' ),
			'description'   => esc_html__( 'Settings "Footer Widget Area grid"', 'rococo' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget__title">',
			'after_title'   => '</h3>',
		);
		register_sidebars( $col, $args );
		register_sidebar(
			array(
				'id'            => 'footer_instagram',
				'name'          => esc_html__( 'Instagram Feed *', 'rococo' ),
				'description'   => esc_html__( 'This area only for Instagram widget marked by "*"', 'rococo' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget__title">',
				'after_title'   => '</h3>',
			)
		);
	}
endif;
add_action( 'widgets_init', 'rococo_register_sidebars' );

/**
 * Add user social link
 */
function rococo_user_social_links( $contactmethods ) {
	$contactmethods['facebook']  = esc_html__( 'Facebook Username', 'rococo' );
	$contactmethods['twitter']   = esc_html__( 'Twitter Username', 'rococo' );
	$contactmethods['instagram'] = esc_html__( 'Instagram Username', 'rococo' );
	$contactmethods['pinterest'] = esc_html__( 'Pinterest Username', 'rococo' );
	$contactmethods['google']    = esc_html__( 'Google+ Account ID', 'rococo' );
	$contactmethods['tumblr']    = esc_html__( 'Tumblr Username', 'rococo' );
	$contactmethods['linkedin']  = esc_html__( 'LinkedIn Public Profile URL', 'rococo' );

	return $contactmethods;
}
add_filter( 'user_contactmethods', 'rococo_user_social_links' );

if ( ! function_exists( 'rococo_custom_excerpt_length' ) ) :
	/**
	 * Modify excerpt length
	 */
	function rococo_custom_excerpt_length() {
		return 100;
	}
endif;
add_filter( 'excerpt_length', 'rococo_custom_excerpt_length', 999 );

if ( ! function_exists( 'rococo_modify_read_more_link' ) ) :
	/**
	 * Modify more tag
	 */
	function rococo_modify_read_more_link() {
		return ' <a class="more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more...', 'rococo' ) . '<span class="screen-reader-text">' . get_the_title() . '</span></a>';
	}
endif;
add_filter( 'the_content_more_link', 'rococo_modify_read_more_link' );

/**
 * Check have post
 */
function rococo_more_posts() {
	global $wp_query;

	return $wp_query->current_post + 1 < $wp_query->post_count;
}

/**
 * Check have content
 */
function rococo_have_content( $str ) {
	$str = trim( str_replace( '&nbsp;', '', $str ) );
	return $str ? true : false;
}

/**
 * Retrieves the attachment ID from the file URL
 */
function rococo_get_image_id( $image_url ) {
	global $wpdb;
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );

	return $attachment[0];
}

/**
 * Move Comment Field to Bottom
 */
function rococo_wp_comment( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	return $fields;
}
add_filter( 'comment_form_fields', 'rococo_wp_comment' );

/**
 * Get logo image width.
 */
function rococo_logo_image_width( $url, $isRetina = false ) {
    $uri = wp_make_link_relative( $url );
    $path = realpath( $_SERVER["DOCUMENT_ROOT"] . $uri );
    $size = getimagesize( $path );

    return $isRetina ? $size[0] / 2 : $size[0];
}

/**
 * Get logo image.
 */
function rococo_logo_image() {
	$alt = get_bloginfo( 'name' );
	$images = array(
		'light_retina' => array(
			'retina' => true,
			'class' => 'light-logo retina',
			'url' => get_theme_mod( 'header=>mobile-light', get_template_directory_uri() . '/images/bg/logo-light@2x.png' ),
		),
		'light' => array(
			'retina' => false,
			'class' => 'light-logo',
			'url' => get_theme_mod( 'header=>desktop-light', get_template_directory_uri() . '/images/bg/logo-light.png' ),
		),
		'dark_retina' => array(
			'retina' => true,
			'class' => 'dark-logo retina',
			'url' => get_theme_mod( 'header=>mobile', get_template_directory_uri() . '/images/bg/logo@2x.png' ),
		),
		'dark' => array(
			'retina' => false,
			'class' => 'dark-logo',
			'url' => get_theme_mod( 'header=>desktop', get_template_directory_uri() . '/images/bg/logo.png' ),
		),
	);

	foreach ( $images as $img ) {
		if ( $img['url'] ) {
			echo '<img width="' . esc_attr( rococo_logo_image_width( $img['url'], $img['retina'] ) ) . '" class="' . esc_attr( $img['class'] ) . '" src="' . esc_url( $img['url'] ) . '" alt="' . esc_attr( $alt ) . '"/>';
		}
	}
}

/**
 * Terms ajax search in customizer
 */
function rococo_term_ajax_search() {
	check_ajax_referer( 'nonce', 'ajax_nonce' );

	function get_selected_tax() {
		$taxonomies = get_object_taxonomies( 'post' );
		$key = array_search( 'post_format', $taxonomies );
		if ( false !== $key ) {
			unset( $taxonomies[ $key ] );
		}

		$decode = json_decode( wp_unslash( $_GET['taxonomies'] ), true );
		if ( ! empty( $decode['active'] ) && ! empty( $decode['selected'] ) ) {
			$taxonomies = array_intersect( $decode['active'], $decode['selected'] );
		} else if ( ! empty( $decode['active'] ) ) {
			$taxonomies = $decode['active'];
		} else if ( ! empty( $decode['selected'] ) ) {
			$taxonomies = $decode['selected'];
		}
		return $taxonomies;
	}

	function get_selected_tax_term() {
		$taxonomies  = get_selected_tax();
		$storage_arr = array();
		$str = filter_input( INPUT_GET, 'term', FILTER_SANITIZE_SPECIAL_CHARS );
		foreach ( $taxonomies as $key => $taxonomy ) {
			$terms = get_terms( $taxonomy );

			foreach ( $terms as $key2 => $term ) {
				if ( strpos( $term->slug, strtolower( $str ) ) !== false ) {
					array_push( $storage_arr, array(
						'value'    => $term->slug,
						'text'     => $term->name,
						'taxonomy' => $taxonomy,
					) );
				}
			}
		}
		return $storage_arr;
	}

	wp_send_json( get_selected_tax_term() );
}
add_action( 'wp_ajax_nopriv_rococo_term_search', 'rococo_term_ajax_search' );
add_action( 'wp_ajax_rococo_term_search', 'rococo_term_ajax_search' );

/**
 * Articles ajax search in customizer
 */
function rococo_articles_ajax_search() {
	check_ajax_referer( 'nonce', 'ajax_nonce' );

	global $wpdb;
	$str = filter_input( INPUT_GET, 'term', FILTER_SANITIZE_SPECIAL_CHARS );
	$ids = $wpdb->get_col( "SELECT ID FROM $wpdb->posts WHERE UCASE(post_title) LIKE '%$str%' AND post_type='post' AND post_status='publish'" );

	$posts = array();
	if ( $ids ) {
		$args     = array(
			'post__in'            => $ids,
			'ignore_sticky_posts' => true,
			'posts_per_page'      => - 1,
			'meta_query'          => array(
				array(
					'key' => '_thumbnail_id',
				),
			),
		);
		$my_query = new WP_Query( $args );
		if ( $my_query->have_posts() ) {
			while ( $my_query->have_posts() ) {
				$my_query->the_post();
				array_push( $posts, array(
					'id'    => get_the_ID(),
					'date'  => get_the_date( 'F j, Y' ),
					'label' => get_the_title(),
					'value' => get_the_title(),
				) );
			}
			wp_reset_query();  // Restore global post data stomped by the_post().
		} else {
			array_push( $posts, array(
				'date'  => '',
				'label' => esc_html__( 'No posts found', 'rococo' ),
				'value' => 'empty_results',
			) );
		}
	} else {
		array_push( $posts, array(
			'date'  => '',
			'label' => esc_html__( 'No posts found', 'rococo' ),
			'value' => 'empty_results',
		) );
	}
	wp_send_json( $posts );
};
add_action( 'wp_ajax_nopriv_rococo_articles_search', 'rococo_articles_ajax_search' );
add_action( 'wp_ajax_rococo_articles_search', 'rococo_articles_ajax_search' );

/**
 * Include files
 */
require get_template_directory() . '/inc/tgm/tgm-plugin-registration.php';
require get_template_directory() . '/inc/customizer/nbt-custom-controllers.php';
require get_template_directory() . '/inc/articles-filter/class-articles-filter.php';
require get_template_directory() . '/inc/customizer/nbt-customizer.php';
require get_template_directory() . '/inc/customizer/nbt-customizer-styles.php';
require get_template_directory() . '/inc/customizer/nbt-customizer-scripts.php';
require get_template_directory() . '/templates/template-controller.php';
require get_template_directory() . '/inc/related-post/related.php';
if ( ( get_theme_mod( 'pagination=>index', false ) === 'numeric' ) ||
     ( get_theme_mod( 'pagination=>archive', false ) === 'numeric' ) ||
     ( get_theme_mod( 'pagination=>search', false ) === 'numeric' )
) {
	require get_template_directory() . '/inc/pagination/numeric-pagination.php';
}
require get_template_directory() . '/inc/widgets/widget-instagram-footer.php';
require get_template_directory() . '/inc/widgets/widget-instagram-feed.php';
require get_template_directory() . '/inc/widgets/widget-social-media.php';
require get_template_directory() . '/inc/widgets/widget-contact-us.php';
require get_template_directory() . '/inc/widgets/widget-promo-box.php';
require get_template_directory() . '/inc/widgets/widget-facebook.php';
require get_template_directory() . '/inc/widgets/widget-about-me.php';
require get_template_directory() . '/inc/widgets/widget-posts.php';
