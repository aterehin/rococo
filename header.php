<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section.
 *
 * @package Nobrand
 * @version 1.1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
	<link rel="shortcut icon" href="<?php echo esc_url( get_theme_mod( 'header=>favicon', get_template_directory_uri() . '/images/bg/favicon.png' ) ) ?>">
	<?php else :
		wp_site_icon();
	endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a href="#main" class="screen-reader-shortcut">Skip to content</a>
<?php if ( get_theme_mod( 'layout=>scroll-to-top', true ) ) : ?>
	<a href="#" class="scroll-to-top" data-js="scroll-to-top"><i class="fa fa-angle-up"></i></a>
<?php endif; ?>

<?php echo get_header_image() ? '<div class="header-bg-overlay" style="background-image: url('. esc_url( get_header_image() ) .')">' : ''?>
<div data-js="mob-wrapper">
	<div class="top-bar">
		<div class="container">
			<div class="top-bar__left">
				<?php if ( get_theme_mod( 'top-bar->socials=>enable', true ) ) : ?>

					<ul class="social-list _in-top-bar hidden-xs">

						<?php if ( get_theme_mod( 'top-bar->socials=>facebook-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://www.facebook.com/' . get_theme_mod( 'top-bar->socials=>facebook-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>twitter-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://twitter.com/' . get_theme_mod( 'top-bar->socials=>twitter-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>instagram-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://instagram.com/' . get_theme_mod( 'top-bar->socials=>instagram-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-instagram"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>pinterest-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://pinterest.com/' . get_theme_mod( 'top-bar->socials=>pinterest-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-pinterest"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>google-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://plus.google.com/' . get_theme_mod( 'top-bar->socials=>google-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-google-plus"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>youtube-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://youtube.com/' . get_theme_mod( 'top-bar->socials=>youtube-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>tumblr-link', false ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://' . get_theme_mod( 'top-bar->socials=>tumblr-link', 'nobrand' ) . '.tumblr.com/' ) ?>"
								   target="_blank"><i class="fa fa-tumblr"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>linkedin-link', false ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( get_theme_mod( 'top-bar->socials=>linkedin-link', 'nobrand' ) ) ?>"
								   target="_blank"><i class="fa fa-linkedin"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'top-bar->socials=>rss-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( get_theme_mod( 'top-bar->socials=>rss-link', '' ) ) ?>"
								   target="_blank"><i class="fa fa-rss"></i></a>
							</li>
						<?php endif; ?>

					</ul>

				<?php endif; ?>
			</div>

			<div class="top-bar__right">
				<?php if ( get_theme_mod( 'top-bar->search=>enable', true ) ) :
					get_search_form();
				endif ?>
			</div>

		</div>
	</div>

	<header class="header">

		<div class="menu-toggle" data-js="menu-toggle">
			<span class="menu-toggle__line"></span>
		</div>
		<h1 class="site-brand">
			<?php bloginfo( 'name' ) ?>

			<?php if ( ! is_home() || is_paged() ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<?php endif; ?>

			<?php rococo_logo_image(); ?>

			<?php if ( ! is_home() || is_paged() ) : ?>
			</a>
			<?php endif; ?>
		</h1>
	</header>

	<div class="mob-menu-overlay" <?php if ( ! wp_is_mobile() ) : ?>data-js="sticky-menu"<?php endif; ?>>
		<?php wp_nav_menu( array(
			'theme_location'  => has_nav_menu( 'main-menu' ) ? 'main-menu' : '',
			'container'       => 'div',
			'container_class' => 'menu-list sticky-menu',
			'menu_class'      => '',
			'fallback_cb'     => 'rococo_page_menu',
		) );

		function rococo_page_menu() {
			wp_page_menu( array(
				'menu_class' => 'menu-list sticky-menu',
			) );
		} ?>
	</div>
</div>
<?php echo get_header_image() ? '</div>' : ''?>
