<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Norband
 */

if ( is_active_sidebar( 'main_sidebar' ) ) { dynamic_sidebar( 'main_sidebar' ); } ?>
