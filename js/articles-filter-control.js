( function( $, api ) {

    api.controlConstructor['articles-filter'] = api.Control.extend( {
		ready: function() {
            this.state = this.params.state;
            this.defaults = $.extend( {}, this.params.scope );

            this.selectors = {
                post_type: '.af-control_post-type',
                filter_type: '.af-control_filter-type input[type="radio"]',
                term: '.af-control_terms input[type="checkbox"]',
                incex: '.af-control_incex-control input[type="radio"]',
                tags: '.af-control_incex-rule input[type="text"]',
                order_by: '.af-control_order-by',
                posts: {
                    wrapper: '.af-control_posts',
                    list: '.af-control_p-list',
                    list_item: '.af-control_p-list_item',
                    input: '.af-control_posts input[type="text"]',
                    ac_item: 'af-control_ac-item',
                    control: '.af-item-nav a:not(.af-control-disabled)'
                }
            };

            _.bindAll( this,
                'handleChangePostType',
                'handleChangeTerm',
                'handleChangeIncex',
                'handleChangeTags',
                'handleChangeOrderBy',
                'handleAddPost',
                'handleReorder',
                'handleChangeFilterType',
                'handleBeforeTags'
            );

            this.container.on( 'change', this.selectors.post_type, this.handleChangePostType );
            this.container.on( 'click', this.selectors.term, this.handleChangeTerm );
            this.container.on( 'click', this.selectors.incex, this.handleChangeIncex );
            this.container.on( 'click', this.selectors.filter_type, this.handleChangeFilterType );
            this.container.on( 'beforeItemAdd', this.selectors.tags, this.handleBeforeTags );
            this.container.on( 'itemAdded itemRemoved', this.selectors.tags, this.handleChangeTags );
            this.container.on( 'change', this.selectors.order_by, this.handleChangeOrderBy );
            this.container.on( 'autocompleteselect', this.selectors.posts.input, this.handleAddPost );
            this.container.on( 'click', this.selectors.posts.control, this.handleReorder );

            this.initTags();
            this.initPosts()
		},
        handleChangeFilterType: function( e ) {
            var value = this.container.find( this.selectors.filter_type + ':checked' ).val();

            this.updateState( {
                filter_type: value
            }, [
                this.renderContent,
                this.initPosts,
                this.initTags
            ] );
        },
        handleChangePostType: function( e ) {
            var value = e.currentTarget.value;
            var terms = [];

            if( '' !== value ) {
                terms = this.defaults.post_types[value].terms;
            }

            this.updateState( {
                post_type: value,
                terms: {
                    active: terms,
                    selected: this.state.terms.selected
                }
            }, [
                this.renderContent,
                this.initTags,
                this.initPosts
            ] );
        },
        handleChangeTerm: function() {
            var inputs = this.container.find( this.selectors.term + ':checked' );
            var selected = [];

            inputs.each( function(index, item) {
                selected.push( item.value );
            } );

            this.updateState( {
                terms: {
                    active: this.state.terms.active,
                    selected: selected
                }
            }, [
                this.renderContent,
                this.initTags
            ] );
        },
        handleChangeIncex: function() {
            var value = this.container.find( this.selectors.incex + ':checked' ).val();

            this.updateState( {
                incex: value
            }, this.initPosts );
        },
        handleChangeTags: function( e ) {
            var tags = $( e.currentTarget ).tagsinput( 'items' ).slice();

            this.updateState( {
                tags: tags
            } );
        },
        handleChangeOrderBy: function( e ) {
            var value = e.currentTarget.value;

            this.updateState( {
                order_by: value
            } );
        },
        handleReorder: function( e ) {
            var target = $( e.currentTarget );
            var action = target.data( 'af-post-control' );
            var index = target.data( 'af-post-index' );
            var posts = [];

            switch( action ) {
                case 'up':
                    posts = this.reorderPosts( index, -1 );
                break;
                case 'down':
                    posts = this.reorderPosts( index, 1 );
                break;
                case 'delete':
                    posts = this.reorderPosts( index, 0 );
                break;
            }

            this.updateState( {
                posts: posts
            }, [
                this.renderContent,
                this.initPosts
            ] );
        },
        reorderPosts: function( start, end ) {
            var posts = this.state.posts.slice();
            var current = posts.splice(start, 1);

            if( 0 !== end ) {
                posts.splice(start + end, 0, current[0]);
            }

            return posts;
        },
        handleAddPost: function( e, ui ) {
            var posts = [];

            if( ui.item.value !== 'empty_results' ) {
                posts.push( ui.item );

                for( var i in this.state.posts ) {
                    posts.push( this.state.posts[i] );
                }

                this.updateState( {
                    posts: posts
                }, [
                    this.renderContent,
                    this.initPosts
                ] );
            }
        },
        handleBeforeTags: function( e ) {
            if(e.options && e.options.prevent) {
                e.silence = true;
            }
        },
        initTags: function() {
            var that = this;

            var el = this.container.find( this.selectors.tags );
                el.on( 'keyup', this.selectors.tags, function(e) {
                    if( 13 === e.which ) {
                        $( e.currentTarget ).trigger( 'keypress' );
                    }
                } )
                .tagsinput({
                    tagClass: function(item) {
                        var active = that.state.terms.active;
                        var selected = that.state.terms.selected;

                        if(active.length && selected.length) {
                            var currentTax = [];

                            for (var k = 0; k < active.length; k++) {
                                if (selected.indexOf(active[k]) !== -1) {
                                    currentTax.push(active[k])
                                }
                            }
                            if (currentTax.indexOf(item['taxonomy']) !== -1) {
                                return 'label'
                            } else {
                                return 'label disabled'
                            }
                        } else if (active.length) {
                            if (active.indexOf(item['taxonomy']) !== -1) {
                                return 'label'
                            } else {
                                return 'label disabled'
                            }
                        } else if (selected.length) {
                            if (selected.indexOf(item['taxonomy']) !== -1) {
                                return 'label'
                            } else {
                                return 'label disabled'
                            }
                        } else {
                            return 'label'
                        }
                    },
                    itemValue: 'value',
                    itemText: 'text',
                    typeaheadjs: {
                        name: 'terms',
                        minLength: 3,
                        hint: false,
                        display: 'text',
                        source: function(query, syncResults, asyncResults) {
                            $.ajax({
                                type: "GET",
                                url: admin_ajax['url'] + '?action=rococo_term_search&ajax_nonce=' + admin_ajax['nonce'],
                                data: {
                                    taxonomies: JSON.stringify(that.state['terms']),
                                    term: query
                                },
                                success: function(data) {
                                    asyncResults(data)
                                }
                            });
                        },
                        templates: {
                            empty: [
                                '<div class="tt-suggestion">',
                                'No terms found',
                                '</div>'
                            ].join('\n')
                        }
                    }
                });

            this.container.find( '.tt-input' ).attr( 'size',1);

            var tags = this.state.tags.slice();

            for(var tag in tags ) {
                el.tagsinput( 'add', tags[tag], { prevent: true } )
            }
        },
        initPosts: function() {
            var self = this;
            var input = $( this.selectors.posts.input );

            input.autocomplete( {
                appendTo: this.selectors.posts.wrapper,
                minLength: 3,
                source: admin_ajax['url'] + '?action=rococo_articles_search&ajax_nonce=' + admin_ajax['nonce'],
                focus: function( event, ui ) {
                    if( ui.item.value === 'empty_results' ) {
                        event.preventDefault();
                    }
                },
                select: function( event, ui ) {
                    if( ui.item.value === 'empty_results' ) {
                        event.preventDefault();
                    }
                }
            } ).autocomplete( 'instance' )._renderItem = function( ul, item ) {
                var template = '<li class="' + self.selectors.posts.ac_item + '">'+
                                    '<h4>' + item.label + '</h4>'+
                                    '<span>' + item.date + '</span>'+
                               '</li>';

                return $( template ).appendTo( ul );
            };
        },
        updateState: function( value, callbacks ) {
            this.state = this.params.state = $.extend( {}, this.state, value );

            this.setting( this.state );

            if( $.isArray( callbacks ) ) {
                for( var i in callbacks ) {
                    callbacks[i].apply( this );
                }
            } else if( $.isFunction( callbacks ) ) {
                callbacks.apply( this );
            }
        }
    });

} )( jQuery, wp.customize );
