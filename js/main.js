jQuery(document).ready(function ($) {

    'use strict';

    /**
     *  Application Init
     *  Init Application widgets and components.
     */

    Application.init({
        searchInput: function () {
            var input = $('[data-js="search-field"]');
            input.on('focus blur', function () {
                $(this).closest('.site-search')
                    .toggleClass('_focus');
            });

            return input;
        },
        stickyHeader: function () {
            if(!localize_array['sticky_menu'] || !$('[data-js="sticky-menu"]').length) {
                return
            }

            var module = {};

            function mediator() {

                var spacerHeight = module.spacer.outerHeight();
                var actionPoint = module.spacer.offset().top;
                var htmlOffset = $('html').css('margin-top').replace('px', '');

                if (htmlOffset > 0) {
                    actionPoint -= htmlOffset
                }

                if ($(window).scrollTop() > actionPoint + 1) {
                    module.spacer.css('height', spacerHeight);
                    module.header.addClass('_fixed');
                } else  {
                    module.header.removeClass('_fixed _top');
                }
            }

            module.init = function () {
                module.header = $('.sticky-menu');
                module.spacer = $('[data-js="sticky-menu"]');

                $(window).on('load scroll resize', function () {
                    if (!Application.isMobile && window.innerWidth > 991) {
                        mediator();
                    }
                });
            };

            module.init();

            return module;
        },
        mobileMenu: function () {
            var module = {};

            module.menuClose = function () {
                $('[data-js="mob-wrapper"]').removeClass('_menu-open');
                $(module.menuToggle).removeClass('_active');
                $('body').removeClass('_fixed');
                $('.mob-menu .menu-item-has-children.active').removeClass('active')
                    .find('.sub-menu').css('display', 'none');
            };

            module.menuOpen = function () {
                $('[data-js="mob-wrapper"]').addClass('_menu-open')
                    .scrollTop(0);
                $(module.menuToggle).addClass('_active');
                $('body').addClass('_fixed');
            };

            function menuAction() {
                if (module.menuToggle.hasClass('_active')) {
                    module.menuClose();
                } else {
                    module.menuOpen();
                }
            }

            module.init = function () {
                this.menuToggle = $('[data-js="menu-toggle"]');

                this.menuToggle.on('click', menuAction);
                var deviceWidth = window.outerWidth;

                $(window).resize(function(){
                    if (window.outerWidth != deviceWidth) {
                        module.menuClose();
                        deviceWidth = window.outerWidth;
                    }
                });
            };

            module.init();
            return module;
        },
        sliderParallax: function(){
            if (Application.isMobile || window.innerWidth < 992 || !$('[data-js="parallax"]').length) return;

            $(window).on('load', function(){
                var $parallaxElement = $( '[data-js="parallax"]' );
                var $parallaxElementHeight = $parallaxElement.innerHeight();

                var $darkenElement = $( '[data-js="parallax-darken"]' );
                var $darkenElementOpacity = $darkenElement.css( 'opacity' );
                var $darkenElementDiapason = ( 0.9 - $darkenElementOpacity );

                var $lightenElement = $( '[data-js="parallax-lighten"]' );
                var $lightenElementOpacity = $lightenElement.css( 'opacity' );
                var $lightenElementDiapason = 1;

                var startPos = $parallaxElement.offset().top;

                function action() {
                    if ( ( window.pageYOffset - startPos ) > 0 && !( ( window.pageYOffset - startPos ) > $parallaxElementHeight ) ) {

                        var currentScrollPos = window.pageYOffset - startPos;

                        $parallaxElement.css( 'top', ( currentScrollPos ) / 2.5 );

                        $darkenElement.css( 'opacity', +$darkenElementOpacity + percent( currentScrollPos, $darkenElementDiapason ) );
                        $lightenElement.css( 'opacity', +$lightenElementOpacity - percent( currentScrollPos, $lightenElementDiapason ) );
                    } else if ( ( window.pageYOffset - startPos ) < 0 ) {
                        $parallaxElement.css( 'top', 0 );
                        $darkenElement.css('opacity', $darkenElementOpacity);
                        $lightenElement.css('opacity', $lightenElementOpacity);
                    }
                }

                function percent(x, y) {
                    return y / 100 * ( x * 100 / $parallaxElementHeight )
                }

                action();

                $(document).on('scroll', function() {
                    action()
                });
            })
        },
        scrollToTop: function(){
            if (Application.isMobile) return;
            var $toggle = $('[data-js="scroll-to-top"]');
            var $toggleDefaultBottom = $toggle.css('bottom');
            var $footer = $('[data-js="bottom-panel"]');
            $(window).on('scroll load resize', function(){
                var $this = $(this);
                if ($this.scrollTop() > 800) {
                    $toggle.addClass('_visible')
                } else {
                    $toggle.removeClass('_visible')
                }

                if ($this.scrollTop() + $this.height() >= $footer.offset().top) {
                    var $offset = $this.scrollTop() + $this.height() - $footer.offset().top + 30;
                    $toggle.css('bottom', $offset);
                } else {
                    $toggle.css('bottom', $toggleDefaultBottom)
                }
            });

            $toggle.on('click', function(){
                $('html, body').animate({scrollTop: 0}, 600);
                return false;
            });
        },
        embedContent: function(){
            $.fn.fitVids = function( options ) {
                var settings = {
                    customSelector: null,
                    ignore: null
                };

                if(!document.getElementById('fit-vids-style')) {
                    // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
                    var head = document.head || document.getElementsByTagName('head')[0];
                    var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
                    var div = document.createElement("div");
                    div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
                    head.appendChild(div.childNodes[1]);
                }

                if ( options ) {
                    $.extend( settings, options );
                }

                return this.each(function(){
                    var selectors = [
                        'iframe[src*="player.vimeo.com"]',
                        'iframe[src*="youtube.com"]',
                        'iframe[src*="youtube-nocookie.com"]',
                        'iframe[src*="kickstarter.com"][src*="video.html"]',
                        'object',
                        'embed'
                    ];

                    if (settings.customSelector) {
                        selectors.push(settings.customSelector);
                    }

                    var ignoreList = '.fitvidsignore';

                    if(settings.ignore) {
                        ignoreList = ignoreList + ', ' + settings.ignore;
                    }

                    var $allVideos = $(this).find(selectors.join(','));
                    $allVideos = $allVideos.not('object object'); // SwfObj conflict patch
                    $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

                    $allVideos.each(function(count){
                        var $this = $(this);
                        if($this.parents(ignoreList).length > 0) {
                            return; // Disable FitVids on this video.
                        }
                        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
                        if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
                        {
                            $this.attr('height', 9);
                            $this.attr('width', 16);
                        }
                        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
                            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
                            aspectRatio = height / width;
                        if(!$this.attr('id')){
                            var videoID = 'fitvid' + count;
                            $this.attr('id', videoID);
                        }
                        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
                        $this.removeAttr('height').removeAttr('width');
                    });
                });
            };
            $("body").fitVids( {
                customSelector: [
                    "iframe[src*='youtu.be']",
                    "iframe[src*='blip.tv']",
                    "iframe[src*='dailymotion.com']",
                    "iframe[src*='spotify.com']",
                    "iframe[src*='slideshare.net']",
                    "iframe[src*='vimeo.com']"
                ],
                ignore: ["iframe[src*='soundcloud.com']"]
            } );
        }
    });
});
