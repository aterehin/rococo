<?php
/**
 * The template for displaying all pages.
 * Template Name: Full Width Layout
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Nobrand
 * @version 1.1.0
 */

get_header(); ?>
	<hr class="delimiter">
	<div class="container">
		<div class="content _full-width">
			<div id="main" class="content__primary">
				<?php if ( have_posts() ) :
					while ( have_posts() ) {
						the_post();
						get_template_part( 'templates/content', 'page' );
					}
				endif; ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
