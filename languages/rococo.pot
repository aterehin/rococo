#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Rococo\n"
"POT-Creation-Date: 2016-03-30 22:23+0500\n"
"PO-Revision-Date: 2016-03-05 12:10+0500\n"
"Last-Translator: Nobrand <mail.nobrand.team>\n"
"Language-Team: Nobrand <mail.nobrand.team>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.5\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _n:1,2;esc_html__;esc_html_e;esc_html_x:1,2c;__\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: css\n"
"X-Poedit-SearchPathExcluded-1: fonts\n"
"X-Poedit-SearchPathExcluded-2: images\n"
"X-Poedit-SearchPathExcluded-3: languages\n"
"X-Poedit-SearchPathExcluded-4: maps\n"
"X-Poedit-SearchPathExcluded-5: node_modules\n"
"X-Poedit-SearchPathExcluded-6: sass\n"
"X-Poedit-SearchPathExcluded-7: js\n"
"X-Poedit-SearchPathExcluded-8: inc/tgm\n"

#: 404.php:15
#, php-format
msgid "Page %s not found. %s"
msgstr ""

#: 404.php:18
#, php-format
msgid "It looks like nothing was found at this location.%s Maybe try a search?"
msgstr ""

#: archive.php:220 index.php:228 search.php:221
#, php-format
msgid "Older Posts %s"
msgstr ""

#: archive.php:221 index.php:229 search.php:222
#, php-format
msgid "%s Newer Posts"
msgstr ""

#: comments.php:23
msgid "comment"
msgid_plural "comments"
msgstr[0] ""
msgstr[1] ""

#: comments.php:36
msgid "Name"
msgstr ""

#: comments.php:36
msgid "Email"
msgstr ""

#: comments.php:37
msgid "Comment"
msgstr ""

#: comments.php:46
msgid "Leave a Reply"
msgstr ""

#: comments.php:47
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: comments.php:48
msgid "Cancel reply"
msgstr ""

#: comments.php:49
msgid "Post Comment"
msgstr ""

#: footer.php:116
#, php-format
msgid "%s %d All Rights Reserved."
msgstr ""

#: functions.php:46
msgid "Primary menu"
msgstr ""

#: functions.php:139
msgctxt "Merriweather font: on or off"
msgid "on"
msgstr ""

#: functions.php:143
msgctxt "Lato font: on or off"
msgid "on"
msgstr ""

#: functions.php:172
msgid "Right Sidebar"
msgstr ""

#: functions.php:173
msgid "Drag widget"
msgstr ""

#: functions.php:183
#, php-format
msgid "Footer Widgets Area %d"
msgstr ""

#: functions.php:184
msgid "Settings \"Footer Widget Area grid\""
msgstr ""

#: functions.php:194
msgid "Instagram Feed *"
msgstr ""

#: functions.php:195
msgid "This area only for Instagram widget marked by \"*\""
msgstr ""

#: functions.php:210
msgid "Facebook Username"
msgstr ""

#: functions.php:211
msgid "Twitter Username"
msgstr ""

#: functions.php:212
msgid "Instagram Username"
msgstr ""

#: functions.php:213
msgid "Pinterest Username"
msgstr ""

#: functions.php:214
msgid "Google+ Account ID"
msgstr ""

#: functions.php:215
msgid "Tumblr Username"
msgstr ""

#: functions.php:216
msgid "LinkedIn Public Profile URL"
msgstr ""

#: functions.php:237
msgid "Read more..."
msgstr ""

#: functions.php:413 functions.php:420
msgid "No posts found"
msgstr ""

#: image.php:98
#, php-format
msgid "%s Prev Image"
msgstr ""

#: image.php:101
#, php-format
msgid "Next Image %s"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:178
msgid "Order by:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:189
msgid "Filter by:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:199
msgid "Post type & taxonomies"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:209
msgid "Specific posts"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:218
msgid "Specific posts:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:239
msgid "Start typing a text, and then select a post."
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:246
msgid "Post type:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:249
msgid "All Post Types"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:260
msgid "Taxonomies:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:285
msgid "Taxonomy rules:"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:294
msgid "Include"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:304
msgid "Exclude"
msgstr ""

#: inc/customizer/nbt-custom-controllers.php:310
msgid ""
"Type a word and select taxanomy term, so you added a term, which will be "
"include or exclude posts."
msgstr ""

#: inc/customizer/nbt-customizer.php:13
msgctxt "Used in Layout Customizing"
msgid "Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:14
msgctxt "Used in Layout Customizing"
msgid "Select page layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:90
msgctxt "Used in Layout Customizing"
msgid "Sticky Menu"
msgstr ""

#: inc/customizer/nbt-customizer.php:91
msgctxt "Used in Layout Customizing"
msgid "Enable fixed menu"
msgstr ""

#: inc/customizer/nbt-customizer.php:98
msgctxt "Used in Layout Customizing"
msgid "Scroll To Top"
msgstr ""

#: inc/customizer/nbt-customizer.php:99
msgctxt "Used in Layout Customizing"
msgid "Enable button"
msgstr ""

#: inc/customizer/nbt-customizer.php:106
msgctxt "Used in Layout Customizing"
msgid "Home Page Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:107 inc/customizer/nbt-customizer.php:152
#: inc/customizer/nbt-customizer.php:210
msgctxt "Used in Layout Customizing"
msgid "Posts layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:110 inc/customizer/nbt-customizer.php:155
#: inc/customizer/nbt-customizer.php:213
msgctxt "Used in Layout Customizing"
msgid "Standard"
msgstr ""

#: inc/customizer/nbt-customizer.php:111 inc/customizer/nbt-customizer.php:156
#: inc/customizer/nbt-customizer.php:214
msgctxt "Used in Layout Customizing"
msgid "Standard First & Grid"
msgstr ""

#: inc/customizer/nbt-customizer.php:112 inc/customizer/nbt-customizer.php:157
#: inc/customizer/nbt-customizer.php:215
msgctxt "Used in Layout Customizing"
msgid "Standard First & List"
msgstr ""

#: inc/customizer/nbt-customizer.php:113 inc/customizer/nbt-customizer.php:158
#: inc/customizer/nbt-customizer.php:216
msgctxt "Used in Layout Customizing"
msgid "Standard & Grid"
msgstr ""

#: inc/customizer/nbt-customizer.php:114 inc/customizer/nbt-customizer.php:159
#: inc/customizer/nbt-customizer.php:217
msgctxt "Used in Layout Customizing"
msgid "Standard & List"
msgstr ""

#: inc/customizer/nbt-customizer.php:115 inc/customizer/nbt-customizer.php:160
#: inc/customizer/nbt-customizer.php:218
msgctxt "Used in Layout Customizing"
msgid "Grid"
msgstr ""

#: inc/customizer/nbt-customizer.php:116 inc/customizer/nbt-customizer.php:161
#: inc/customizer/nbt-customizer.php:219
msgctxt "Used in Layout Customizing"
msgid "List"
msgstr ""

#: inc/customizer/nbt-customizer.php:117 inc/customizer/nbt-customizer.php:162
#: inc/customizer/nbt-customizer.php:220
msgctxt "Used in Layout Customizing"
msgid "Grid & List"
msgstr ""

#: inc/customizer/nbt-customizer.php:118 inc/customizer/nbt-customizer.php:163
#: inc/customizer/nbt-customizer.php:221
msgctxt "Used in Layout Customizing"
msgid "Zigzag"
msgstr ""

#: inc/customizer/nbt-customizer.php:125 inc/customizer/nbt-customizer.php:170
#: inc/customizer/nbt-customizer.php:190 inc/customizer/nbt-customizer.php:228
msgctxt "Used in Layout Customizing"
msgid "Enable featured slider"
msgstr ""

#: inc/customizer/nbt-customizer.php:132
msgctxt "Used in Layout Customizing"
msgid "Enable popular posts"
msgstr ""

#: inc/customizer/nbt-customizer.php:139 inc/customizer/nbt-customizer.php:177
#: inc/customizer/nbt-customizer.php:197 inc/customizer/nbt-customizer.php:235
msgctxt "Used in Layout Customizing"
msgid "Sidebar"
msgstr ""

#: inc/customizer/nbt-customizer.php:142 inc/customizer/nbt-customizer.php:180
#: inc/customizer/nbt-customizer.php:200 inc/customizer/nbt-customizer.php:238
msgctxt "Used in Layout Customizing"
msgid "Left"
msgstr ""

#: inc/customizer/nbt-customizer.php:143 inc/customizer/nbt-customizer.php:181
#: inc/customizer/nbt-customizer.php:201 inc/customizer/nbt-customizer.php:239
msgctxt "Used in Layout Customizing"
msgid "Right"
msgstr ""

#: inc/customizer/nbt-customizer.php:144 inc/customizer/nbt-customizer.php:182
#: inc/customizer/nbt-customizer.php:202 inc/customizer/nbt-customizer.php:240
msgctxt "Used in Layout Customizing"
msgid "Disable"
msgstr ""

#: inc/customizer/nbt-customizer.php:151
msgctxt "Used in Layout Customizing"
msgid "Archive Page Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:189
msgctxt "Used in Layout Customizing"
msgid "Single Post Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:209
msgctxt "Used in Layout Customizing"
msgid "Search Result Page Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:247
msgctxt "Used in Top Bar Customizing"
msgid "Top Bar Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:253
msgctxt "Used in Top Bar Customizing"
msgid "Social Links Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:314
msgctxt "Used in Top Bar Customizing"
msgid "Social Links"
msgstr ""

#: inc/customizer/nbt-customizer.php:315
msgctxt "Used in Top Bar Customizing"
msgid "Enable links"
msgstr ""

#: inc/customizer/nbt-customizer.php:322
msgctxt "Used in Top Bar Customizing"
msgid "Facebook Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:329
msgctxt "Used in Top Bar Customizing"
msgid "Twitter Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:336
msgctxt "Used in Top Bar Customizing"
msgid "Instagram Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:343
msgctxt "Used in Top Bar Customizing"
msgid "Pinterest Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:350
msgctxt "Used in Top Bar Customizing"
msgid "Google+ Account ID"
msgstr ""

#: inc/customizer/nbt-customizer.php:357
msgctxt "Used in Top Bar Customizing"
msgid "YouTube Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:364
msgctxt "Used in Top Bar Customizing"
msgid "Tumblr Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:371
msgctxt "Used in Top Bar Customizing"
msgid "Linkedin Public Profile URL"
msgstr ""

#: inc/customizer/nbt-customizer.php:378
msgctxt "Used in Top Bar Customizing"
msgid "RSS URL"
msgstr ""

#: inc/customizer/nbt-customizer.php:385 inc/customizer/nbt-customizer.php:401
msgctxt "Used in Top Bar Customizing"
msgid "Site Search"
msgstr ""

#: inc/customizer/nbt-customizer.php:395
msgctxt "Used in Top Bar Customizing"
msgid "Search and hit enter..."
msgstr ""

#: inc/customizer/nbt-customizer.php:402
msgctxt "Used in Top Bar Customizing"
msgid "Enable search"
msgstr ""

#: inc/customizer/nbt-customizer.php:409
msgctxt "Used in Top Bar Customizing"
msgid "Search placeholder"
msgstr ""

#: inc/customizer/nbt-customizer.php:416
msgctxt "Used in Logo Customizing"
msgid "Header"
msgstr ""

#: inc/customizer/nbt-customizer.php:472
msgctxt "Used in Logo Customizing"
msgid "Header padding top"
msgstr ""

#: inc/customizer/nbt-customizer.php:479
msgctxt "Used in Logo Customizing"
msgid "Header padding bottom"
msgstr ""

#: inc/customizer/nbt-customizer.php:486
msgctxt "Used in Logo Customizing"
msgid "Header padding top (on mobile devices)"
msgstr ""

#: inc/customizer/nbt-customizer.php:493
msgctxt "Used in Logo Customizing"
msgid "Header padding bottom (on mobile devices)"
msgstr ""

#: inc/customizer/nbt-customizer.php:507
msgctxt "Used in Logo Customizing"
msgid "Logo"
msgstr ""

#: inc/customizer/nbt-customizer.php:513
msgctxt "Used in Logo Customizing"
msgid "Required for header background version"
msgstr ""

#: inc/customizer/nbt-customizer.php:514
msgctxt "Used in Logo Customizing"
msgid "Logo Light"
msgstr ""

#: inc/customizer/nbt-customizer.php:520
msgctxt "Used in Logo Customizing"
msgid "Retina Logo"
msgstr ""

#: inc/customizer/nbt-customizer.php:526
msgctxt "Used in Logo Customizing"
msgid "Retina Logo Light"
msgstr ""

#: inc/customizer/nbt-customizer.php:533
msgctxt "Used in Logo Customizing"
msgid "Favicon"
msgstr ""

#: inc/customizer/nbt-customizer.php:540
msgctxt "Used in Featured slider Settings"
msgid "Featured Slider"
msgstr ""

#: inc/customizer/nbt-customizer.php:617
msgctxt "Used in Featured slider Settings"
msgid "Articles Filter"
msgstr ""

#: inc/customizer/nbt-customizer.php:623
msgctxt "Used in Featured slider Settings"
msgid "Slider Style"
msgstr ""

#: inc/customizer/nbt-customizer.php:626
msgctxt "Used in Featured slider Settings"
msgid "Box with preview"
msgstr ""

#: inc/customizer/nbt-customizer.php:627
msgctxt "Used in Featured slider Settings"
msgid "Box"
msgstr ""

#: inc/customizer/nbt-customizer.php:628
msgctxt "Used in Featured slider Settings"
msgid "Full width"
msgstr ""

#: inc/customizer/nbt-customizer.php:635
msgctxt "Used in Featured slider Settings"
msgid "Number of slides"
msgstr ""

#: inc/customizer/nbt-customizer.php:642
msgctxt "Used in Featured slider Settings"
msgid "Navigation Arrows"
msgstr ""

#: inc/customizer/nbt-customizer.php:643 inc/customizer/nbt-customizer.php:671
#: inc/customizer/nbt-customizer.php:699
msgctxt "Used in Featured slider Settings"
msgid "Enable"
msgstr ""

#: inc/customizer/nbt-customizer.php:650
msgctxt "Used in Featured slider Settings"
msgid "Navigation arrows style"
msgstr ""

#: inc/customizer/nbt-customizer.php:653
msgctxt "Used in Featured slider Settings"
msgid "Thumb Flip"
msgstr ""

#: inc/customizer/nbt-customizer.php:654
msgctxt "Used in Featured slider Settings"
msgid "Circle Slide"
msgstr ""

#: inc/customizer/nbt-customizer.php:655
msgctxt "Used in Featured slider Settings"
msgid "Diamond"
msgstr ""

#: inc/customizer/nbt-customizer.php:656 inc/customizer/nbt-customizer.php:684
msgctxt "Used in Featured slider Settings"
msgid "None"
msgstr ""

#: inc/customizer/nbt-customizer.php:663
msgctxt "Used in Featured slider Settings"
msgid "Navigation arrows speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:670
msgctxt "Used in Featured slider Settings"
msgid "Navigation Dots"
msgstr ""

#: inc/customizer/nbt-customizer.php:678
msgctxt "Used in Featured slider Settings"
msgid "Navigation dots style"
msgstr ""

#: inc/customizer/nbt-customizer.php:681
msgctxt "Used in Featured slider Settings"
msgid "Scale Up"
msgstr ""

#: inc/customizer/nbt-customizer.php:682
msgctxt "Used in Featured slider Settings"
msgid "Fill Up"
msgstr ""

#: inc/customizer/nbt-customizer.php:683
msgctxt "Used in Featured slider Settings"
msgid "Fall"
msgstr ""

#: inc/customizer/nbt-customizer.php:691
msgctxt "Used in Featured slider Settings"
msgid "Navigation dots speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:698
msgctxt "Used in Featured slider Settings"
msgid "Autoplay"
msgstr ""

#: inc/customizer/nbt-customizer.php:706
msgctxt "Used in Featured slider Settings"
msgid "Autoplay interval timeout"
msgstr ""

#: inc/customizer/nbt-customizer.php:713
msgctxt "Used in Featured slider Settings"
msgid "Autoplay speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:720
msgctxt "Used in Post Customizing"
msgid "Post Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:726
msgctxt "Used in Post Customizing"
msgid "Post Layout"
msgstr ""

#: inc/customizer/nbt-customizer.php:777
msgctxt "Used in Post Customizing"
msgid "Post Categories"
msgstr ""

#: inc/customizer/nbt-customizer.php:778
msgctxt "Used in Post Customizing"
msgid "Enable categories"
msgstr ""

#: inc/customizer/nbt-customizer.php:785
msgctxt "Used in Post Customizing"
msgid "Post Date"
msgstr ""

#: inc/customizer/nbt-customizer.php:786
msgctxt "Used in Post Customizing"
msgid "Enable date"
msgstr ""

#: inc/customizer/nbt-customizer.php:793
msgctxt "Used in Post Customizing"
msgid "Post Author"
msgstr ""

#: inc/customizer/nbt-customizer.php:794
msgctxt "Used in Post Customizing"
msgid "Enable author name"
msgstr ""

#: inc/customizer/nbt-customizer.php:801
msgctxt "Used in Post Customizing"
msgid "Number of Comments"
msgstr ""

#: inc/customizer/nbt-customizer.php:802
msgctxt "Used in Post Customizing"
msgid "Enable comments"
msgstr ""

#: inc/customizer/nbt-customizer.php:809
msgctxt "Used in Post Customizing"
msgid "Read More Link"
msgstr ""

#: inc/customizer/nbt-customizer.php:810
msgctxt "Used in Post Customizing"
msgid "Enable link"
msgstr ""

#: inc/customizer/nbt-customizer.php:817
msgctxt "Used in Post Customizing"
msgid "Tags"
msgstr ""

#: inc/customizer/nbt-customizer.php:818
msgctxt "Used in Post Customizing"
msgid "Enable tags"
msgstr ""

#: inc/customizer/nbt-customizer.php:825
msgctxt "Used in Post Customizing"
msgid "Author Biography"
msgstr ""

#: inc/customizer/nbt-customizer.php:826
msgctxt "Used in Post Customizing"
msgid "Enable biography"
msgstr ""

#: inc/customizer/nbt-customizer.php:833
msgctxt "Used in Post Customizing"
msgid "Post Navigation Links"
msgstr ""

#: inc/customizer/nbt-customizer.php:834 inc/customizer/nbt-customizer.php:896
msgctxt "Used in Post Customizing"
msgid "Enable links"
msgstr ""

#: inc/customizer/nbt-customizer.php:841
msgctxt "Used in Post Customizing"
msgid "Related Posts"
msgstr ""

#: inc/customizer/nbt-customizer.php:842
msgctxt "Used in Post Customizing"
msgid "Enable posts"
msgstr ""

#: inc/customizer/nbt-customizer.php:849
msgctxt "Used in Post Customizing"
msgid "Social Links Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:895
msgctxt "Used in Post Customizing"
msgid "Social links"
msgstr ""

#: inc/customizer/nbt-customizer.php:903
msgctxt "Used in Post Customizing"
msgid "Facebook"
msgstr ""

#: inc/customizer/nbt-customizer.php:910
msgctxt "Used in Post Customizing"
msgid "Twitter"
msgstr ""

#: inc/customizer/nbt-customizer.php:917
msgctxt "Used in Post Customizing"
msgid "Pinterest"
msgstr ""

#: inc/customizer/nbt-customizer.php:924
msgctxt "Used in Post Customizing"
msgid "Google+"
msgstr ""

#: inc/customizer/nbt-customizer.php:931
msgctxt "Used in Post Customizing"
msgid "Tumblr"
msgstr ""

#: inc/customizer/nbt-customizer.php:938
msgctxt "Used in Post Customizing"
msgid "Linkedin"
msgstr ""

#: inc/customizer/nbt-customizer.php:945
msgctxt "Used in Post Customizing"
msgid "Stumbleupon"
msgstr ""

#: inc/customizer/nbt-customizer.php:953
msgctxt "Used in Post Customizing"
msgid "Image Slider Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:1010
msgctxt "Used in Post Customizing"
msgid "Slider Animation Effect"
msgstr ""

#: inc/customizer/nbt-customizer.php:1013
msgctxt "Used in Post Customizing"
msgid "List"
msgstr ""

#: inc/customizer/nbt-customizer.php:1014
msgctxt "Used in Post Customizing"
msgid "Fade Out"
msgstr ""

#: inc/customizer/nbt-customizer.php:1015
msgctxt "Used in Post Customizing"
msgid "Fade Out In"
msgstr ""

#: inc/customizer/nbt-customizer.php:1016
msgctxt "Used in Post Customizing"
msgid "Flip In X"
msgstr ""

#: inc/customizer/nbt-customizer.php:1017
msgctxt "Used in Post Customizing"
msgid "Slide Out Down"
msgstr ""

#: inc/customizer/nbt-customizer.php:1024
msgctxt "Used in Post Customizing"
msgid "Navigation Arrows"
msgstr ""

#: inc/customizer/nbt-customizer.php:1025
#: inc/customizer/nbt-customizer.php:1053
#: inc/customizer/nbt-customizer.php:1081
msgctxt "Used in Post Customizing"
msgid "Enable"
msgstr ""

#: inc/customizer/nbt-customizer.php:1032
msgctxt "Used in Post Customizing"
msgid "Navigation arrows style"
msgstr ""

#: inc/customizer/nbt-customizer.php:1035
msgctxt "Used in Post Customizing"
msgid "Thumb Flip"
msgstr ""

#: inc/customizer/nbt-customizer.php:1036
msgctxt "Used in Post Customizing"
msgid "Circle Slide"
msgstr ""

#: inc/customizer/nbt-customizer.php:1037
msgctxt "Used in Post Customizing"
msgid "Diamond"
msgstr ""

#: inc/customizer/nbt-customizer.php:1038
#: inc/customizer/nbt-customizer.php:1066
msgctxt "Used in Post Customizing"
msgid "None"
msgstr ""

#: inc/customizer/nbt-customizer.php:1045
msgctxt "Used in Post Customizing"
msgid "Navigation arrows speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:1052
msgctxt "Used in Post Customizing"
msgid "Navigation Dots"
msgstr ""

#: inc/customizer/nbt-customizer.php:1060
msgctxt "Used in Post Customizing"
msgid "Navigation dots style"
msgstr ""

#: inc/customizer/nbt-customizer.php:1063
msgctxt "Used in Post Customizing"
msgid "Scale Up"
msgstr ""

#: inc/customizer/nbt-customizer.php:1064
msgctxt "Used in Post Customizing"
msgid "Fill Up"
msgstr ""

#: inc/customizer/nbt-customizer.php:1065
msgctxt "Used in Post Customizing"
msgid "Fall"
msgstr ""

#: inc/customizer/nbt-customizer.php:1073
msgctxt "Used in Post Customizing"
msgid "Navigation dots speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:1080
msgctxt "Used in Post Customizing"
msgid "Autoplay"
msgstr ""

#: inc/customizer/nbt-customizer.php:1088
msgctxt "Used in Post Customizing"
msgid "Autoplay interval timeout"
msgstr ""

#: inc/customizer/nbt-customizer.php:1095
msgctxt "Used in Post Customizing"
msgid "Autoplay speed"
msgstr ""

#: inc/customizer/nbt-customizer.php:1102
msgctxt "Used in Pagination Customizing"
msgid "Pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1103
msgctxt "Used in Pagination Customizing"
msgid "Select page pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1124
msgctxt "Used in Pagination Customizing"
msgid "Index Page Pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1127
#: inc/customizer/nbt-customizer.php:1138
#: inc/customizer/nbt-customizer.php:1149
msgctxt "Used in Pagination Customizing"
msgid "Prev page / Next page"
msgstr ""

#: inc/customizer/nbt-customizer.php:1128
#: inc/customizer/nbt-customizer.php:1139
#: inc/customizer/nbt-customizer.php:1150
msgctxt "Used in Pagination Customizing"
msgid "Numeric Pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1135
msgctxt "Used in Pagination Customizing"
msgid "Archive Page Pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1146
msgctxt "Used in Pagination Customizing"
msgid "Search Result Page Pagination"
msgstr ""

#: inc/customizer/nbt-customizer.php:1156
msgctxt "Used in Footer Customizing"
msgid "Footer Settings"
msgstr ""

#: inc/customizer/nbt-customizer.php:1166
#, php-format
msgctxt "Used in Footer Customizing"
msgid "%s %d All Rights Reserved."
msgstr ""

#: inc/customizer/nbt-customizer.php:1227
msgctxt "Used in Footer Customizing"
msgid "Widgets Area Grid"
msgstr ""

#: inc/customizer/nbt-customizer.php:1230
msgctxt "Used in Footer Customizing"
msgid "One Column"
msgstr ""

#: inc/customizer/nbt-customizer.php:1231
msgctxt "Used in Footer Customizing"
msgid "Two Column"
msgstr ""

#: inc/customizer/nbt-customizer.php:1232
msgctxt "Used in Footer Customizing"
msgid "Three Column"
msgstr ""

#: inc/customizer/nbt-customizer.php:1233
msgctxt "Used in Footer Customizing"
msgid "Four Column"
msgstr ""

#: inc/customizer/nbt-customizer.php:1240
msgctxt "Used in Footer Customizing"
msgid "Copyright Left"
msgstr ""

#: inc/customizer/nbt-customizer.php:1247
msgctxt "Used in Footer Customizing"
msgid "Copyright Right"
msgstr ""

#: inc/customizer/nbt-customizer.php:1254
msgctxt "Used in Footer Customizing"
msgid "Social Bar"
msgstr ""

#: inc/customizer/nbt-customizer.php:1255
msgctxt "Used in Footer Customizing"
msgid "Enable bar"
msgstr ""

#: inc/customizer/nbt-customizer.php:1262
msgctxt "Used in Footer Customizing"
msgid "Facebook Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1269
msgctxt "Used in Footer Customizing"
msgid "Twitter Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1276
msgctxt "Used in Footer Customizing"
msgid "Instagram Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1283
msgctxt "Used in Footer Customizing"
msgid "Pinterest Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1290
msgctxt "Used in Footer Customizing"
msgid "Google+ Account ID"
msgstr ""

#: inc/customizer/nbt-customizer.php:1297
msgctxt "Used in Footer Customizing"
msgid "YouTube Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1304
msgctxt "Used in Footer Customizing"
msgid "Tumblr Username"
msgstr ""

#: inc/customizer/nbt-customizer.php:1311
msgctxt "Used in Footer Customizing"
msgid "Linkedin Public Profile URL"
msgstr ""

#: inc/customizer/nbt-customizer.php:1318
msgctxt "Used in Footer Customizing"
msgid "RSS URL"
msgstr ""

#: inc/customizer/nbt-customizer.php:1324
msgctxt "Used in Messages Customizing"
msgid "Messages"
msgstr ""

#: inc/customizer/nbt-customizer.php:1329
msgctxt "Used in Messages Customizing"
msgid ""
"It seems we cant find what youre looking for. Perhaps searching can help."
msgstr ""

#: inc/customizer/nbt-customizer.php:1339
msgctxt "Used in Messages Customizing"
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: inc/customizer/nbt-customizer.php:1349
msgctxt "Used in Messages Customizing"
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr ""

#: inc/customizer/nbt-customizer.php:1360
msgctxt "Used in 404 Page Customizing"
msgid "Index Page Message"
msgstr ""

#: inc/customizer/nbt-customizer.php:1367
#: inc/customizer/nbt-customizer.php:1381
#: inc/customizer/nbt-customizer.php:1395
msgctxt "Used in 404 Page Customizing"
msgid "Enable search field"
msgstr ""

#: inc/customizer/nbt-customizer.php:1374
msgctxt "Used in 404 Page Customizing"
msgid "Search Result Page Message"
msgstr ""

#: inc/customizer/nbt-customizer.php:1388
msgctxt "Used in 404 Page Customizing"
msgid "404 Page Message"
msgstr ""

#: inc/customizer/nbt-customizer.php:1400
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Colors and Backgrounds"
msgstr ""

#: inc/customizer/nbt-customizer.php:1436
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Text Color"
msgstr ""

#: inc/customizer/nbt-customizer.php:1442
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Primary Theme Color"
msgstr ""

#: inc/customizer/nbt-customizer.php:1448
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Top Bar Background"
msgstr ""

#: inc/customizer/nbt-customizer.php:1454
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Footer Widget Area Background"
msgstr ""

#: inc/customizer/nbt-customizer.php:1460
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Footer Widget Area Background Image"
msgstr ""

#: inc/customizer/nbt-customizer.php:1466
msgctxt "Used in Colors and Backgrounds Customizing"
msgid "Footer Background"
msgstr ""

#: inc/customizer/nbt-customizer.php:1470
msgctxt "Used in Custom code Customizing"
msgid "Custom code"
msgstr ""

#: inc/customizer/nbt-customizer.php:1486
msgctxt "Used in Custom code Customizing"
msgid "Custom CSS"
msgstr ""

#: inc/customizer/nbt-customizer.php:1493
msgctxt "Used in Custom code Customizing"
msgid "Custom JavaScript"
msgstr ""

#: inc/customizer/nbt-customizer.php:1494
msgctxt "Used in Custom code Customizing"
msgid "Google Analytics (or other) tracking code"
msgstr ""

#: inc/featured-slider/featured.php:42 templates/template-controller.php:228
msgid "read more"
msgstr ""

#: inc/related-post/related.php:31
msgid "Related Posts"
msgstr ""

#: inc/related-post/related.php:32
msgid "You might also like"
msgstr ""

#: inc/widgets/widget-about-me.php:26
msgid "Nobrand About Me"
msgstr ""

#: inc/widgets/widget-about-me.php:29
msgid "A widget that displays your personal information."
msgstr ""

#: inc/widgets/widget-about-me.php:139 inc/widgets/widget-contact-us.php:84
#: inc/widgets/widget-instagram-feed.php:146 inc/widgets/widget-posts.php:154
#: inc/widgets/widget-social-media.php:117
msgid "Title:"
msgstr ""

#: inc/widgets/widget-about-me.php:143 inc/widgets/widget-promo-box.php:99
msgid "Image:"
msgstr ""

#: inc/widgets/widget-about-me.php:148 inc/widgets/widget-promo-box.php:103
msgid "Upload image"
msgstr ""

#: inc/widgets/widget-about-me.php:152 inc/widgets/widget-promo-box.php:107
msgid "Specify URL"
msgstr ""

#: inc/widgets/widget-about-me.php:157 inc/widgets/widget-promo-box.php:112
msgid "Choose File"
msgstr ""

#: inc/widgets/widget-about-me.php:172 inc/widgets/widget-promo-box.php:126
msgid "Link to page:"
msgstr ""

#: inc/widgets/widget-about-me.php:178 inc/widgets/widget-promo-box.php:132
msgid "Open in new page"
msgstr ""

#: inc/widgets/widget-about-me.php:182 inc/widgets/widget-promo-box.php:136
msgid "Text:"
msgstr ""

#: inc/widgets/widget-about-me.php:186 inc/widgets/widget-social-media.php:121
msgid "Facebook URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:190 inc/widgets/widget-social-media.php:125
msgid "Twitter URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:194 inc/widgets/widget-social-media.php:129
msgid "Instagram URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:198 inc/widgets/widget-social-media.php:133
msgid "Pinterest URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:202 inc/widgets/widget-social-media.php:137
msgid "Google+ URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:206 inc/widgets/widget-social-media.php:141
msgid "YouTube URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:210 inc/widgets/widget-social-media.php:145
msgid "RSS URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:214 inc/widgets/widget-social-media.php:149
msgid "Flickr URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:218 inc/widgets/widget-social-media.php:153
msgid "Tumblr URL:"
msgstr ""

#: inc/widgets/widget-about-me.php:222 inc/widgets/widget-social-media.php:157
msgid "LinkedIn URL:"
msgstr ""

#: inc/widgets/widget-contact-us.php:27
msgid "Nobrand Contact Us"
msgstr ""

#: inc/widgets/widget-contact-us.php:30
msgid ""
"A widget that displays WordPress Contact Form 7 so no shortcodes are needed."
msgstr ""

#: inc/widgets/widget-contact-us.php:88
msgid "Contact Form 7:"
msgstr ""

#: inc/widgets/widget-contact-us.php:102
msgid "There are no forms available"
msgstr ""

#: inc/widgets/widget-facebook.php:28
msgid "Nobrand Like Box"
msgstr ""

#: inc/widgets/widget-facebook.php:31
msgid "A widget that displays a facebook feed."
msgstr ""

#: inc/widgets/widget-facebook.php:96
msgid "Facebook profile URL:"
msgstr ""

#: inc/widgets/widget-facebook.php:100
msgid "Show faces"
msgstr ""

#: inc/widgets/widget-facebook.php:103
msgid "Show stream"
msgstr ""

#: inc/widgets/widget-instagram-feed.php:28
msgid "Nobrand Instagram Feed"
msgstr ""

#: inc/widgets/widget-instagram-feed.php:31
msgid "A widget that displays your latest Instagram photos."
msgstr ""

#: inc/widgets/widget-instagram-feed.php:150
#: inc/widgets/widget-instagram-footer.php:316
msgid "Access token:"
msgstr ""

#: inc/widgets/widget-instagram-feed.php:151
#: inc/widgets/widget-instagram-footer.php:317
msgid "How to get?"
msgstr ""

#: inc/widgets/widget-instagram-feed.php:155
msgid "Images count:"
msgstr ""

#: inc/widgets/widget-instagram-feed.php:159
msgid "Columns:"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:29
msgid "Nobrand Instagram Feed *"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:32
msgid ""
"A widget that displays your latest Instagram photos. Only for \"Instagram "
"Feed *\" area."
msgstr ""

#: inc/widgets/widget-instagram-footer.php:322
msgid "Image size:"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:325
msgid "Big"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:329
msgid "Small"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:333
msgid "Lines:"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:336
msgid "Top"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:340
msgid "Bottom"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:344
msgid "Label text:"
msgstr ""

#: inc/widgets/widget-instagram-footer.php:351
msgid "Slider"
msgstr ""

#: inc/widgets/widget-posts.php:28
msgid "Nobrand Posts"
msgstr ""

#: inc/widgets/widget-posts.php:31
msgid "A widget that displays your posts."
msgstr ""

#: inc/widgets/widget-posts.php:158
msgid "Sort:"
msgstr ""

#: inc/widgets/widget-posts.php:160
msgid "Popular posts"
msgstr ""

#: inc/widgets/widget-posts.php:161
msgid "Recent posts"
msgstr ""

#: inc/widgets/widget-posts.php:162
msgid "Random posts"
msgstr ""

#: inc/widgets/widget-posts.php:168
msgid "Show photo"
msgstr ""

#: inc/widgets/widget-posts.php:174
msgid "Show date"
msgstr ""

#: inc/widgets/widget-posts.php:180
msgid "Show author name"
msgstr ""

#: inc/widgets/widget-posts.php:184
msgid "Count posts:"
msgstr ""

#: inc/widgets/widget-promo-box.php:27
msgid "Nobrand Promo Box"
msgstr ""

#: inc/widgets/widget-promo-box.php:30
msgid "A widget that displays your promo image."
msgstr ""

#: inc/widgets/widget-social-media.php:28
msgid "Nobrand Social Media"
msgstr ""

#: inc/widgets/widget-social-media.php:31
msgid "A widget that displays links to social profiles."
msgstr ""

#: index.php:240
msgid "Nothing Found!"
msgstr ""

#: index.php:245
#, php-format
msgid ""
"It seems we cant find what youre looking for. %s Perhaps searching can help."
msgstr ""

#: search.php:26
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: search.php:230
msgid "Post not found!"
msgstr ""

#: search.php:235
#, php-format
msgid ""
"Sorry, but nothing matched your search terms.%s Please try again with some "
"different keywords."
msgstr ""

#: searchform.php:10
msgid "Search and hit enter..."
msgstr ""

#: single.php:36
#, php-format
msgid "%s Next Post"
msgstr ""

#: single.php:37
#, php-format
msgid "Prev Post %s"
msgstr ""

#: templates/content-grid.php:36 templates/content-list.php:56
#: templates/content-zigzag.php:37
msgid "There is no excerpt because this is a protected post."
msgstr ""

#: templates/content-grid.php:45 templates/content-list.php:64
#: templates/content-zigzag.php:46 templates/content.php:54
msgid "Page"
msgstr ""

#: templates/content.php:45
#, php-format
msgid "%sTags%s"
msgstr ""

#: templates/template-controller.php:93
msgid "Sticky"
msgstr ""

#: templates/template-controller.php:97
msgctxt "Used before category names."
msgid "Categories"
msgstr ""

#: templates/template-controller.php:137
msgctxt "Used before post author name."
msgid "Author"
msgstr ""

#: templates/template-controller.php:147
#, php-format
msgid "No Comments %s on %s %s"
msgstr ""

#: templates/template-controller.php:147
#, php-format
msgid "%d Comment %s on %s %s"
msgstr ""

#: templates/template-controller.php:147
#, php-format
msgid "%d Comments %s on %s %s"
msgstr ""

#: templates/template-controller.php:162
msgid "Comment navigation"
msgstr ""

#: templates/template-controller.php:166
#, php-format
msgid "Older Comments %s"
msgstr ""

#: templates/template-controller.php:170
#, php-format
msgid "%s Newer Comments"
msgstr ""

#: templates/template-controller.php:199
msgid "Reply"
msgstr ""

#: templates/template-controller.php:203
msgid "Edit"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Rococo"
msgstr ""

#. Theme URI of the plugin/theme
msgid "demo.nobrand.team/rococo"
msgstr ""

#. Description of the plugin/theme
msgid "WordPress Theme for Bloggers"
msgstr ""

#. Author of the plugin/theme
msgid "Nobrand"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.nobrand.team/"
msgstr ""
