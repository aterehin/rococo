<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Nobrand
 */

if ( ! function_exists( 'rococo_post_media' ) ) :
	/**
	 * Displays the media content attached to the post.
	 */
	function rococo_post_media() {
		if ( has_post_format( 'video' ) ) {
			echo '<div class="post__media">';
			$s_video = get_post_meta( get_the_ID(), '_format_video_embed', true );
			if ( wp_oembed_get( $s_video ) ) {
				echo wp_oembed_get( $s_video );
			} else {
				echo $s_video;
			}
			echo '</div>';
		} elseif ( has_post_format( 'audio' ) ) {
			echo '<div class="post__media">';
			$s_audio = get_post_meta( get_the_ID(), '_format_audio_embed', true );
			if ( wp_oembed_get( $s_audio ) ) {
				echo wp_oembed_get( $s_audio );
			} else {
				echo $s_audio;
			}
			echo '</div>';
		} elseif ( has_post_format( 'gallery' ) ) {
			$images = get_post_meta( get_the_ID(), '_format_gallery_images', true );
			if ( is_array( $images ) ) {
				echo '<div class="slider-wrapper">';
				echo '<div class="post__slider">';
				foreach ( $images as $image ) :
					$the_image = wp_get_attachment_image_src( $image, 'rococo_base_thumb' );
					$the_preview = wp_get_attachment_image_src( $image, 'thumbnail' );
					$the_caption = get_post_field( 'post_excerpt', $image ); ?>
					<img class="slide__featured" data-thumbnail="<?php echo esc_url( $the_preview[0] )?>" src="<?php echo esc_url( $the_image[0] ) ?>" title="<?php echo esc_attr( $the_caption ? $the_caption : '' ) ?>"/>
				<?php endforeach;
				echo '</div></div>';
			}
		};
	}
endif;

if ( ! function_exists( 'rococo_post_thumbnail' ) ) :
	/**
	 * Displays post thumbnail.
	 *
	 * @param string $thumb_size Original size.
	 *
	 * @return string Post thumbnail html
	 */
	function rococo_post_thumbnail( $thumb_size ) {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return false;
		}

		if ( is_singular() ) :
			?>

			<div class="post__thumbnail">
				<?php the_post_thumbnail( $thumb_size, array( 'alt' => get_the_title() ) ); ?>
			</div>

		<?php else : ?>

			<a class="post__thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
                <?php the_post_thumbnail( $thumb_size, array( 'alt' => get_the_title() ) );?>
            </a>

		<?php endif;
		return false;
	}
endif;

if ( ! function_exists( 'rococo_post_meta' ) ) :
	/**
	 * Displays post category.
	 *
	 * @return string Post category html
	 */
	function rococo_post_meta() {
		if ( ! get_theme_mod( 'post->parts=>categories', true ) ) {
			return;
		}

		if ( is_sticky() && is_home() && ! is_paged() ) {
			printf( '<div class="featured"><span class="featured__label">%s</span></div>', esc_html__( 'Sticky', 'rococo' ) );
		} else if ( 'post' == get_post_type() ) {
			$categories_list = get_the_category_list( ' &#149; ' );
			if ( $categories_list ) {
				printf( '<div class="post__category"><span class="screen-reader-text">%1$s </span>%2$s</div>', esc_html_x( 'Categories', 'Used before category names.', 'rococo' ), wp_kses( $categories_list, array( 'a' => array( 'href' => array() ) ) ) );
			}
		}
	}
endif;

if ( ! function_exists( 'rococo_post_info' ) ) :
	/**
	 * Displays post date|author|comments.
	 *
	 * @param boolean $date Enable or disable the display of the date of publication.
	 * @param boolean $author Enable or disable the display of the author's name.
	 * @param boolean $comments Enable or disable the display of the number of comments.
	 */
	function rococo_post_info( $date, $author, $comments ) {
		if ( $date ) {
			$date = get_theme_mod( 'post->parts=>date', true ) ? true : false;
		}
		if ( $author ) {
			$author = get_theme_mod( 'post->parts=>author', true ) ? true : false;
		}
		if ( $comments ) {
			$comments = get_theme_mod( 'post->parts=>comments-number', true ) ? true : false;
		}

		if ( 'post' == get_post_type() ) {
			if ( $date ) {
				$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

				$time_string = sprintf( $time_string,
					esc_attr( get_the_date( 'c' ) ),
					get_the_date()
				);
				echo '<div class="post__date"><a href="' . esc_url( get_permalink() ) . '">';
				echo sprintf( wp_kses( $time_string, array( 'time' => array( 'class' => array(), 'datetime' => array() ) ) ), esc_attr( get_the_date( 'c' ) ), get_the_date() );
				echo '</a></div>';
			}

			if ( $author ) {
				printf( '<div class="post__author"><span class="screen-reader-text">%1$s </span><a href="%2$s">%3$s</a></div>',
					esc_html_x( 'Author', 'Used before post author name.', 'rococo' ),
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					get_the_author()
				);
			}
		}

		if ( $comments ) {
			if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
				echo '<div class="post__comments">';
				comments_popup_link( sprintf( esc_html__( 'No Comments %s on %s %s', 'rococo' ), '<span class="screen-reader-text">', get_the_title(), '</span>' ), sprintf( esc_html__( '%d Comment %s on %s %s', 'rococo' ), '1', '<span class="screen-reader-text">', get_the_title(), '</span>' ), sprintf( esc_html__( '%d Comments %s on %s %s', 'rococo' ), get_comments_number(), '<span class="screen-reader-text">', get_the_title(), '</span>' ) );
				echo '</div>';
			}
		}
	}
endif;

if ( ! function_exists( 'rococo_comment_nav' ) ) :
	/**
	 * Display navigation to next/previous comment when applicable.
	 */
	function rococo_comment_nav() {
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
			?>
			<nav class="navigation comment-navigation" role="navigation">
				<h4 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'rococo' ); ?></h4>

				<div class="nav-links">
					<?php
					if ( $prev_link = get_previous_comments_link( sprintf( esc_html__( 'Older Comments %s', 'rococo' ), '<i class="fa fa-angle-double-right"></i>' ) ) ) :
						printf( '<div class="nav-previous">%s</div>', wp_kses( $prev_link, array( 'a' => array( 'href' => array() ) ) ) );
					endif;

					if ( $next_link = get_next_comments_link( sprintf( esc_html__( '%s Newer Comments', 'rococo' ), '<i class="fa fa-angle-double-left"></i>' ) ) ) :
						printf( '<div class="nav-next">%s</div>', wp_kses( $next_link, array( 'a' => array( 'href' => array() ) ) ) );
					endif;
					?>
				</div>
			</nav>
		<?php
		endif;
	}
endif;

if ( ! function_exists( 'rococo_comment_view' ) ) :
	/**
	 * The function returns the html form comments.
	 *
	 * @param array  $comment comments array.
	 * @param array  $args comments option.
	 * @param number $depth comment depth.
	 */
	function rococo_comment_view( $comment, $args, $depth ) {
		?>
		<li <?php comment_class( 'comments-list__item' ); ?> id="comment-<?php comment_ID() ?>">
			<div class="comment">
				<div class="comment__avatar">
					<?php echo get_avatar( $comment ); ?>
				</div>
				<div class="comment__content">
					<div class="comment__reply">
						<?php comment_reply_link( array(
							'reply_text' => esc_html__( 'Reply', 'rococo' ),
							'depth'      => $depth,
							'max_depth'  => $args['max_depth'],
						),  $comment->comment_ID ); ?>
						<?php edit_comment_link( esc_html__( 'Edit', 'rococo' ) ); ?>
					</div>
					<h4 class="comment__author">
						<?php echo get_comment_author_link(); ?>
					</h4>

					<div class="comment__date">
						<?php comment_date( 'F j, Y' ); ?>
					</div>
					<p class="comment__text">
                        <?php echo wp_kses_post( apply_filters( 'comment_text', get_comment_text() ) ) ?>
                    </p>
				</div>
			</div>
		</li>
	<?php
	}
endif;

if ( ! function_exists( 'rococo_post_more' ) ) :
	/**
	 * Customize Read More link
	 */
	function rococo_post_more() {
		if ( ! is_single() && get_theme_mod( 'post->parts=>read-more', true ) && ! strpos( get_post()->post_content, '<!--nextpage-->' ) ) {
			echo '<div class="post__more"><a href="' . esc_url( get_permalink() ) . '" class="btn">' . esc_html__( 'read more', 'rococo' ) . '</a></div>';
		}
	}
endif;
