<?php
/**
 * Template part for displaying posts.
 * Quote layout
 *
 * @package Nobrand
 */

?>
<div class="table-cell-layer">
	<div class="post__header">
		<?php rococo_post_meta(); ?>
	</div>

	<div class="post__content">
		<i class="fa fa-quote-left"></i>
		<?php the_content(); ?>
	</div>

	<div class="post__info">
		<?php rococo_post_info( true, false, false ); // Date, Author, Comments. ?>
	</div>
</div>
