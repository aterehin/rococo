<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Nobrand
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
	<div class="post__header">
		<?php rococo_post_meta(); ?>

		<h1 class="post__title"><?php the_title() ?></h1>

	</div>

	<?php rococo_post_thumbnail( 'original' ); ?>

	<?php if ( rococo_have_content( get_the_content() ) ) : ?>
		<div class="post__content">

			<?php the_content(); ?>

		</div>
	<?php endif; ?>
</article>
