<?php
/**
 * Template part for displaying posts.
 * Grid layout
 *
 * @package Nobrand
 */

	$post_class = array(
		'post',
		'_type-grid',
		! has_post_thumbnail() ? '_no-image' : '',
	);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>
	<?php rococo_post_thumbnail( 'rococo_medium_thumb' ); ?>

	<div class="content-box-overlay">
		<div class="post__header">
			<?php rococo_post_meta(); ?>

			<h2 class="post__title"><a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"><?php the_title() ?></a></h2>

		</div>

		<?php if ( rococo_have_content( get_the_content() ) ) : ?>
			<div class="post__content">
				<?php if ( ! post_password_required() ) {
					if ( str_word_count( strip_tags( get_the_content( '' ) ) ) < 34 && strpos( get_post()->post_content, '<!--more-->' ) ) {
						echo esc_html( get_the_excerpt() ) . '...';
					} else {
						echo esc_html( wp_trim_words( get_the_excerpt(), 34, '...' ) );
					}
				} else {
					esc_html_e( 'There is no excerpt because this is a protected post.', 'rococo' );
				}?>
			</div>

			<?php wp_link_pages( array(
				'before'      => '<div class="page-links">',
				'after'       => '</div>',
				'link_before' => '<div class="page-links__item">',
				'link_after'  => '</div>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rococo' ) . '</span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		endif; ?>

		<div class="post__info">
			<?php rococo_post_info( true, false, false ); // Date, Author, Comments. ?>
		</div>
	</div>
</article>
