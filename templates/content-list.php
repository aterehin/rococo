<?php
/**
 * Template part for displaying posts.
 * List layout
 *
 * @package Nobrand
 */

$post_class = array(
	'post',
	'_type-list',
	! has_post_thumbnail() ? '_no-image' : '',
);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>
	<div class="row">
		<?php if ( has_post_thumbnail() ) : ?>

		<?php if ( ( is_front_page() && ! get_theme_mod( 'layout=>index-sidebar', true ) ) || ( is_archive() && ! get_theme_mod( 'layout=>archive-sidebar', true ) ) || ( is_search() && ! get_theme_mod( 'layout=>search-sidebar', true ) ) ) {
			$add_class = 'col-md-4';
			$add_class2 = 'col-md-8';
		};?>
		<div class="col-sm-6 <?php echo esc_attr( $add_class ) ?>">
			<?php rococo_post_thumbnail( 'rococo_base_thumb' ); ?>
		</div>

		<div class="col-sm-6 <?php echo esc_attr( $add_class2 )?>">
			<?php else : ?>
			<div class="col-sm-12">
				<?php endif ?>
				<div class="post__header">

					<?php rococo_post_meta(); ?>
					<h2 class="post__title"><a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"><?php the_title() ?></a></h2>

				</div>

				<?php if ( rococo_have_content( get_the_content() ) ) : ?>
					<div class="post__content">
						<?php if ( ! post_password_required() ) {
							if ( ! has_post_thumbnail() ) {
								if ( str_word_count( strip_tags( get_the_content( '' ) ) ) < 100 && strpos( get_post()->post_content, '<!--more-->' ) ) {
									echo esc_html( get_the_excerpt() ) . '...';
								} else {
									echo esc_html( wp_trim_words( get_the_excerpt(), 100, '...' ) );
								}
							} else {
								if ( str_word_count( strip_tags( get_the_content( '' ) ) ) < 30 && strpos( get_post()->post_content, '<!--more-->' ) ) {
									echo esc_html( get_the_excerpt() ) . '...';
								} else {
									echo esc_html( wp_trim_words( get_the_excerpt(), 30, '...' ) );
								}
							}
						} else {
							esc_html_e( 'There is no excerpt because this is a protected post.', 'rococo' );
						}?>
					</div>
					<?php wp_link_pages( array(
						'before'      => '<div class="page-links">',
						'after'       => '</div>',
						'link_before' => '<div class="page-links__item">',
						'link_after'  => '</div>',
						'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rococo' ) . '</span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					) );
				endif; ?>

				<div class="post__info">
					<?php rococo_post_info( true, false, false ); // Date, Author, Comments. ?>
				</div>
			</div>
		</div>
</article>
