<?php
/**
 * Template part for displaying posts.
 * Grid layout
 *
 * @package Nobrand
 */

$post_class = array(
	'_type-apposition',
	! has_post_thumbnail() ? '_no-image' : '',
);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>

	<div class="apposition-image">
		<?php rococo_post_thumbnail( 'rococo_base_thumb' );?>
	</div>

	<div class="apposition-content">
		<div class="post__header">
			<?php rococo_post_meta(); ?>

			<h2 class="post__title"><a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"><?php the_title() ?></a></h2>

			<div class="post__info">
				<?php rococo_post_info( true, true, true ); // Date, Author, Comments. ?>
			</div>
		</div>

		<?php if ( rococo_have_content( get_the_content() ) ) : ?>
			<div class="post__content">
				<?php if ( ! post_password_required() ) {
					echo esc_html( wp_trim_words( strip_shortcodes( get_the_content() ), 50 ) );
				} else {
					esc_html_e( 'There is no excerpt because this is a protected post.', 'rococo' );
				}?>
			</div>

			<?php wp_link_pages( array(
				'before'      => '<div class="page-links">',
				'after'       => '</div>',
				'link_before' => '<div class="page-links__item">',
				'link_after'  => '</div>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rococo' ) . '</span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		endif;?>

		<?php if ( str_word_count( strip_tags( get_the_content( '' ) ) ) > 50 ) {
			rococo_post_more();
		}?>
	</div>

</article>
