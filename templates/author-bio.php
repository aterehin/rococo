<?php
/**
 * Display author information.
 *
 * @package Nobrand
 */

?>
<div class="post__author-bio">
	<div class="row">
		<div class="col-sm-2 hidden-xs">
			<?php echo get_avatar( get_the_author_meta( 'email' ), '' ) ?>
		</div>
		<div class="col-sm-10 col-xs-12">
			<h4 class="author__title"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author(); ?></a></h4>
			<p class="author__biographical"><?php the_author_meta( 'description' ); ?></p>
			<ul class="social-list _in-author-bio">
				<?php
				if ( get_the_author_meta( 'facebook' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://www.facebook.com/' . get_the_author_meta( 'facebook' ) ) . '"><i class="fa fa-facebook"></i></a></li>';
				}
				if ( get_the_author_meta( 'twitter' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://twitter.com/' . get_the_author_meta( 'twitter' ) ) . '"><i class="fa fa-twitter"></i></a></li> ';
				}
				if ( get_the_author_meta( 'instagram' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://instagram.com/' . get_the_author_meta( 'instagram' ) ) . '"><i class="fa fa-instagram"></i></a></li> ';
				}
				if ( get_the_author_meta( 'pinterest' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://pinterest.com/' . get_the_author_meta( 'pinterest' ) ) . '"><i class="fa fa-pinterest"></i></a></li> ';
				}
				if ( get_the_author_meta( 'google' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://plus.google.com/' . get_the_author_meta( 'google' ) ) . '"><i class="fa fa-google-plus"></i></a></li> ';
				}
				if ( get_the_author_meta( 'tumblr' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( 'http://' . get_the_author_meta( 'tumblr' ) . '.tumblr.com/' ) . '"><i class="fa fa-tumblr"></i></a></li> ';
				}
				if ( get_the_author_meta( 'linkedin' ) !== '' ) {
					echo '<li class="social-list__item"><a target="_blank" href="' . esc_url( get_the_author_meta( 'linkedin' ) ) . '"><i class="fa fa-linkedin"></i></a></li>';
				}?>
			</ul>
		</div>
	</div>
</div>
