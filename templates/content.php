<?php
/**
 * Template part for displaying posts.
 *
 * @package Nobrand
 */

$post_class = array(
	'post',
	! has_post_thumbnail() ? '_no-image' : '',
);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>

	<?php rococo_post_thumbnail( 'rococo_base_thumb' ); ?>

	<div class="content-box-overlay">

		<div class="content-box">
			<div class="post__header">
				<?php rococo_post_meta(); ?>

				<?php if ( is_single() ) : ?>
					<h1 class="post__title"><?php the_title() ?></h1>
				<?php else : ?>
					<h2 class="post__title"><a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"><?php the_title() ?></a></h2>
				<?php endif ?>

				<div class="post__info">
					<?php rococo_post_info( true, true, true ); // Date, Author, Comments. ?>
				</div>

			</div>

			<?php rococo_post_media(); ?>

			<?php if ( rococo_have_content( get_the_content() ) || is_attachment() ) : ?>
				<div class="post__content">
					<?php the_content( '' ); ?>
				</div>

				<?php if ( is_single() && get_theme_mod( 'post->parts=>tags', true ) ) : ?>
					<dl class="post__tags">
						<?php the_tags( sprintf( esc_html__( '%sTags%s', 'rococo' ), '<dt>', ':</dt>' ) .' <dd>', ',</dd> <dd>', '</dd>' );?>
					</dl>
				<?php endif; ?>

				<?php wp_link_pages( array(
					'before'      => '<div class="page-links">',
					'after'       => '</div>',
					'link_before' => '<div class="page-links__item">',
					'link_after'  => '</div>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rococo' ) . '</span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			endif;?>

			<?php if ( get_theme_mod( 'post->socials=>enable', true ) ) : ?>
				<div class="post__share">
					<ul class="social-list _in-post">
						<?php if ( get_theme_mod( 'post->socials=>facebook-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink() ) ?>"
								   target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>twitter-link', true ) ) : ?>
							<?php
							$tags     = get_the_tags();
							$tags_str = '';
							if ( $tags ) {
								foreach ( $tags as $tag ) {
									$tags_str = $tags_str . $tag->name . ', ';
								}
							}
							?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://twitter.com/share?text=' . get_the_title() . '&url=' . get_the_permalink() . ( ! empty( $tags_str ) ? '&hashtags=' . $tags_str : '' ) ) ?>"
								   target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>pinterest-link', true ) ) : ?>
							<li class="social-list__item">
								<?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) );
								$thumbnail_utl   = $thumbnail['0']; ?>
								<a href="<?php echo esc_url( 'http://pinterest.com/pin/create/bookmarklet/?media=' . $thumbnail_utl . '&url=' . get_the_permalink() . '&is_video=false&description=' . get_the_title() ) ?>"
								   target="_blank"><i class="fa fa-pinterest"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>google-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'https://plus.google.com/share?url=' . get_the_permalink() ) ?>"
								   target="_blank"><i class="fa fa-google-plus"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>tumblr-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://www.tumblr.com/share?v=3&u=' . get_the_permalink() . '&t=' . get_the_title() ) ?>"
								   target="_blank"><i class="fa fa-tumblr"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>linkedin-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() . '&title=' . get_the_title() . '&source=' . home_url( '/' ) ) ?>"
								   target="_blank"><i class="fa fa-linkedin"></i></a>
							</li>
						<?php endif; ?>

						<?php if ( get_theme_mod( 'post->socials=>stumbleupon-link', true ) ) : ?>
							<li class="social-list__item">
								<a href="<?php echo esc_url( 'http://www.stumbleupon.com/submit?url=' . get_the_permalink() . '&title=' . get_the_title() ) ?>"
								   target="_blank"><i class="fa fa-stumbleupon"></i></a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			<?php endif; ?>

			<?php if ( strpos( get_post()->post_content, '<!--more-->' ) ) {
				rococo_post_more();
			}?>
		</div>
	</div>

	<?php if ( is_single() && get_theme_mod( 'post->parts=>author-bio', true ) && get_the_author_meta( 'description' ) !== '' ) :
		get_template_part( 'templates/author-bio' );
	endif; ?>

</article>
