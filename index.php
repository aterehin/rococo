<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Nobrand
 * @version 1.1.0
 */

?>
<?php get_header(); ?>

<?php if ( ! get_theme_mod( 'layout=>index-featured-slider', true ) && ! get_theme_mod( 'layout=>index-popular-posts', true ) ) {
	echo '<hr class="delimiter">';
}?>

<?php if ( get_theme_mod( 'layout=>index-featured-slider', true ) ) {
	get_template_part( 'inc/featured-slider/featured' );
}
?>
	<div class="container">
		<?php if ( get_theme_mod( 'layout=>index-popular-posts', true ) ) :
			get_template_part( 'inc/popular-posts/popular-posts' );
		endif; ?>

		<div <?php if ( get_theme_mod( 'layout=>index-sidebar', 'right' ) !== 'disable' ) : ?> class="content" <?php else : ?> class="content _full-width" <?php endif; ?>>

			<div id="main" class="content__primary <?php if ( get_theme_mod( 'layout=>index-sidebar', 'right' ) === 'left' ) echo '_align-right'; ?>">
				<?php if ( have_posts() ) {
					global $wp_query;
					/**
					 * Apposition post style
					 */
					if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'zigzag' ) {
						echo '<div class="apposition-wrapper '. esc_attr( get_theme_mod( 'layout=>index-sidebar', 'right' ) !== 'disable' ? '_with-sidebar' : '' ) .'">';
						while ( have_posts() ) {
							the_post();
							if ( has_post_format( 'quote' ) ) {
								echo '<article class="post _format-quote _type-apposition" id="post-' . esc_attr( get_the_ID() ) . '">';
								get_template_part( 'templates/content', 'quote' );
								echo '</article>';
							} else {
								get_template_part( 'templates/content', 'zigzag' );
							}
						}
						echo '</div>';
					} else {
						/**
						 * Post style 1 standard / 1 list
						 */
						if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard&list' ) {
							$count = 0;
							while ( have_posts() ) {
								$count++;
								the_post();

								$x = 0;
								if ( has_post_format( 'quote' ) ) {
									echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
									get_template_part( 'templates/content', 'quote' );
									echo '</article>';
								} else {
									get_template_part( 'templates/content' );
								}

								while ( $x++ < 1 && $count++ < $wp_query->post_count ) {
									the_post();
									if ( has_post_format( 'quote' ) ) {
										echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
										get_template_part( 'templates/content', 'quote' );
										echo '</article>';
									} else {
										get_template_part( 'templates/content', 'list' );
									}
								}
							}
						} /**
						  * Post style 1 standard / 2 grid or 3 grid if sidebar hidden
						  */
						else if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard&grid' ) {
							$count = 0;
							$grid_per_line = get_theme_mod( 'layout=>index-sidebar', 'right' ) !== 'disable' ? 2 : 3;
							while ( have_posts() ) {
								$count++;
								the_post();

								$x = 0;
								if ( has_post_format( 'quote' ) ) {
									echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
									get_template_part( 'templates/content', 'quote' );
									echo '</article>';
								} else {
									get_template_part( 'templates/content' );
								}

								if ( $count < $wp_query->post_count ) {
									echo '<ul class="grid-list">';
									while ( $x++ < $grid_per_line && $count++ < $wp_query->post_count ) {
										the_post();
										echo '<li class="grid-list__item">';
										if ( has_post_format( 'quote' ) ) {
											echo '<article class="post _type-grid _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
											get_template_part( 'templates/content', 'quote' );
											echo '</article>';
										} else {
											get_template_part( 'templates/content', 'grid' );
										}
										echo '</li>';
									}
									echo '</ul>';
								}
							}
						} /**
						  * Post style 1 list / 2 grid or 3 grid if sidebar hidden
						  */
						else if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'grid&list' ) {
							$count = 0;
							$grid_per_line = get_theme_mod( 'layout=>index-sidebar', 'right' ) !== 'disable' ? 2 : 3;
							while ( have_posts() ) {
								$count++;
								the_post();

								$x = 0;
								if ( has_post_format( 'quote' ) ) {
									echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
									get_template_part( 'templates/content', 'quote' );
									echo '</article>';
								} else {
									get_template_part( 'templates/content', 'list' );
								}

								if ( $count < $wp_query->post_count ) {
									echo '<ul class="grid-list">';
									while ( $x++ < $grid_per_line && $count++ < $wp_query->post_count ) {
										the_post();
										echo '<li class="grid-list__item">';
										if ( has_post_format( 'quote' ) ) {
											echo '<article class="post _type-grid _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
											get_template_part( 'templates/content', 'quote' );
											echo '</article>';
										} else {
											get_template_part( 'templates/content', 'grid' );
										}
										echo '</li>';
									}
									echo '</ul>';
								}
							}
						} else {
							/**
							 * Differ first post, if post style list or grid
							 */
							if ( ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard_first&grid'  || get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard_first&list' ) && ! is_paged() ) {
								the_post();
								if ( has_post_format( 'quote' ) ) {
									echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
									get_template_part( 'templates/content', 'quote' );
									echo '</article>';
								} else {
									get_template_part( 'templates/content' );
								}
							}
							/**
							 * Grid post style
							 */
							if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'grid' || get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard_first&grid' ) {
								echo '<ul class="grid-list">';
								while ( have_posts() ) {
									the_post();

									echo '<li class="grid-list__item">';
									if ( has_post_format( 'quote' ) ) {
										echo '<article class="post _type-grid _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
										get_template_part( 'templates/content', 'quote' );
										echo '</article>';
									} else {
										get_template_part( 'templates/content', 'grid' );
									}
									echo '</li>';
								}
								echo '</ul>';
							} /**
							   * List post style
							   */
							else if ( get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'list' || get_theme_mod( 'layout=>index-post-style', 'standard_first&grid' ) === 'standard_first&list' ) {
								while ( have_posts() ) {
									the_post();

									if ( has_post_format( 'quote' ) ) {
										echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
										get_template_part( 'templates/content', 'quote' );
										echo '</article>';
									} else {
										get_template_part( 'templates/content', 'list' );
									}
								}
							} /**
							   * Standard post style
							   */
							else {
								while ( have_posts() ) {
									the_post();

									if ( has_post_format( 'quote' ) ) {
										echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
										get_template_part( 'templates/content', 'quote' );
										echo '</article>';
									} else {
										get_template_part( 'templates/content' );
									}
								}
							}
						}
					}

					/**
					 * Load pagination
					 */
					if ( get_theme_mod( 'pagination=>index', false ) === 'numeric' ) {
						rococo_pagination();
					} else {
						the_posts_navigation( array(
							'prev_text' => sprintf( esc_html__( 'Older Posts %s', 'rococo' ), '<i class="fa fa-angle-double-right"></i>' ),
							'next_text' => sprintf( esc_html__( '%s Newer Posts', 'rococo' ), '<i class="fa fa-angle-double-left"></i>' ),
						) );
					}
				} /**
				   * If post not found
				   */
				else {
					?>
					<div class="error">
						<div class="error__header">
							<div class="error__desc">
								<?php esc_html_e( 'Nothing Found!', 'rococo' ) ?>
							</div>
						</div>

						<div class="error__text">
							<?php echo wp_kses_data( get_theme_mod( 'messages=>index-text', sprintf( esc_html__( 'It seems we cant find what youre looking for. %s Perhaps searching can help.', 'rococo' ), '</br>' ) ) )?>
						</div>

						<?php if ( get_theme_mod( 'messages=>index-search', true ) ) : ?>
							<hr class="_in-404"/>
							<div class="error__search">
								<?php get_search_form() ?>
							</div>
						<?php endif; ?>
					</div>
				<?php } ?>
			</div>

			<?php if ( get_theme_mod( 'layout=>index-sidebar', 'right' ) !== 'disable' ) : ?>
				<div class="sidebar "><?php get_sidebar(); ?></div>
			<?php endif; ?>
		</div>
	</div>
<?php get_footer(); ?>
