=== Rococo ===
Theme Name: Rococo
Theme URI: demo.nobrand.team/rococo
Description: WordPress Theme for Bloggers
Author: Nobrand
Author URI: http://www.nobrand.team/
Version: 1.1
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, three-columns, right-sidebar, left-sidebar, fluid-layout , custom-menu, custom-colors, featured-images, post-formats, sticky-post, threaded-comments, translation-ready

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Choose zipped "rococo.zip" file.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to http://docs.nobrand.team/rococo/ for a guide on how to customize this theme.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.
