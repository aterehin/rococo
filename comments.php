<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Nobrand
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}?>

<div id="comments" class="comments">
	<?php if ( have_comments() ) : ?>

		<h4 class="comments__title"><?php echo esc_html( get_comments_number() . ' ' . _n( 'comment', 'comments', get_comments_number(), 'rococo' ) ) ?></h4>
		<ul class="comments-list">
			<?php
			wp_list_comments( array(
				'style'     => 'ul',
				'callback'  => 'rococo_comment_view',
				'max_depth' => 5,
			) );
			?>
		</ul>
		<?php rococo_comment_nav(); ?>
	<?php endif; ?>
	<?php
	$fields        = '<div class="row"><div class="form-group col-sm-6"><label for="author">' . esc_html__( 'Name', 'rococo' ) . '<span class="required">*</span></label><input class="form-control" type="text" id="author" name="author" required="required" aria-required="true"/></div> <div class="form-group col-sm-6"><label for="email">' . esc_html__( 'Email', 'rococo' ) . '<span class="required">*</span></label><input class="form-control" type="text" id="email" name="email" required="required" aria-required="true" aria-describedby="email-notes"/></div></div>';
	$comment_field = '<div class="form-group"><label for="comment">' . esc_html__( 'Comment', 'rococo' ) . '</label><textarea class="form-control" id="comment" name="comment" rows="5" cols="20" required="required" aria-required="true"></textarea></div>';
	$submit_filed  = '<div class="form-group">%1$s %2$s</div>';
	comment_form( array(
		'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
		'submit_field'         => $submit_filed,
		'comment_field'        => $comment_field,
		'comment_notes_before' => '',
		'comment_notes_after'  => '',
		'class_submit'         => 'btn',
		'title_reply'          => esc_html__( 'Leave a Reply', 'rococo' ),
		'title_reply_to'       => esc_html__( 'Leave a Reply to %s', 'rococo' ),
		'cancel_reply_link'    => esc_html__( 'Cancel reply', 'rococo' ),
		'label_submit'         => esc_html__( 'Post Comment', 'rococo' ),
	) ); ?>
</div>
