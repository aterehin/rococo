<?php
/**
 * The template for displaying all single posts.
 *
 * @package Nobrand
 * @version 1.1.0
 */

get_header();

if ( ! get_theme_mod( 'layout=>single-featured-slider', false ) ) {
	echo '<hr class="delimiter">';
}

if ( get_theme_mod( 'layout=>single-featured-slider', false ) ) {
	get_template_part( 'inc/featured-slider/featured' );
}?>

<div class="container">
	<div <?php if ( get_theme_mod( 'layout=>single-sidebar', 'right' ) !== 'disable' ) : ?> class="content" <?php else : ?> class="content _full-width" <?php endif; ?>>

		<div id="main" class="content__primary <?php if ( get_theme_mod( 'layout=>single-sidebar', 'right' ) === 'left' ) echo '_align-right'; ?>">
			<?php if ( have_posts() ) :
				while ( have_posts() ) {
					the_post();
					if ( has_post_format( 'quote' ) ) {
						echo '<article class="post _format-quote" id="post-' . esc_attr( get_the_ID() ) . '">';
						get_template_part( 'templates/content', 'quote' );
						echo '</article>';
					} else {
						get_template_part( 'templates/content' );
					}
				}
				if ( get_theme_mod( 'post->parts=>post-nav', true ) ) {
					the_post_navigation( array(
						'next_text' => sprintf( esc_html__( '%s Next Post', 'rococo' ), '<i class="fa fa-angle-double-left"></i>' ),
						'prev_text' => sprintf( esc_html__( 'Prev Post %s', 'rococo' ), '<i class="fa fa-angle-double-right"></i>' ),
					) );
				}
				if ( get_theme_mod( 'post->parts=>related', true ) ) {
					rococo_related_post();
				}
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			endif; ?>
		</div>

		<?php if ( get_theme_mod( 'layout=>single-sidebar', 'right' ) !== 'disable' ) : ?>
			<div class="sidebar"><?php get_sidebar(); ?></div>
		<?php endif; ?>

	</div>
</div>
<?php get_footer(); ?>
