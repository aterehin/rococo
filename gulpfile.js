var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var prefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var path = {
    css: ['./sass/style.scss*'],
    watch: './sass/**/*.*',
    maps: './maps/'
};

gulp.task('css', function () {
    gulp.src(path.css)
        .pipe(sourcemaps.init())
        .pipe(sass({
            'outputStyle': 'expanded',
            'indentType': 'tab',
            'indentWidth': 1
        }))
        .pipe(prefixer({
            browsers: ['> 10%', 'last 2 versions', 'ie 8']
        }))
        .pipe(sourcemaps.write(path.maps))
        .pipe(gulp.dest('./'))
});

gulp.task('watch', function () {
    watch(path.watch, function () {
        gulp.start('css')
    })
});

gulp.task('default', ['css', 'watch']);