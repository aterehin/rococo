<?php
/**
 * Template for displaying search forms in Rococo
 *
 * @package Nobrand
 */

?>
<form class="site-search" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input class="site-search__field" data-js="search-field" type="text" autocomplete="off" placeholder="<?php echo wp_kses_data( get_theme_mod( 'top-bar->search=>placeholder', esc_html__( 'Search and hit enter...', 'rococo' ) ) ) ?>" value="<?php echo get_search_query() ?>" name="s" id="s"/>
	<button class="site-search__button" type="submit" value="">
		<i class="fa fa-search"></i>
	</button>
</form>
