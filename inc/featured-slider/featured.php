<?php
/**
 * Featured slider template
 *
 * @package Nobrand
 * @version 1.1.0
 */

$rococo_af_posts = new Rococo_Articles_Filter_Class;
$query = $rococo_af_posts->query();

if ( $query->have_posts() ) :?>

	<?php if ( get_theme_mod( 'featured-slider=>model', 'box-with-preview' ) == 'box' ) :
		echo '<div class="container">';
	endif; ?>

	<div class="slider-wrapper">

		<?php if ( get_theme_mod( 'featured-slider=>model', 'box-with-preview' ) == 'box-with-preview' ) :
			echo '<div class="container">';
		endif; ?>

		<div class="loading"></div>
		<div class="entry-slider">

			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="slide">
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
					$preview     = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' ); ?>

					<div class="slide__featured" data-js="parallax" data-thumbnail="<?php echo esc_url( $preview[0] ) ?>" style="background-image: url('<?php echo esc_url( $image[0] ); ?>');"></div>
					<div class="_black_overlay" data-js="parallax-darken"></div>
					<div class="slide__content" data-js="parallax-lighten">
						<div class="slide__content__inner">
							<div class="slide__category"><?php echo wp_kses( get_the_category_list( '&nbsp; &nbsp;' ), array( 'a' => array( 'href' => array() ) ) ) ?></div>
							<h2 class="slide__title"><a href="<?php echo esc_url( get_permalink() ) ?>"><?php the_title(); ?></a></h2>
							<?php echo '<div class="slide__date"><a href="' . esc_url( get_permalink() ) . '">';
							echo get_the_date();
							echo '</a></div>';?>
							<a class="btn hidden-xs" href="<?php echo esc_url( get_permalink() ) ?>"><?php esc_html_e( 'read more', 'rococo' ) ?></a>
						</div>
					</div>
				</div>

			<?php endwhile;
			wp_reset_query();  // Restore global post data stomped by the_post().
			?>

			<?php if ( get_theme_mod( 'featured-slider=>model', 'box-with-preview' ) == 'box-with-preview' ) :
				echo '</div>';
			endif; ?>

		</div>
	</div>

	<?php if ( get_theme_mod( 'featured-slider=>model', 'box-with-preview' ) == 'box' ) :
		echo '</div>';
	endif; ?>

<?php else : ?>
	<hr class="delimiter">
<?php endif; ?>
