<?php
/**
 *  Rules for entry slider
 *  Settings in customizer featured-slider
 *
 *  @version 1.1.0
 */

function rococo_customize_js() {

	$f_nav_arrows = get_theme_mod( 'featured-slider=>nav-arrows', true ) && get_theme_mod( 'featured-slider=>amount', 6 ) > 1 ? 'true' : 'false';
	$f_nav_class = '';

	if ( ! wp_is_mobile() ) {
		if ( get_theme_mod( 'featured-slider=>nav-arrows-style', 'thumb-flip' ) == 'thumb-flip' ) {
			$f_nav_class = 'animate-nav _animate_thumb_flip';
		} else if ( get_theme_mod( 'featured-slider=>nav-arrows-style', 'thumb-flip' ) == 'circle-slide' ) {
			$f_nav_class = 'animate-nav _animate_circle_slide';
		} else if ( get_theme_mod( 'featured-slider=>nav-arrows-style', 'thumb-flip' ) == 'diamond' ) {
			$f_nav_class = 'animate-nav _animate_diamond';
		}
	}

	$f_nav_speed = get_theme_mod( 'featured-slider=>nav-arrows-speed', 900 );
	if ( ! is_numeric( $f_nav_speed ) && $f_nav_speed <= 0 ) {
		$f_nav_speed = 900;
	}

	$f_nav_dots = get_theme_mod( 'featured-slider=>nav-dots', false ) && get_theme_mod( 'featured-slider=>amount', 6 ) > 1 ? 'true' : 'false';
	$f_dots_class = '';

	if ( ! wp_is_mobile() ) {
		if ( get_theme_mod( 'featured-slider=>nav-dots-style', 'scale-up' ) == 'scale-up' ) {
			$f_dots_class = '_animate_scale_up';
		} else if ( get_theme_mod( 'featured-slider=>nav-dots-style', 'scale-up' ) == 'fill-up' ) {
			$f_dots_class = '_animate_fill_up';
		} else if ( get_theme_mod( 'featured-slider=>nav-dots-style', 'scale-up' ) == 'fall' ) {
			$f_dots_class = '_animate_fall';
		}
	}

	$f_dots_speed = get_theme_mod( 'featured-slider=>nav-dots-speed', 600 );
	if ( ! is_numeric( $f_dots_speed ) && $f_dots_speed <= 0 ) {
		$f_dots_speed = 600;
	}

	$f_autoplay = get_theme_mod( 'featured-slider=>auto-play', true ) ? 'true' : 'false';

	$f_autoplay_timeout = get_theme_mod( 'featured-slider=>auto-play-timeout', 7000 );
	if ( ! is_numeric( $f_autoplay_timeout ) && $f_autoplay_timeout <= 0 ) {
		$f_autoplay_timeout = 7000;
	}

	$f_autoplay_speed = get_theme_mod( 'featured-slider=>auto-play-speed', 900 );
	if ( ! is_numeric( $f_autoplay_speed ) && $f_autoplay_speed <= 0 ) {
		$f_autoplay_speed = 7000;
	}

	/**
	 *  Rules for post slider
	 *  Settings in customizer post->slider
	 */

	$p_nav_arrows = get_theme_mod( 'post->slider=>nav-arrows', true ) ? 'true' : 'false';
	$p_nav_class = '';

	if ( ! wp_is_mobile() ) {
		if ( get_theme_mod( 'post->slider=>nav-arrows-style', 'thumb-flip' ) == 'thumb-flip' ) {
			$p_nav_class = 'animate-nav _animate_thumb_flip';
		} else if ( get_theme_mod( 'post->slider=>nav-arrows-style', 'thumb-flip' ) == 'circle-slide' ) {
			$p_nav_class = 'animate-nav _animate_circle_slide';
		} else if ( get_theme_mod( 'post->slider=>nav-arrows-style', 'thumb-flip' ) == 'diamond' ) {
			$p_nav_class = 'animate-nav _animate_diamond';
		}
	}

	$p_nav_speed = get_theme_mod( 'post->slider=>nav-arrows-speed', 900 );
	if ( ! is_numeric( $p_nav_speed ) && $p_nav_speed <= 0 ) {
		$p_nav_speed = 900;
	}

	$p_nav_dots = get_theme_mod( 'post->slider=>nav-dots', false ) ? 'true' : 'false';
	$p_dots_class = '';

	if ( ! wp_is_mobile() ) {
		if ( get_theme_mod( 'post->slider=>nav-dots-style', 'scale-up' ) == 'scale-up' ) {
			$p_dots_class = '_animate_scale_up';
		} else if ( get_theme_mod( 'post->slider=>nav-dots-style', 'scale-up' ) == 'fill-up' ) {
			$p_dots_class = '_animate_fill_up';
		} else if ( get_theme_mod( 'post->slider=>nav-dots-style', 'scale-up' ) == 'fall' ) {
			$p_dots_class = '_animate_fall';
		}
	}

	$p_dots_speed = get_theme_mod( 'post->slider=>nav-dots-speed', 600 );
	if ( ! is_numeric( $p_dots_speed ) && $p_dots_speed <= 0 ) {
		$p_dots_speed = 600;
	}

	$p_autoplay = get_theme_mod( 'post->slider=>auto-play', true ) ? 'true' : 'false';

	$p_autoplay_timeout = get_theme_mod( 'post->slider=>auto-play-timeout', 7000 );
	if ( ! is_numeric( $p_autoplay_timeout ) && $p_autoplay_timeout <= 0 ) {
		$p_autoplay_timeout = 7000;
	}

	$p_autoplay_speed = get_theme_mod( 'post->slider=>auto-play-speed', 900 );
	if ( ! is_numeric( $p_autoplay_speed ) && $p_autoplay_speed <= 0 ) {
		$p_autoplay_speed = 7000;
	}

	$p_animation = get_theme_mod( 'post->slider=>animation', 'fade-out-in' );
	$p_animate_out = '';
	$p_animate_in = '';
	if ( 'fade-out-in' === $p_animation ) {
		$p_animate_out = 'fade-out';
		$p_animate_in = 'fade-in';
	} else if ( 'fade-out' === $p_animation ) {
		$p_animate_out = 'fade-out';
	} else if ( 'slide-out-down' === $p_animation ) {
		$p_animate_out = 'slideOutDown';
		$p_animate_in = 'fade-in';
	} else if ( 'flip-in-x' === $p_animation ) {
		$p_animate_out = 'slideOutDown';
		$p_animate_in = 'flipInX';
	}
	?>
	<script>
		jQuery(document).ready(function ($) {

			'use strict';

			var renderPreview = function (slider) {

				var $slider = $(slider.$element[0]);

				var prev = slider._current - 1;
				var next = slider._current + 1;

				var prevPreview = $slider.find('.owl-prev .owl-nav__preview');
				var nextPreview = $slider.find('.owl-next .owl-nav__preview');

				prevPreview.css('background-image', 'url(' + $slider.find('.owl-item:eq(' + prev + ')').find('[data-thumbnail]').data('thumbnail') + ')');
				nextPreview.css('background-image', 'url(' + $slider.find('.owl-item:eq(' + next + ')').find('[data-thumbnail]').data('thumbnail') + ')');
			};

			(function () {
				var $slider = $('.entry-slider');
				var totalItems = $slider.find('.slide__content').length;
				var isLooped, isDrag;

				if (totalItems == 1) {
					isLooped = false;
					isDrag = false;
				} else {
					isLooped = true;
					isDrag = true;
				}

				if ($slider.length) {
					$slider.owlCarousel({
						items: 1,
						loop: isLooped,
						touchDrag: isDrag,
						mouseDrag: isDrag,
						navText: ["<span class='owl-nav__arrow'></span><div class='owl-nav__preview-wrap'><div class='owl-nav__preview'></div>", "<span class='owl-nav__arrow'></span><div class='owl-nav__preview-wrap'><div class='owl-nav__preview'></div></div>"],
						nav: <?php echo esc_js( $f_nav_arrows ) ?>,
						dots: <?php echo esc_js( $f_nav_dots ) ?>,
						navSpeed: <?php echo esc_js( $f_nav_speed ) ?>,
						dotsSpeed: <?php echo esc_js( $f_dots_speed ) ?>,
						navContainerClass: 'owl-nav _featured_nav <?php echo esc_js( $f_nav_class ) ?>',
						dotsClass: 'owl-dots <?php echo esc_js( $f_dots_class ) ?>',
						autoplay: <?php echo esc_js( $f_autoplay ) ?>,
						autoplayTimeout: <?php echo esc_js( $f_autoplay_timeout ) ?>,
						autoplaySpeed: <?php echo esc_js( $f_autoplay_speed ) ?>,
						autoplayHoverPause: true,
						responsive: {
							0: {
								dots: false
							},
							991: {
								dots: <?php echo esc_js( $f_nav_dots ) ?>
							}
						},
						onInitialized: function () {
							var $this = this;

							$($this.$element[0]).find('.owl-controls')
								.attr('data-js', 'parallax-lighten');

							setTimeout(function () {
								$($this.$element[0]).closest('.slider-wrapper')
									.find('.loading')
									.fadeOut(500);
							}, 50);

							$($this.$element[0]).css('height', 'auto');

							$($this.$element[0])
								.find('.active')
								.addClass('_animate');

							renderPreview($this);

							$($this.$element[0]).find('a')
								.attr('tabindex', '-1');
							$($this.$element[0]).find('.owl-item.active a')
								.removeAttr('tabindex');

							if ( totalItems == 1 ) {
								$($this.$element[0]).find('.owl-controls')
									.hide()
							}
						},
						onTranslate: function () {
							$(this.$element[0])
								.find('._animate')
								.removeClass('_animate');
						},
						onTranslated: function () {
							$(this.$element[0])
								.find('.active')
								.addClass('_animate');

							renderPreview(this);

							$(this.$element[0]).find('a')
								.attr('tabindex', '-1');
							$(this.$element[0]).find('.owl-item.active a')
								.removeAttr('tabindex');
						}
					});
				}
			})();

			(function () {
				var $slider = $('.post__slider');
				var totalItems = $slider.find('.slide__featured').length;
				var isLooped, isDrag;

				if (totalItems == 1) {
					isLooped = false;
					isDrag = false;
				} else {
					isLooped = true;
					isDrag = true;
				}

				if ($slider.length) {
					$slider.owlCarousel({
						items: 1,
						loop: isLooped,
						touchDrag: isDrag,
						mouseDrag: isDrag,
						navText: ["<span class='owl-nav__arrow'></span><div class='owl-nav__preview-wrap'><div class='owl-nav__preview'></div>", "<span class='owl-nav__arrow'></span><div class='owl-nav__preview-wrap'><div class='owl-nav__preview'></div></div>"],
						nav: <?php echo esc_js( $p_nav_arrows ) ?>,
						dots: <?php echo esc_js( $p_nav_dots ) ?>,
						navSpeed: <?php echo esc_js( $p_nav_speed ) ?>,
						dotsSpeed: <?php echo esc_js( $p_dots_speed ) ?>,
						navContainerClass: 'owl-nav <?php echo esc_js( $p_nav_class ) ?>',
						dotsClass: 'owl-dots <?php echo esc_js( $p_dots_class ) ?>',
						autoplay: <?php echo esc_js( $p_autoplay ) ?>,
						autoplayTimeout: <?php echo esc_js( $p_autoplay_timeout ) ?>,
						autoplaySpeed: <?php echo esc_js( $p_autoplay_speed ) ?>,
						autoplayHoverPause: true,
						animateOut: '<?php echo esc_js( $p_animate_out );?>',
						animateIn: '<?php echo esc_js( $p_animate_in );?>',
						autoHeight: true,
						responsive: {
							0: {
								dots: false
							},
							768: {
								dots: <?php echo esc_js( $p_nav_dots ) ?>
							}
						},
						onInitialized: function () {
							var $this = this;

							$($this.$element[0]).css('height', 'auto');

							renderPreview($this);

							$($this.$element[0]).find('a')
								.attr('tabindex', '-1');

							if ( totalItems == 1 ) {
								$($this.$element[0]).find('.owl-controls')
									.hide();
							}
						},
						onTranslate: function () {
							renderPreview(this);
						}
					});
				}
			})();

			<?php if ( get_theme_mod( 'custom-code=>js', false ) ) {echo esc_js( get_theme_mod( 'custom-code=>js' ) );} ?>
		});

	</script>

<?php
}

add_action( 'wp_footer', 'rococo_customize_js' ); ?>
