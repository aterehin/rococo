<?php
/**
 * Custom controller checkbox with the label in the header.
 *
 * @version 1.1.0
 */
function rococo_custom_controllers( $wp_customize ) {
	class Rococo_Custom_Checkbox_Control extends WP_Customize_Control {
		/**
		 * Render the control's content.
		 */

		public function render_content() {
			?>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<label>
				<input type="checkbox" <?php $this->link(); ?> value="<?php echo esc_textarea( $this->value() ); ?>"/>
				<?php echo esc_html( $this->description ); ?>
			</label>
		<?php
		}
	}

	class Rococo_Articles_Filter_Controller extends WP_Customize_Control {
		public $type = 'articles-filter';
		public function enqueue() {
			wp_enqueue_style( 'bootstrap-tagsinput', get_template_directory_uri() . '/css/bootstrap-tagsinput.css' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_script( 'twitter-typeahead', get_template_directory_uri() . '/js/typeahead.jquery.js', array( 'jquery' ) );
			wp_enqueue_script( 'bootstrap-tagsinput', get_template_directory_uri() . '/js/bootstrap-tagsinput.js', array( 'jquery' ) );
		    wp_enqueue_script( 'rococo-articles-filter', get_template_directory_uri() . '/js/articles-filter-control.js', array( 'jquery', 'customize-controls', 'underscore', 'wp-util' ) );

			wp_localize_script( 'rococo-articles-filter', 'admin_ajax',
				array(
					'url'   => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce( 'nonce' ),
				)
			);
		}

		public function rococo_get_posts() {

			/**
			 * Get all taxonomy, attached to the post of the type - the post.
			 */
			$post_tax = get_object_taxonomies( 'post' );

			/**
			 * Get all post, an array.
			 */
			$posts = get_posts( array( 'numberposts' => '-1', 'post_type' => 'post', 'fields' => 'ids' ) );

			/**
			 * Create returned array.
			 */
			$scope = array(
				'post_types' => array(),
				'terms'      => array(),
				'posts'      => array(),
				'order_by'   => array(
					'name' => array(
						'label' => 'Name',
					),
					'date' => array(
						'label' => 'Date',
					),
					'rand' => array(
						'label' => 'Random',
					),
					'comment_count' => array(
						'label' => 'Most commented',
					),
					'post__in' => array(
						'label' => 'None',
					),
				),
			);

			/**
			 * Loop through all posts
			 */
			foreach ( $posts as $post => $id ) {

				/**
				 * Get all taxonomies current post, an array.
				 */
				$post_taxs = wp_get_object_terms( $id, $post_tax );

				/**
				 * Default post format.
				 */
				$current_format = 'post-format-standard';

				/**
				 * Default post label.
				 */
				$current_label = get_post_format_string( 'standard' );
				$current_taxs  = array();
				/**
				 * Loop through all taxonomies posts
				 */
				foreach ( $post_taxs as $i => $tax_arr ) {

					/**
					 * Check if the taxonomy of post format
					 */
					if ( 'post_format' === $tax_arr->taxonomy ) {

						/**
						 * Change post format, the default
						 */
						$current_format = $tax_arr->slug;

						/**
						 * Change post label, the default
						 */
						$current_label = $tax_arr->name;
					} else {
						if ( array_search( $tax_arr->taxonomy, $current_taxs ) === false ) {
							array_push( $current_taxs, $tax_arr->taxonomy );

							if ( ! isset( $scope['terms'][ $tax_arr->taxonomy ] ) ) {
								$scope['terms'][ $tax_arr->taxonomy ] = array(
									'label' => get_taxonomy( $tax_arr->taxonomy )->labels->name,
								);
							}
						}
					}
				}
				if ( ! isset( $current_format, $scope['post_types'][ $current_format ] ) ) {
					$scope['post_types'][ $current_format ] = array(
						'label' => $current_label,
						'terms' => $current_taxs,
					);
				} else {
					foreach ( $current_taxs as $k => $tax ) {
						if ( array_search( $tax, $scope['post_types'][ $current_format ]['terms'] ) === false ) {
							array_push( $scope['post_types'][ $current_format ]['terms'], $tax );
						}
					}
				}
			}

			return $scope;
		}

		public function to_json() {
			parent::to_json();

			$scope = $this->rococo_get_posts();
			$state = array(
				'post_type' => '',
				'terms'     => array(
					'active'   => array(),
					'selected' => array(),
				),
				'incex'     => 'include',
				'tags'      => array(),
				'posts' => array(),
			);

			$this->json['scope'] = $scope;
			$this->json['state'] = $this->value();
		}
		public function content_template() {
	    ?>
		<#
		var attrs = {
			selected: 'selected="selected"',
			checked: 'checked="checked"',
			disabled: 'disabled="disabled"'
		}
		#>
		<div class="af-control">
			<span class="customize-control-title">{{{ data.label }}}</span>
			<div class="af-control_section">
				<label>
					<span class="description customize-control-description"><?php esc_html_e( 'Order by:', 'rococo' ); ?></span>
					<select class="af-control_order-by widefat">
						<# _.each( data.scope.order_by, function( item, key ) { #>
						<option value="{{ key }}" {{{ key === data.state.order_by ? attrs.selected : '' }}}>
							{{{ item.label }}}
						</option>
						<# } ); #>
					</select>
				</label>
			</div>
			<div class="af-control_section">
				<span class="description customize-control-description"><?php esc_html_e( 'Filter by:', 'rococo' ); ?></span>
				<div class="af-control_filter-type">
					<ul>
						<li>
							<label>
								<input
									type="radio"
									name="af-filter_by"
									value="taxonomies"
									{{{ 'taxonomies' === data.state.filter_type ? attrs.checked : '' }}}>
								<?php esc_html_e( 'Post type & taxonomies', 'rococo' ); ?>
							</label>
						</li>
						<li>
							<label>
								<input
									type="radio"
									name="af-filter_by"
									value="posts"
									{{{ 'posts' === data.state.filter_type ? attrs.checked : '' }}}>
								<?php esc_html_e( 'Specific posts', 'rococo' ); ?>
							</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="af-control_body {{{ 'posts' === data.state.filter_type ? '__visible' : '' }}}">
				<div class="af-control_section">
					<label for="af-control-add-post">
						<span class="description customize-control-description"><?php esc_html_e( 'Specific posts:', 'rococo' ); ?></span>
					</label>
					<div class="af-control_p-list {{ 0 === data.state.posts.length ? '__empty' : '' }}">
						<# if( 0 !== data.state.posts.length ) {
							_.each( data.state.posts, function( item, index ) { #>
								<div class="af-control_p-list_item" title="{{ item.label }}" data-af-post-item="{{ index }}">
									<h4>{{{ item.label }}}</h4>
									<span>{{{ item.date }}}</span>
									<ul class="af-item-nav">
										<li><a href="#" {{{ index === 0 ? 'class="af-control-disabled"' : '' }}} data-af-post-control="up" data-af-post-index="{{ index }}"><span class="dashicons dashicons-arrow-up-alt2"></span></a></li>
										<li><a href="#" {{{ data.state.posts.length - 1 === index ? 'class="af-control-disabled"' : '' }}} data-af-post-control="down" data-af-post-index="{{ index }}"><span class="dashicons dashicons-arrow-down-alt2"></span></a></li>
										<li><a href="#" data-af-post-control="delete" data-af-post-index="{{ index }}"><span class="dashicons dashicons-trash"></span></a></li>
									</ul>
								</div>
							<# } );
						} else { #>
							<p>No posts found</p>
						<# } #>
					</div>
					<div class="af-control_posts">
						<input id="af-control-add-post" type="text">
						<p><?php esc_html_e( 'Start typing a text, and then select a post.', 'rococo' ); ?></p>
					</div>
				</div>
			</div>
			<div class="af-control_body {{{ 'taxonomies' === data.state.filter_type ? '__visible' : '' }}}">
				<div class="af-control_section">
					<label>
						<span class="description customize-control-description"><?php esc_html_e( 'Post type:', 'rococo' ); ?></span>
						<select class="af-control_post-type widefat">
							<option value="" {{{ '' === data.state.post_type ? attrs.selected : '' }}}>
								<?php esc_html_e( 'All Post Types', 'rococo' ); ?>
							</option>
							<# _.each( data.scope.post_types, function( item, key ) { #>
							<option value="{{ key }}" {{{ key === data.state.post_type ? attrs.selected : '' }}}>
								{{{ item.label }}}
							</option>
							<# } ); #>
						</select>
					</label>
				</div>
				<div class="af-control_section">
					<span class="description customize-control-description"><?php esc_html_e( 'Taxonomies:', 'rococo' ); ?></span>
					<div class="af-control_terms">
						<ul>
							<# _.each( data.scope.terms, function( item, key ) { #>
							<li>
								<label>
									<input
										type="checkbox"
										value="{{ key }}"
										{{{
											( -1 === data.state.terms.active.indexOf( key ) &&
											data.state.terms.active.length > 0 ) ? attrs.disabled : ''
										}}}
										{{{
											( -1 !== data.state.terms.selected.indexOf( key ) &&
											data.state.terms.selected.length > 0 ) ? attrs.checked : ''
										}}}>
									{{{ item.label }}}
								</label>
							</li>
							<# } ); #>
						</ul>
					</div>
				</div>
				<div class="af-control_section">
					<span class="description customize-control-description"><?php esc_html_e( 'Taxonomy rules:', 'rococo' ); ?></span>
					<ul class="af-control_incex-control">
						<li>
							<label>
								<input
									type="radio"
									name="af-incex_rule"
									value="include"
									{{{ 'include' === data.state.incex ? attrs.checked : '' }}}>
								<?php esc_html_e( 'Include', 'rococo' ); ?>
							</label>
						</li>
						<li>
							<label>
								<input
									type="radio"
									name="af-incex_rule"
									value="exclude"
									{{{ 'exclude' === data.state.incex ? attrs.checked : '' }}}>
								<?php esc_html_e( 'Exclude', 'rococo' ); ?>
							</label>
						</li>
					</ul>
					<div class="af-control_incex-rule">
						<input type="text">
						<p><?php esc_html_e( 'Type a word and select taxanomy term, so you added a term, which will be include or exclude posts.', 'rococo' ); ?></p>
					</div>
				</div>
			</div>
		</div>
	    <?php
	  	}
	}
}

add_action( 'customize_register', 'rococo_custom_controllers' ); ?>
