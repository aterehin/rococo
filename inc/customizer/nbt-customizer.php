<?php
/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @package Nobrand
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @version 1.1.0
 */
function rococo_customize_register( $wp_customize ) {
	/* Layout Settings */
	$wp_customize->add_section( 'layout', array(
		'priority'    => 0,
		'title'       => esc_html_x( 'Layout', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Select page layout', 'Used in Layout Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'layout=>sticky-menu', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'layout=>scroll-to-top', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'layout=>index-post-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'standard_first&grid',
	) );
	$wp_customize->add_setting( 'layout=>index-featured-slider', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'layout=>index-popular-posts', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'layout=>index-sidebar', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'right',
	) );
	$wp_customize->add_setting( 'layout=>archive-post-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'standard',
	) );
	$wp_customize->add_setting( 'layout=>archive-featured-slider', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'layout=>archive-sidebar', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'right',
	) );
	$wp_customize->add_setting( 'layout=>single-featured-slider', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'layout=>single-sidebar', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'right',
	) );
	$wp_customize->add_setting( 'layout=>search-post-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'list',
	) );
	$wp_customize->add_setting( 'layout=>search-featured-slider', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'layout=>search-sidebar', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'right',
	) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'layout=>sticky-menu', array(
		'settings'    => 'layout=>sticky-menu',
		'priority'    => 0,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Sticky Menu', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable fixed menu', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'layout=>scroll-to-top', array(
		'settings'    => 'layout=>scroll-to-top',
		'priority'    => 10,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Scroll To Top', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable button', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>index-post-style', array(
		'settings'    => 'layout=>index-post-style',
		'priority'    => 20,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Home Page Layout', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Posts layout', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'standard'            => esc_html_x( 'Standard', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&grid' => esc_html_x( 'Standard First & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&list' => esc_html_x( 'Standard First & List', 'Used in Layout Customizing', 'rococo' ),
			'standard&grid'       => esc_html_x( 'Standard & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard&list'       => esc_html_x( 'Standard & List', 'Used in Layout Customizing', 'rococo' ),
			'grid'                => esc_html_x( 'Grid', 'Used in Layout Customizing', 'rococo' ),
			'list'                => esc_html_x( 'List', 'Used in Layout Customizing', 'rococo' ),
			'grid&list'           => esc_html_x( 'Grid & List', 'Used in Layout Customizing', 'rococo' ),
			'zigzag'              => esc_html_x( 'Zigzag', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>index-featured-slider', array(
		'settings' => 'layout=>index-featured-slider',
		'priority' => 40,
		'section'  => 'layout',
		'label'    => esc_html_x( 'Enable featured slider', 'Used in Layout Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>index-popular-posts', array(
		'settings' => 'layout=>index-popular-posts',
		'priority' => 50,
		'section'  => 'layout',
		'label'    => esc_html_x( 'Enable popular posts', 'Used in Layout Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>index-sidebar', array(
		'settings'    => 'layout=>index-sidebar',
		'priority'    => 60,
		'section'     => 'layout',
		'description' => esc_html_x( 'Sidebar', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'left'    => esc_html_x( 'Left', 'Used in Layout Customizing', 'rococo' ),
			'right'   => esc_html_x( 'Right', 'Used in Layout Customizing', 'rococo' ),
			'disable' => esc_html_x( 'Disable', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>archive-post-style', array(
		'settings'    => 'layout=>archive-post-style',
		'priority'    => 80,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Archive Page Layout', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Posts layout', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'standard'            => esc_html_x( 'Standard', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&grid' => esc_html_x( 'Standard First & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&list' => esc_html_x( 'Standard First & List', 'Used in Layout Customizing', 'rococo' ),
			'standard&grid'       => esc_html_x( 'Standard & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard&list'       => esc_html_x( 'Standard & List', 'Used in Layout Customizing', 'rococo' ),
			'grid'                => esc_html_x( 'Grid', 'Used in Layout Customizing', 'rococo' ),
			'list'                => esc_html_x( 'List', 'Used in Layout Customizing', 'rococo' ),
			'grid&list'           => esc_html_x( 'Grid & List', 'Used in Layout Customizing', 'rococo' ),
			'zigzag'              => esc_html_x( 'Zigzag', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>archive-featured-slider', array(
		'settings' => 'layout=>archive-featured-slider',
		'priority' => 90,
		'section'  => 'layout',
		'label'    => esc_html_x( 'Enable featured slider', 'Used in Layout Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>archive-sidebar', array(
		'settings'    => 'layout=>archive-sidebar',
		'priority'    => 100,
		'section'     => 'layout',
		'description' => esc_html_x( 'Sidebar', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'left'    => esc_html_x( 'Left', 'Used in Layout Customizing', 'rococo' ),
			'right'   => esc_html_x( 'Right', 'Used in Layout Customizing', 'rococo' ),
			'disable' => esc_html_x( 'Disable', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'layout=>single-featured-slider', array(
		'settings'    => 'layout=>single-featured-slider',
		'priority'    => 120,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Single Post Layout', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable featured slider', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>single-sidebar', array(
		'settings'    => 'layout=>single-sidebar',
		'priority'    => 130,
		'section'     => 'layout',
		'description' => esc_html_x( 'Sidebar', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'left'    => esc_html_x( 'Left', 'Used in Layout Customizing', 'rococo' ),
			'right'   => esc_html_x( 'Right', 'Used in Layout Customizing', 'rococo' ),
			'disable' => esc_html_x( 'Disable', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>search-post-style', array(
		'settings'    => 'layout=>search-post-style',
		'priority'    => 150,
		'section'     => 'layout',
		'label'       => esc_html_x( 'Search Result Page Layout', 'Used in Layout Customizing', 'rococo' ),
		'description' => esc_html_x( 'Posts layout', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'standard'            => esc_html_x( 'Standard', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&grid' => esc_html_x( 'Standard First & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard_first&list' => esc_html_x( 'Standard First & List', 'Used in Layout Customizing', 'rococo' ),
			'standard&grid'       => esc_html_x( 'Standard & Grid', 'Used in Layout Customizing', 'rococo' ),
			'standard&list'       => esc_html_x( 'Standard & List', 'Used in Layout Customizing', 'rococo' ),
			'grid'                => esc_html_x( 'Grid', 'Used in Layout Customizing', 'rococo' ),
			'list'                => esc_html_x( 'List', 'Used in Layout Customizing', 'rococo' ),
			'grid&list'           => esc_html_x( 'Grid & List', 'Used in Layout Customizing', 'rococo' ),
			'zigzag'              => esc_html_x( 'Zigzag', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>search-featured-slider', array(
		'settings' => 'layout=>search-featured-slider',
		'priority' => 170,
		'section'  => 'layout',
		'label'    => esc_html_x( 'Enable featured slider', 'Used in Layout Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'layout=>search-sidebar', array(
		'settings'    => 'layout=>search-sidebar',
		'priority'    => 180,
		'section'     => 'layout',
		'description' => esc_html_x( 'Sidebar', 'Used in Layout Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'left'    => esc_html_x( 'Left', 'Used in Layout Customizing', 'rococo' ),
			'right'   => esc_html_x( 'Right', 'Used in Layout Customizing', 'rococo' ),
			'disable' => esc_html_x( 'Disable', 'Used in Layout Customizing', 'rococo' ),
		),
	) ) );

	/* Top Bar Settings */
	$wp_customize->add_panel( 'top-bar', array(
		'priority' => 10,
		'title'    => esc_html_x( 'Top Bar Settings', 'Used in Top Bar Customizing', 'rococo' ),
	) );
	/* Top Bar Settings --Socials */
	$wp_customize->add_section( 'top-bar->socials', array(
		'priority' => 0,
		'panel'    => 'top-bar',
		'title'    => esc_html_x( 'Social Links Settings', 'Used in Top Bar Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'top-bar->socials=>enable', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'top-bar->socials=>facebook-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>twitter-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>instagram-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>pinterest-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>google-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>youtube-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>tumblr-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>linkedin-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>stumbleupon-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'top-bar->socials=>rss-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'top-bar->socials=>enable', array(
		'settings'    => 'top-bar->socials=>enable',
		'priority'    => 0,
		'section'     => 'top-bar->socials',
		'label'       => esc_html_x( 'Social Links', 'Used in Top Bar Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable links', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>facebook-link', array(
		'settings'    => 'top-bar->socials=>facebook-link',
		'priority'    => 10,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Facebook Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>twitter-link', array(
		'settings'    => 'top-bar->socials=>twitter-link',
		'priority'    => 20,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Twitter Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>instagram-link', array(
		'settings'    => 'top-bar->socials=>instagram-link',
		'priority'    => 30,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Instagram Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>pinterest-link', array(
		'settings'    => 'top-bar->socials=>pinterest-link',
		'priority'    => 40,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Pinterest Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>google-link', array(
		'settings'    => 'top-bar->socials=>google-link',
		'priority'    => 50,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Google+ Account ID', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>youtube-link', array(
		'settings'    => 'top-bar->socials=>youtube-link',
		'priority'    => 60,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'YouTube Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>tumblr-link', array(
		'settings'    => 'top-bar->socials=>tumblr-link',
		'priority'    => 70,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Tumblr Username', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>linkedin-link', array(
		'settings'    => 'top-bar->socials=>linkedin-link',
		'priority'    => 80,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'Linkedin Public Profile URL', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->socials=>rss-link', array(
		'settings'    => 'top-bar->socials=>rss-link',
		'priority'    => 90,
		'section'     => 'top-bar->socials',
		'description' => esc_html_x( 'RSS URL', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	/* Top Bar Settings --Find */
	$wp_customize->add_section( 'top-bar->search', array(
		'priority' => 10,
		'panel'    => 'top-bar',
		'title'    => esc_html_x( 'Site Search', 'Used in Top Bar Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'top-bar->search=>enable', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'top-bar->search=>placeholder', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => esc_html_x( 'Search and hit enter...', 'Used in Top Bar Customizing', 'rococo' ),
	) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'top-bar->search=>enable', array(
		'settings'    => 'top-bar->search=>enable',
		'priority'    => 0,
		'section'     => 'top-bar->search',
		'label'       => esc_html_x( 'Site Search', 'Used in Top Bar Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable search', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'top-bar->search=>placeholder', array(
		'settings'    => 'top-bar->search=>placeholder',
		'priority'    => 10,
		'section'     => 'top-bar->search',
		'description' => esc_html_x( 'Search placeholder', 'Used in Top Bar Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );

	/* Header Settings */
	$wp_customize->add_section( 'header', array(
		'priority' => 20,
		'title'    => esc_html_x( 'Header', 'Used in Logo Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'header=>desktop-indent-top', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 95,
	) );
	$wp_customize->add_setting( 'header=>desktop-indent-bottom', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 95,
	) );
	$wp_customize->add_setting( 'header=>mobile-indent-top', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 40,
	) );
	$wp_customize->add_setting( 'header=>mobile-indent-bottom', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 40,
	) );
	$wp_customize->add_setting( 'header=>mobile-indent-top', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 40,
	) );
	$wp_customize->add_setting( 'header=>mobile-indent-bottom', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 40,
	) );
	$wp_customize->add_setting( 'header=>desktop', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => get_template_directory_uri() . '/images/bg/logo.png',
	) );
	$wp_customize->add_setting( 'header=>desktop-light', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => get_template_directory_uri() . '/images/bg/logo-light.png',
	) );
	$wp_customize->add_setting( 'header=>mobile', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => get_template_directory_uri() . '/images/bg/logo@2x.png',
	) );
	$wp_customize->add_setting( 'header=>mobile-light', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => get_template_directory_uri() . '/images/bg/logo-light@2x.png',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header=>desktop-indent-top', array(
		'settings'    => 'header=>desktop-indent-top',
		'priority'    => 0,
		'section'     => 'header',
		'description' => esc_html_x( 'Header padding top', 'Used in Logo Customizing', 'rococo' ),
		'type'        => 'number',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header=>desktop-indent-bottom', array(
		'settings'    => 'header=>desktop-indent-bottom',
		'priority'    => 10,
		'section'     => 'header',
		'description' => esc_html_x( 'Header padding bottom', 'Used in Logo Customizing', 'rococo' ),
		'type'        => 'number',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header=>mobile-indent-top', array(
		'settings'    => 'header=>mobile-indent-top',
		'priority'    => 20,
		'section'     => 'header',
		'description' => esc_html_x( 'Header padding top (on mobile devices)', 'Used in Logo Customizing', 'rococo' ),
		'type'        => 'number',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header=>mobile-indent-bottom', array(
		'settings'    => 'header=>mobile-indent-bottom',
		'priority'    => 30,
		'section'     => 'header',
		'description' => esc_html_x( 'Header padding bottom (on mobile devices)', 'Used in Logo Customizing', 'rococo' ),
		'type'        => 'number',
	) ) );
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		$wp_customize->add_setting( 'header=>favicon', array(
			'sanitize_callback' => 'rococo_sanitize_callback',
			'type'              => 'theme_mod',
			'default'           => get_template_directory_uri() . '/images/bg/favicon.png',
		) );
	}
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header=>desktop', array(
		'settings' => 'header=>desktop',
		'priority' => 40,
		'section'  => 'header',
		'label'    => esc_html_x( 'Logo', 'Used in Logo Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header=>desktop-light', array(
		'settings'    => 'header=>desktop-light',
		'priority'    => 50,
		'section'     => 'header',
		'description' => esc_html_x( 'Required for header background version', 'Used in Logo Customizing', 'rococo' ),
		'label'       => esc_html_x( 'Logo Light', 'Used in Logo Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header=>mobile', array(
		'settings' => 'header=>mobile',
		'priority' => 60,
		'section'  => 'header',
		'label'    => esc_html_x( 'Retina Logo', 'Used in Logo Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header=>mobile-light', array(
		'settings' => 'header=>mobile-light',
		'priority' => 70,
		'section'  => 'header',
		'label'    => esc_html_x( 'Retina Logo Light', 'Used in Logo Customizing', 'rococo' ),
	) ) );
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header=>favicon', array(
			'settings' => 'header=>favicon',
			'priority' => 80,
			'section'  => 'header',
			'label'    => esc_html_x( 'Favicon', 'Used in Logo Customizing', 'rococo' ),
		) ) );
	}

	/* Featured slider Settings */
	$wp_customize->add_section( 'featured-slider', array(
		'priority' => 30,
		'title'    => esc_html_x( 'Featured Slider', 'Used in Featured slider Settings', 'rococo' ),
	) );
	$wp_customize->add_setting( 'featured-slider=>articles-filter', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default' => array(
			'post_type'   => '',
			'filter_type' => 'taxonomies',
			'terms'       => array(
				'active'   => array(),
				'selected' => array(),
			),
			'incex'       => 'include',
			'tags'        => array(),
			'order_by'    => 'post__in',
			'posts'       => array(),
		),
	) );
	$wp_customize->add_setting( 'featured-slider=>model', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'box-with-preview',
	) );
	$wp_customize->add_setting( 'featured-slider=>amount', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 6,
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-arrows', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-arrows-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'thumb-flip',
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-arrows-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 900,
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-dots', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-dots-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'scale-up',
	) );
	$wp_customize->add_setting( 'featured-slider=>nav-dots-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 600,
	) );
	$wp_customize->add_setting( 'featured-slider=>auto-play', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'featured-slider=>auto-play-timeout', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 7000,
	) );
	$wp_customize->add_setting( 'featured-slider=>auto-play-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 900,
	) );
	$wp_customize->add_control( new Rococo_Articles_Filter_Controller( $wp_customize, 'featured-slider=>articles-filter', array(
		'settings' => 'featured-slider=>articles-filter',
		'priority' => 0,
		'section'  => 'featured-slider',
		'label'    => esc_html_x( 'Articles Filter', 'Used in Featured slider Settings', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>model', array(
		'settings' => 'featured-slider=>model',
		'priority' => 0,
		'section'  => 'featured-slider',
		'label'    => esc_html_x( 'Slider Style', 'Used in Featured slider Settings', 'rococo' ),
		'type'     => 'select',
		'choices'  => array(
			'box-with-preview' => esc_html_x( 'Box with preview', 'Used in Featured slider Settings', 'rococo' ),
			'box'              => esc_html_x( 'Box', 'Used in Featured slider Settings', 'rococo' ),
			'full-width'       => esc_html_x( 'Full width', 'Used in Featured slider Settings', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>amount', array(
		'settings'    => 'featured-slider=>amount',
		'priority'    => 10,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Number of slides', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'featured-slider=>nav-arrows', array(
		'settings'    => 'featured-slider=>nav-arrows',
		'priority'    => 20,
		'section'     => 'featured-slider',
		'label'       => esc_html_x( 'Navigation Arrows', 'Used in Featured slider Settings', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>nav-arrows-style', array(
		'settings'    => 'featured-slider=>nav-arrows-style',
		'priority'    => 30,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Navigation arrows style', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'thumb-flip'   => esc_html_x( 'Thumb Flip', 'Used in Featured slider Settings', 'rococo' ),
			'circle-slide' => esc_html_x( 'Circle Slide', 'Used in Featured slider Settings', 'rococo' ),
			'diamond'      => esc_html_x( 'Diamond', 'Used in Featured slider Settings', 'rococo' ),
			'false'        => esc_html_x( 'None', 'Used in Featured slider Settings', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>nav-arrows-speed', array(
		'settings'    => 'featured-slider=>nav-arrows-speed',
		'priority'    => 40,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Navigation arrows speed', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'featured-slider=>nav-dots', array(
		'settings'    => 'featured-slider=>nav-dots',
		'priority'    => 50,
		'section'     => 'featured-slider',
		'label'       => esc_html_x( 'Navigation Dots', 'Used in Featured slider Settings', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>nav-dots-style', array(
		'settings'    => 'featured-slider=>nav-dots-style',
		'priority'    => 60,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Navigation dots style', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'scale-up' => esc_html_x( 'Scale Up', 'Used in Featured slider Settings', 'rococo' ),
			'fill-up'  => esc_html_x( 'Fill Up', 'Used in Featured slider Settings', 'rococo' ),
			'fall'     => esc_html_x( 'Fall', 'Used in Featured slider Settings', 'rococo' ),
			'false'    => esc_html_x( 'None', 'Used in Featured slider Settings', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>nav-dots-speed', array(
		'settings'    => 'featured-slider=>nav-dots-speed',
		'priority'    => 70,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Navigation dots speed', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'featured-slider=>auto-play', array(
		'settings'    => 'featured-slider=>auto-play',
		'priority'    => 80,
		'section'     => 'featured-slider',
		'label'       => esc_html_x( 'Autoplay', 'Used in Featured slider Settings', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>auto-play-timeout', array(
		'settings'    => 'featured-slider=>auto-play-timeout',
		'priority'    => 90,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Autoplay interval timeout', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'featured-slider=>auto-play-speed', array(
		'settings'    => 'featured-slider=>auto-play-speed',
		'priority'    => 100,
		'section'     => 'featured-slider',
		'description' => esc_html_x( 'Autoplay speed', 'Used in Featured slider Settings', 'rococo' ),
		'type'        => 'text',
	) ) );

	/* Post Settings */
	$wp_customize->add_panel( 'post', array(
		'priority' => 40,
		'title'    => esc_html_x( 'Post Settings', 'Used in Post Customizing', 'rococo' ),
	) );
	/* Post Settings --Parts */
	$wp_customize->add_section( 'post->parts', array(
		'priority' => 0,
		'panel'    => 'post',
		'title'    => esc_html_x( 'Post Layout', 'Used in Post Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'post->parts=>categories', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>date', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>author', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>comments-number', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>read-more', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>tags', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>author-bio', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>post-nav', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->parts=>related', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>categories', array(
		'settings'    => 'post->parts=>categories',
		'priority'    => 0,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Post Categories', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable categories', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>date', array(
		'settings'    => 'post->parts=>date',
		'priority'    => 10,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Post Date', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable date', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>author', array(
		'settings'    => 'post->parts=>author',
		'priority'    => 20,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Post Author', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable author name', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>comments-number', array(
		'settings'    => 'post->parts=>comments-number',
		'priority'    => 30,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Number of Comments', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable comments', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>read-more', array(
		'settings'    => 'post->parts=>read-more',
		'priority'    => 40,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Read More Link', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable link', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>tags', array(
		'settings'    => 'post->parts=>tags',
		'priority'    => 50,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Tags', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable tags', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>author-bio', array(
		'settings'    => 'post->parts=>author-bio',
		'priority'    => 60,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Author Biography', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable biography', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>post-nav', array(
		'settings'    => 'post->parts=>post-nav',
		'priority'    => 70,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Post Navigation Links', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable links', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->parts=>related', array(
		'settings'    => 'post->parts=>related',
		'priority'    => 80,
		'section'     => 'post->parts',
		'label'       => esc_html_x( 'Related Posts', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable posts', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	/* Post Settings --Social Links */
	$wp_customize->add_section( 'post->socials', array(
		'priority' => 10,
		'panel'    => 'post',
		'title'    => esc_html_x( 'Social Links Settings', 'Used in Post Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'post->socials=>enable', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>facebook-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>twitter-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>pinterest-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>google-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>tumblr-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>linkedin-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->socials=>stumbleupon-link', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->socials=>enable', array(
		'settings'    => 'post->socials=>enable',
		'priority'    => 0,
		'section'     => 'post->socials',
		'label'       => esc_html_x( 'Social links', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable links', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>facebook-link', array(
		'settings' => 'post->socials=>facebook-link',
		'priority' => 10,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Facebook', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>twitter-link', array(
		'settings' => 'post->socials=>twitter-link',
		'priority' => 20,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Twitter', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>pinterest-link', array(
		'settings' => 'post->socials=>pinterest-link',
		'priority' => 30,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Pinterest', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>google-link', array(
		'settings' => 'post->socials=>google-link',
		'priority' => 40,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Google+', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>tumblr-link', array(
		'settings' => 'post->socials=>tumblr-link',
		'priority' => 50,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Tumblr', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>linkedin-link', array(
		'settings' => 'post->socials=>linkedin-link',
		'priority' => 60,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Linkedin', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->socials=>stumbleupon-link', array(
		'settings' => 'post->socials=>stumbleupon-link',
		'priority' => 70,
		'section'  => 'post->socials',
		'label'    => esc_html_x( 'Stumbleupon', 'Used in Post Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );

	/* Post Settings --Slider */
	$wp_customize->add_section( 'post->slider', array(
		'priority' => 20,
		'panel'    => 'post',
		'title'    => esc_html_x( 'Image Slider Settings', 'Used in Post Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'post->slider=>animation', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'fade-out-in',
	) );
	$wp_customize->add_setting( 'post->slider=>nav-arrows', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->slider=>nav-arrows-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'thumb-flip',
	) );
	$wp_customize->add_setting( 'post->slider=>nav-arrows-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 900,
	) );
	$wp_customize->add_setting( 'post->slider=>nav-dots', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'post->slider=>nav-dots-style', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'scale-up',
	) );
	$wp_customize->add_setting( 'post->slider=>nav-dots-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 600,
	) );
	$wp_customize->add_setting( 'post->slider=>auto-play', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'post->slider=>auto-play-timeout', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 7000,
	) );
	$wp_customize->add_setting( 'post->slider=>auto-play-speed', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 900,
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>animation', array(
		'settings'    => 'post->slider=>animation',
		'priority'    => 0,
		'section'     => 'post->slider',
		'label' => esc_html_x( 'Slider Animation Effect', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'select',
		'choices' => array(
			'list'        => esc_html_x( 'List', 'Used in Post Customizing', 'rococo' ),
			'fade-out'    => esc_html_x( 'Fade Out', 'Used in Post Customizing', 'rococo' ),
			'fade-out-in' => esc_html_x( 'Fade Out In', 'Used in Post Customizing', 'rococo' ),
			'flip-in-x' => esc_html_x( 'Flip In X', 'Used in Post Customizing', 'rococo' ),
			'slide-out-down' => esc_html_x( 'Slide Out Down', 'Used in Post Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->slider=>nav-arrows', array(
		'settings'    => 'post->slider=>nav-arrows',
		'priority'    => 0,
		'section'     => 'post->slider',
		'label'       => esc_html_x( 'Navigation Arrows', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>nav-arrows-style', array(
		'settings'    => 'post->slider=>nav-arrows-style',
		'priority'    => 10,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Navigation arrows style', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'thumb-flip'   => esc_html_x( 'Thumb Flip', 'Used in Post Customizing', 'rococo' ),
			'circle-slide' => esc_html_x( 'Circle Slide', 'Used in Post Customizing', 'rococo' ),
			'diamond'      => esc_html_x( 'Diamond', 'Used in Post Customizing', 'rococo' ),
			'false'        => esc_html_x( 'None', 'Used in Post Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>nav-arrows-speed', array(
		'settings'    => 'post->slider=>nav-arrows-speed',
		'priority'    => 20,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Navigation arrows speed', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->slider=>nav-dots', array(
		'settings'    => 'post->slider=>nav-dots',
		'priority'    => 30,
		'section'     => 'post->slider',
		'label'       => esc_html_x( 'Navigation Dots', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>nav-dots-style', array(
		'settings'    => 'post->slider=>nav-dots-style',
		'priority'    => 40,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Navigation dots style', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'select',
		'choices'     => array(
			'scale-up' => esc_html_x( 'Scale Up', 'Used in Post Customizing', 'rococo' ),
			'fill-up'  => esc_html_x( 'Fill Up', 'Used in Post Customizing', 'rococo' ),
			'fall'     => esc_html_x( 'Fall', 'Used in Post Customizing', 'rococo' ),
			'false'    => esc_html_x( 'None', 'Used in Post Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>nav-dots-speed', array(
		'settings'    => 'post->slider=>nav-dots-speed',
		'priority'    => 50,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Navigation dots speed', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'post->slider=>auto-play', array(
		'settings'    => 'post->slider=>auto-play',
		'priority'    => 60,
		'section'     => 'post->slider',
		'label'       => esc_html_x( 'Autoplay', 'Used in Post Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>auto-play-timeout', array(
		'settings'    => 'post->slider=>auto-play-timeout',
		'priority'    => 70,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Autoplay interval timeout', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'post->slider=>auto-play-speed', array(
		'settings'    => 'post->slider=>auto-play-speed',
		'priority'    => 80,
		'section'     => 'post->slider',
		'description' => esc_html_x( 'Autoplay speed', 'Used in Post Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );

	/* Pagination Settings */
	$wp_customize->add_section( 'pagination', array(
		'priority'    => 50,
		'title'       => esc_html_x( 'Pagination', 'Used in Pagination Customizing', 'rococo' ),
		'description' => esc_html_x( 'Select page pagination', 'Used in Pagination Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'pagination=>index', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'standard',
	) );
	$wp_customize->add_setting( 'pagination=>archive', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'standard',
	) );
	$wp_customize->add_setting( 'pagination=>search', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'standard',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pagination=>index', array(
		'settings' => 'pagination=>index',
		'priority' => 0,
		'section'  => 'pagination',
		'label'    => esc_html_x( 'Index Page Pagination', 'Used in Pagination Customizing', 'rococo' ),
		'type'     => 'radio',
		'choices'  => array(
			'standard' => esc_html_x( 'Prev page / Next page', 'Used in Pagination Customizing', 'rococo' ),
			'numeric'  => esc_html_x( 'Numeric Pagination', 'Used in Pagination Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pagination=>archive', array(
		'settings' => 'pagination=>archive',
		'priority' => 10,
		'section'  => 'pagination',
		'label'    => esc_html_x( 'Archive Page Pagination', 'Used in Pagination Customizing', 'rococo' ),
		'type'     => 'radio',
		'choices'  => array(
			'standard' => esc_html_x( 'Prev page / Next page', 'Used in Pagination Customizing', 'rococo' ),
			'numeric'  => esc_html_x( 'Numeric Pagination', 'Used in Pagination Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pagination=>search', array(
		'settings' => 'pagination=>search',
		'priority' => 20,
		'section'  => 'pagination',
		'label'    => esc_html_x( 'Search Result Page Pagination', 'Used in Pagination Customizing', 'rococo' ),
		'type'     => 'radio',
		'choices'  => array(
			'standard' => esc_html_x( 'Prev page / Next page', 'Used in Pagination Customizing', 'rococo' ),
			'numeric'  => esc_html_x( 'Numeric Pagination', 'Used in Pagination Customizing', 'rococo' ),
		),
	) ) );
	/* Footer settings */
	$wp_customize->add_section( 'footer', array(
		'priority' => 60,
		'title'    => esc_html_x( 'Footer Settings', 'Used in Footer Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'footer=>grid', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 3,
	) );
	$wp_customize->add_setting( 'footer=>left-copyright', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => sprintf( esc_html_x( '%s %d All Rights Reserved.', 'Used in Footer Customizing', 'rococo' ), '&#169;', date( 'Y' ) ),
	) );
	$wp_customize->add_setting( 'footer=>right-copyright', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'Designed by Nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'footer=>social-bar-facebook', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-twitter', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-instagram', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-pinterest', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-google', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-youtube', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-tumblr', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-linkedin', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'footer=>social-bar-rss', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => 'nobrand',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>grid', array(
		'settings' => 'footer=>grid',
		'priority' => 0,
		'section'  => 'footer',
		'label'    => esc_html_x( 'Widgets Area Grid', 'Used in Footer Customizing', 'rococo' ),
		'type'     => 'select',
		'choices'  => array(
			1 => esc_html_x( 'One Column', 'Used in Footer Customizing', 'rococo' ),
			2 => esc_html_x( 'Two Column', 'Used in Footer Customizing', 'rococo' ),
			3 => esc_html_x( 'Three Column', 'Used in Footer Customizing', 'rococo' ),
			4 => esc_html_x( 'Four Column', 'Used in Footer Customizing', 'rococo' ),
		),
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>left-copyright', array(
		'settings' => 'footer=>left-copyright',
		'priority' => 10,
		'section'  => 'footer',
		'label'    => esc_html_x( 'Copyright Left', 'Used in Footer Customizing', 'rococo' ),
		'type'     => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>right-copyright', array(
		'settings' => 'footer=>right-copyright',
		'priority' => 20,
		'section'  => 'footer',
		'label'    => esc_html_x( 'Copyright Right', 'Used in Footer Customizing', 'rococo' ),
		'type'     => 'text',
	) ) );
	$wp_customize->add_control( new Rococo_Custom_Checkbox_Control( $wp_customize, 'footer=>social-bar', array(
		'settings'    => 'footer=>social-bar',
		'priority'    => 30,
		'section'     => 'footer',
		'label'       => esc_html_x( 'Social Bar', 'Used in Footer Customizing', 'rococo' ),
		'description' => esc_html_x( 'Enable bar', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-facebook', array(
		'settings'    => 'footer=>social-bar-facebook',
		'priority'    => 40,
		'section'     => 'footer',
		'description' => esc_html_x( 'Facebook Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-twitter', array(
		'settings'    => 'footer=>social-bar-twitter',
		'priority'    => 50,
		'section'     => 'footer',
		'description' => esc_html_x( 'Twitter Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-instagram', array(
		'settings'    => 'footer=>social-bar-instagram',
		'priority'    => 60,
		'section'     => 'footer',
		'description' => esc_html_x( 'Instagram Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-pinterest', array(
		'settings'    => 'footer=>social-bar-pinterest',
		'priority'    => 70,
		'section'     => 'footer',
		'description' => esc_html_x( 'Pinterest Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-google', array(
		'settings'    => 'footer=>social-bar-google',
		'priority'    => 80,
		'section'     => 'footer',
		'description' => esc_html_x( 'Google+ Account ID', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-youtube', array(
		'settings'    => 'footer=>social-bar-youtube',
		'priority'    => 90,
		'section'     => 'footer',
		'description' => esc_html_x( 'YouTube Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-tumblr', array(
		'settings'    => 'footer=>social-bar-tumblr',
		'priority'    => 100,
		'section'     => 'footer',
		'description' => esc_html_x( 'Tumblr Username', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-linkedin', array(
		'settings'    => 'footer=>social-bar-linkedin',
		'priority'    => 110,
		'section'     => 'footer',
		'description' => esc_html_x( 'Linkedin Public Profile URL', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footer=>social-bar-rss', array(
		'settings'    => 'footer=>social-bar-rss',
		'priority'    => 120,
		'section'     => 'footer',
		'description' => esc_html_x( 'RSS URL', 'Used in Footer Customizing', 'rococo' ),
		'type'        => 'text',
	) ) );
	/* 404 settings */
	$wp_customize->add_section( 'messages', array(
		'priority' => 70,
		'title'    => esc_html_x( 'Messages', 'Used in Messages Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'messages=>index-text', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => esc_html_x( 'It seems we cant find what youre looking for. Perhaps searching can help.', 'Used in Messages Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'messages=>index-search', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'messages=>search-result-text', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => esc_html_x( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'Used in Messages Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'messages=>search-result-search', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_setting( 'messages=>404-text', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => esc_html_x( 'It looks like nothing was found at this location. Maybe try a search?', 'Used in Messages Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'messages=>404-search', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => true,
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>index-text', array(
		'settings' => 'messages=>index-text',
		'priority' => 0,
		'section'  => 'messages',
		'label'    => esc_html_x( 'Index Page Message', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'textarea',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>index-search', array(
		'settings' => 'messages=>index-search',
		'priority' => 10,
		'section'  => 'messages',
		'label'    => esc_html_x( 'Enable search field', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>search-result-text', array(
		'settings' => 'messages=>search-result-text',
		'priority' => 20,
		'section'  => 'messages',
		'label'    => esc_html_x( 'Search Result Page Message', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'textarea',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>search-result-search', array(
		'settings' => 'messages=>search-result-search',
		'priority' => 30,
		'section'  => 'messages',
		'label'    => esc_html_x( 'Enable search field', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>404-text', array(
		'settings' => 'messages=>404-text',
		'priority' => 40,
		'section'  => 'messages',
		'label'    => esc_html_x( '404 Page Message', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'textarea',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'messages=>404-search', array(
		'settings' => 'messages=>404-search',
		'priority' => 50,
		'section'  => 'messages',
		'label'    => esc_html_x( 'Enable search field', 'Used in 404 Page Customizing', 'rococo' ),
		'type'     => 'checkbox',
	) ) );
	$wp_customize->add_section( 'colors-backgrounds', array(
		'priority' => 80,
		'title'    => esc_html_x( 'Colors and Backgrounds', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>text-color', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '#444444',
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>primary', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '#afa376',
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>top-bar-bg', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '#000000',
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>footer-widget-bg-color', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '#ffffff',
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>footer-widget-bg-image', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => false,
	) );
	$wp_customize->add_setting( 'colors-backgrounds=>footer-bg', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '#000000',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colors-backgrounds=>text-color', array(
		'settings' => 'colors-backgrounds=>text-color',
		'priority' => 30,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Text Color', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colors-backgrounds=>primary', array(
		'settings' => 'colors-backgrounds=>primary',
		'priority' => 40,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Primary Theme Color', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colors-backgrounds=>top-bar-bg', array(
		'settings' => 'colors-backgrounds=>top-bar-bg',
		'priority' => 80,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Top Bar Background', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colors-backgrounds=>footer-widget-bg-color', array(
		'settings' => 'colors-backgrounds=>footer-widget-bg-color',
		'priority' => 330,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Footer Widget Area Background', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'colors-backgrounds=>footer-widget-bg-image', array(
		'settings' => 'colors-backgrounds=>footer-widget-bg-image',
		'priority' => 340,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Footer Widget Area Background Image', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colors-backgrounds=>footer-bg', array(
		'settings' => 'colors-backgrounds=>footer-bg',
		'priority' => 350,
		'section'  => 'colors-backgrounds',
		'label'    => esc_html_x( 'Footer Background', 'Used in Colors and Backgrounds Customizing', 'rococo' ),
	) ) );
	$wp_customize->add_section( 'custom-code', array(
		'priority' => 90,
		'title'    => esc_html_x( 'Custom code', 'Used in Custom code Customizing', 'rococo' ),
	) );
	$wp_customize->add_setting( 'custom-code=>css', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_setting( 'custom-code=>js', array(
		'sanitize_callback' => 'rococo_sanitize_callback',
		'type'              => 'theme_mod',
		'default'           => '',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom-code=>css', array(
		'settings' => 'custom-code=>css',
		'priority' => 0,
		'section'  => 'custom-code',
		'label'    => esc_html_x( 'Custom CSS', 'Used in Custom code Customizing', 'rococo' ),
		'type'     => 'textarea',
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'custom-code=>js', array(
		'settings'    => 'custom-code=>js',
		'priority'    => 10,
		'section'     => 'custom-code',
		'label'       => esc_html_x( 'Custom JavaScript', 'Used in Custom code Customizing', 'rococo' ),
		'description' => esc_html_x( 'Google Analytics (or other) tracking code', 'Used in Custom code Customizing', 'rococo' ),
		'type'        => 'textarea',
	) ) );

	$wp_customize->remove_section( 'colors' );
	$wp_customize->register_control_type( 'Rococo_Articles_Filter_Controller' );
}

function rococo_sanitize_callback( $value ) {
	return $value;
}
add_action( 'customize_register', 'rococo_customize_register' );

?>
