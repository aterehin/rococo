<?php
/**
 *  Settings in customizer custom colors
 *
 *  @version 1.1.0
 */

function rococo_customizer_style() {
	if ( is_admin() ) {
		wp_enqueue_style( 'customizer', get_template_directory_uri() . '/css/customizer.css' );
	}
}
add_action( 'wp_loaded', 'rococo_customizer_style' );

function rococo_customize_css() { ?>
	<style>
	<?php if ( get_theme_mod( 'colors-backgrounds=>text-color', false ) && get_theme_mod( 'colors-backgrounds=>text-color' ) !== '#444444' ) : ?>
	body,
	.widget_archive ul li a, .widget_categories ul li a, .widget_meta ul li a, .widget_recent_comments ul li a, .widget_rss ul li a, .widget_pages ul li a, .widget_recent_entries ul li a, .widget_nav_menu ul li a {
		color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>text-color' ) ) ?>;
	}
	<?php endif;?>

	<?php if ( get_theme_mod( 'colors-backgrounds=>primary', false ) && get_theme_mod( 'colors-backgrounds=>primary' ) !== '#afa376' ) : ?>
	*::selection {
		background: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>primary' ) ) ?>;
	}
	*::-moz-selection {
		background: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>primary' ) ) ?>;
	}
	a,
	.btn, input.wpcf7-submit,
	.social-list .social-list__item a:hover,
	.social-list .social-list__item a:focus,
	.popular-post .popular-post__content .popular-post__date a:hover,
	.popular-post .popular-post__content .popular-post__date a:focus,
	.post .post__category,
	.post .post__category a,
	.post .post__info a:hover,
	.post .post__info a:focus,
	.post._format-quote .post__content i[class*="quote"],
	.social-list._in-post .social-list__item a,
	.pagination .pagination__item,
	.nav-previous a, .nav-next a,
	.page-links a .page-links__item,
	.social-list._in-author-bio .social-list__item a,
	.comments-list .comment__reply .comment-reply-link, .comments-list .comment__reply .comment-edit-link,
	.comment-respond .comment-reply-title small a,
	.comment-form .logged-in-as a:hover,
	.comment-form .logged-in-as a:focus,
	.related-posts .related-post .post__date a:hover,
	.related-posts .related-post .post__date a:focus,
	.social-list._in-widget .social-list__item a:hover,
	.social-list._in-widget .social-list__item a:focus,
	.widget_archive ul li a:hover, .widget_categories ul li a:hover, .widget_meta ul li a:hover, .widget_recent_comments ul li a:hover, .widget_rss ul li a:hover, .widget_pages ul li a:hover, .widget_recent_entries ul li a:hover, .widget_nav_menu ul li a:hover,
	.widget_archive ul li a:focus, .widget_categories ul li a:focus, .widget_meta ul li a:focus, .widget_recent_comments ul li a:focus, .widget_rss ul li a:focus, .widget_pages ul li a:focus, .widget_recent_entries ul li a:focus, .widget_nav_menu ul li a:focus,
	.widget_recent_comments ul li a, .widget_rss ul li a,
	.follow-text a .fa,
	.posts .post__item .post__content .post__author a:hover,
	.posts .post__item .post__content .post__author a:focus,
	.posts .post__item .post__content .post__date a:hover,
	.posts .post__item .post__content .post__date a:focus,
	.footer .footer__bottom-panel .designed a:hover,
	.footer .footer__bottom-panel .designed a:focus,
	.footer ._in-footer-social-bar .fa,
	.menu-list > ul > li > a:hover,
	.menu-list > ul > .current-menu-item > a,
	.sub-menu li:hover > a,
	.sub-menu .current-menu-item > a,
	.header-bg-overlay .menu-list:not(._fixed) > ul > li:not(.current-menu-item) > a:hover,
	.header-bg-overlay .menu-list:not(._fixed) > ul > li:not(.current-menu-item) > a:focus,
	.mob-menu-overlay .menu-list .current-menu-item > a {
		color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>primary' ) ) ?>;
	}
	.btn, input.wpcf7-submit,
	blockquote,
	.post__content a:hover,
	.post__content a:focus,
	.popular-post .popular-post__content,
	.popular-post .popular-post__content .popular-post__date a:hover,
	.popular-post .popular-post__content .popular-post__date a:focus,
	.content-box-overlay .content-box,
	.post .post__category a,
	.post .post__info a:hover,
	.post .post__info a:focus,
	.post .post__tags dd a,
	.pagination .pagination__item,
	.nav-previous a, .nav-next a,
	.page-links .page-links__item,
	.comments-list .comment__reply .comment-reply-link, .comments-list .comment__reply .comment-edit-link,
	.comment-respond .comment-reply-title small a,
	.comment-form .logged-in-as a:hover,
	.comment-form .logged-in-as a:focus,
	.related-posts .related-post .post__date a:hover,
	.related-posts .related-post .post__date a:focus,
	.widget_archive ul li a:hover, .widget_categories ul li a:hover, .widget_meta ul li a:hover, .widget_recent_comments ul li a:hover, .widget_rss ul li a:hover, .widget_pages ul li a:hover, .widget_recent_entries ul li a:hover, .widget_nav_menu ul li a:hover,
	.widget_archive ul li a:focus, .widget_categories ul li a:focus, .widget_meta ul li a:focus, .widget_recent_comments ul li a:focus, .widget_rss ul li a:focus, .widget_pages ul li a:focus, .widget_recent_entries ul li a:focus, .widget_nav_menu ul li a:focus,
	.widget_recent_comments ul li a:hover, .widget_rss ul li a:hover,
	.widget_recent_comments ul li a:focus, .widget_rss ul li a:focus,
	.tagcloud a:hover,
	.tagcloud a:focus,
	.about-me a:hover,
	.about-me a:focus,
	.footer .footer__bottom-panel .designed a:hover,
	.footer .footer__bottom-panel .designed a:focus {
		border-color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>primary' ) ) ?>;
	}
	.btn:hover, input.wpcf7-submit:hover,
	.wpcf7-form .wpcf7-validation-errors,
	.slide .slide__category a,
	.post._format-quote .post__content::after,
	.post._type-grid .post__content::after,
	.pagination .pagination__item:hover,
	.pagination .current,
	.nav-previous a:hover, .nav-next a:hover,
	.page-links .page-links__item,
	.page-links a .page-links__item:hover,
	.featured .featured__label,
	.comments-list .comment__reply .comment-reply-link:hover, .comments-list .comment__reply .comment-edit-link:hover,
	.comment-respond .comment-reply-title small a:hover,
	.tagcloud a:hover,
	#wp-calendar tbody td#today,
	.mc4wp-form-fields input[type="submit"],
	#wpadminbar .screen-reader-shortcut:focus,
	.screen-reader-shortcut:focus {
		background-color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>primary' ) ) ?>;
	}
	<?php endif;?>
	<?php if ( get_theme_mod( 'colors-backgrounds=>top-bar-bg', false ) && get_theme_mod( 'colors-backgrounds=>top-bar-bg' ) !== '#000000' ) : ?>
	.top-bar,
	.top-bar__right .site-search {
		background-color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>top-bar-bg' ) ) ?>;
	}
	<?php endif;?>
	<?php if ( get_theme_mod( 'colors-backgrounds=>footer-bg', false ) && get_theme_mod( 'colors-backgrounds=>footer-bg' ) !== '#000000' ) : ?>
	.footer .footer__social-bar,
	.footer .footer__bottom-panel {
		background-color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>footer-bg' ) ) ?>;
	}
	<?php endif;?>
	<?php if ( get_theme_mod( 'colors-backgrounds=>footer-widget-bg-color', false ) && get_theme_mod( 'colors-backgrounds=>footer-widget-bg-color' ) !== '#ffffff' ) : ?>
	.footer__widget-area {
		background-color: <?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>footer-widget-bg-color' ) ) ?>;
	}
	<?php endif;?>

	<?php if ( get_theme_mod( 'colors-backgrounds=>footer-widget-bg-image', false ) ) : ?>
	.footer__widget-area {
		background-image: url("<?php echo esc_attr( get_theme_mod( 'colors-backgrounds=>footer-widget-bg-image' ) ) ?>");
		background-repeat: repeat;
	}
	<?php endif;?>

	.site-brand {
		padding-top: <?php echo esc_attr( get_theme_mod( 'header=>desktop-indent-top', 95 ) ); ?>px;
		padding-bottom: <?php echo esc_attr( get_theme_mod( 'header=>desktop-indent-bottom', 95 ) ); ?>px;
	}

	@media (max-width: 991px) {
		.site-brand {
			padding-top: <?php echo esc_attr( get_theme_mod( 'header=>mobile-indent-top', 40 ) ); ?>px;
			padding-bottom: <?php echo esc_attr( get_theme_mod( 'header=>mobile-indent-bottom', 40 ) ); ?>px;
		}
	}

	<?php echo esc_attr( get_theme_mod( 'custom-code=>css', false ) ? get_theme_mod( 'custom-code=>css' ) : '' ) ?>
	</style>
<?php }
add_action( 'wp_head', 'rococo_customize_css' );?>
