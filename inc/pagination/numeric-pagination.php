<?php
/**
 * Numeric pagination.
 *
 * @param string $pages blank.
 *
 * @param int    $range 1.
 *
 * @package Nobrand
 */

function rococo_pagination( $pages = '', $range = 1 ) {
	$showitems = ( $range * 2 ) + 1;

	global $paged;
	if ( empty( $paged ) ) {
		$paged = 1;
	}

	if ( '' == $pages ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if ( ! $pages ) {
			$pages = 1;
		}
	}

	if ( 1 != $pages ) {
		echo "<div class='pagination'>";
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
			echo "<a class='pagination__item' href='" . esc_url( get_pagenum_link( 1 ) ) . "'><i class='fa fa-angle-double-left'></i></a>";
		}
		if ( $paged > 1 && $showitems < $pages ) {
			echo "<a class='pagination__item' href='" . esc_url( get_pagenum_link( $paged - 1 ) ) . "'><i class='fa fa-angle-left'></i></a>";
		}

		for ( $i = 1; $i <= $pages; $i ++ ) {
			if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i ) ? "<span class='pagination__item current'>" . wp_kses_data( $i ) . '</span>' : "<a class='pagination__item' href='" . esc_url( get_pagenum_link( $i ) ) . "' class='inactive' >" . wp_kses_data( $i ) . '</a>';
			}
		}

		if ( $paged < $pages && $showitems < $pages ) {
			echo "<a class='pagination__item' href='" . esc_url( get_pagenum_link( $paged + 1 ) ) . "'><i class='fa fa-angle-right'></i></a>";
		}
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
			echo "<a class='pagination__item' href='" . esc_url( get_pagenum_link( $pages ) ) . "'><i class='fa fa-angle-double-right'></i></a>";
		}
		echo "</div>\n";
	}
}?>
