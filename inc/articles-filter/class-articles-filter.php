<?php
/**
 * Featured slider filter class
 *
 * @package Nobrand
 * @since 1.1.0
 */

class Rococo_Articles_Filter_Class {

	/**
	 * @var array $settings Current filter settings 'featured-slider=>articles-filter'.
	 */
	private $settings;

	/**
	 * Create WP Query with current settings.
	 */
	function query() {
		$this->settings = get_theme_mod( 'featured-slider=>articles-filter' );

		$args = array(
			'posts_per_page'      => is_numeric( get_theme_mod( 'featured-slider=>amount', 6 ) ) ? get_theme_mod( 'featured-slider=>amount', 6 ) : 1,
			'ignore_sticky_posts' => true,
			'post_type'           => 'post',
			'post__in'            => $this->get_post_ids(),
			'orderby'             => $this->get_orderby(),
			'tax_query'           => array(
				'relation' => 'AND',
				$this->get_post_format(),
				$this->get_taxonomies()
			),
			'meta_query'          => array(
				array(
					'key' => '_thumbnail_id',
				),
			),
		);

		$query = new WP_Query( $args );

		return $query;
	}

	/**
	 * Return order by value.
	 *
	 * @return string
	 */
	private function get_orderby() {
		return $this->settings['order_by'];
	}

	/**
	 * Create array ids posts, found using ajax.
	 *
	 * @return array ids
	 */
	private function get_post_ids() {
		if ( ! empty( $this->settings['posts'] ) && $this->settings['filter_type'] === 'posts' ) {
			$post_query_arr = array();

			foreach ( $this->settings['posts'] as $k => $post ) {
				array_push( $post_query_arr, $post['id'] );
			}

			return $post_query_arr;
		} else if ( $this->settings['filter_type'] === 'posts' ) {
			return array( - 1 );
		}
	}

	/**
	 * Create array selected taxonomies.
	 *
	 * @return array
	 */
	private function get_taxonomies() {

		if ( $this->settings['filter_type'] === 'taxonomies' ) {
			$taxonomies_array  = $this->settings['terms'];
			$active_taxonomies = get_object_taxonomies( 'post' );
			$key = array_search( 'post_format', $active_taxonomies );
			if ( false !== $key ) {
				unset( $active_taxonomies[ $key ] );
			}

			if ( ! empty( $taxonomies_array['active'] ) && ! empty( $taxonomies_array['selected'] ) ) {
				$active_taxonomies = array_intersect( $taxonomies_array['active'], $taxonomies_array['selected'] );
			} else if ( ! empty( $taxonomies_array['active'] ) ) {
				$active_taxonomies = $taxonomies_array['active'];
			} else if ( ! empty( $taxonomies_array['selected'] ) ) {
				$active_taxonomies = $taxonomies_array['selected'];
			}

			if ( ! empty( $active_taxonomies ) ) {
				$tax_query_arr = array( 'relation' => 'include' === $this->settings['incex'] ? 'OR' : 'AND' );

				foreach ( $active_taxonomies as $k => $tax ) {
					$operator = 'include' === $this->settings['incex'] ? 'IN' : 'NOT IN';

					array_push( $tax_query_arr, array(
						'taxonomy' => $tax,
						'field'    => 'slug',
						'terms'    => ! empty( $this->settings['tags'] ) ? $this->get_tags() : get_terms( $tax, array( 'fields' => 'id=>slug' ) ),
						'operator' => $operator,
					) );
				}

				return $tax_query_arr;
			}
		}
	}

	/**
	 * Create array selected tags.
	 */
	private function get_tags() {
		$arr = array();
		foreach ( $this->settings['tags'] as $key => $tag ) {
			array_push( $arr, $tag['value'] );
		}
		return $arr;
	}

	/**
	 * Create array selected post_format.
	 *
	 * @return array string
	 */
	private function get_post_format() {

		if ( $this->settings['filter_type'] === 'taxonomies' ) {
			switch ( $this->settings['post_type'] ) {
				case 'post-format-standard':
					return array(
						'taxonomy' => 'post_format',
						'field'    => 'slug',
						'terms'    => array(
							'post-format-aside',
							'post-format-audio',
							'post-format-chat',
							'post-format-gallery',
							'post-format-image',
							'post-format-link',
							'post-format-quote',
							'post-format-status',
							'post-format-video',
						),
						'operator' => 'NOT IN',
					);
					break;
				case '':
					return '';
					break;
				default:
					return array(
						'taxonomy' => 'post_format',
						'field'    => 'slug',
						'terms'    => array( $this->settings['post_type'] ),
					);
			}
		}
	}
}
?>
