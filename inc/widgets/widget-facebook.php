<?php
/**
 * Widget API: Rococo_Widget_Facebook class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 *
 */

/**
 * Core class used to implement a Facebook widget.
 *
 * @see WP_Widget
 */
class Rococo_Widget_Facebook extends WP_Widget {

	/**
	 * Sets up a new Facebook widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'facebook', // Widget ID
			esc_html__( 'Nobrand Like Box', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'facebook', // Widget Class.
				'description' => esc_html__( 'A widget that displays a facebook feed.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Facebook widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {

		if ( $instance['show_stream'] ) {
			$height = 400;
		} else if ( $instance['show_faces'] ) {
			$height = 230;
		} else {
			$height = 70;
		}

		?>
		<iframe
			src="https://www.facebook.com/plugins/likebox.php?href=<?php echo urlencode( $instance['url'] ) ?>&amp;width=300&amp;height=400&amp;show_faces=<?php echo $instance['show_faces'] ? 'true' : 'false'; ?>&amp;stream=<?php echo $instance['show_stream'] ? 'true' : 'false'; ?>&amp;show_border=false&amp;header=false"
			scrolling="no" frameborder="0"
			style="display: block; margin: auto; border:none; overflow:hidden; width:auto; max-width: 100%; height: <?php echo esc_attr( $height ) ?>px"
			allowTransparency="true">
		</iframe>
		<?php

	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['url']         = esc_url( $new_instance['url'] );
		$instance['show_faces']  = $new_instance['show_faces'];
		$instance['show_stream'] = $new_instance['show_stream'];

		return $instance;
	}

	/**
	 * Outputs the settings form for the Facebook widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults = array(
			'show_faces'  => true,
			'show_stream' => true,
		);

		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"><?php esc_html_e( 'Facebook profile URL:', 'rococo' ) ?></label> <br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'url' ) ) ?>" value="<?php if ( ! empty( $instance['url'] ) ) {echo esc_url( $instance['url'] );}?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_faces' ) ) ?>"><input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'show_faces' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_faces' ) ) ?>" value="1" <?php if ( isset( $instance['show_faces'] ) ) {checked( '1', $instance['show_faces'], true );} ?> /><?php esc_html_e( 'Show faces', 'rococo' ) ?></label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_stream' ) ) ?>"><input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'show_stream' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_stream' ) ) ?>" value="1" <?php if ( isset( $instance['show_stream'] ) ) {checked( '1', $instance['show_stream'], true );} ?> /><?php esc_html_e( 'Show stream', 'rococo' ) ?></label>
		</p>
	<?php
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_Facebook" );' )
);

?>
