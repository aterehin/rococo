<?php
/**
 * Widget API: Rococo_Widget_Posts class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 *
 */

/**
 * Core class used to implement a Posts widget.
 *
 * @see WP_Widget
 */
class Rococo_Widget_Posts extends WP_Widget {

	/**
	 * Sets up a new Posts widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'posts', // Widget ID
			esc_html__( 'Nobrand Posts', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'posts', // Widget Class.
				'description' => esc_html__( 'A widget that displays your posts.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Posts widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		if ( 'random' == $instance['sort'] ) {
			$sort = 'rand';
		} else if ( 'popular' == $instance['sort'] ) {
			$sort = 'comment_count';
		} else {
			$sort = 'date';
		}

		$params = array(
			'orderby'             => $sort,
			'ignore_sticky_posts' => 1,
			'cache_results'       => 0,
			'posts_per_page'      => $instance['count'],
		);

		$query = new WP_Query( $params );

		if ( $query->have_posts() ) {
			echo $args['before_widget'];

			if ( $title ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}

			while ( $query->have_posts() ) :
				$query->the_post(); ?>
				<div class="post__item">
					<?php if ( $instance['photo'] ) : ?>
						<div class="post__featured">
							<a class="post__thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
	                            <?php if ( has_post_thumbnail() ) {
	                                the_post_thumbnail( 'rococo_small_thumb', array( 'alt' => get_the_title() ) );
                                } else {
	                                echo '<div class="image-placeholder _in-post-widget">';
	                                echo '<img src="'. esc_url( get_template_directory_uri() ) . '/images/bg/img-placeholder_thumb.jpg" alt="'. get_the_title() .'">';
	                                if ( 'audio' == get_post_format() ) {
	                                    echo '<i class="fa fa-file-audio-o"></i>';
	                                } else if ( get_post_format() == 'video' ) {
	                                     echo '<i class="fa fa-file-video-o"></i>';
	                                } else if ( get_post_format() == 'gallery' ) {
	                                    echo '<i class="fa fa-file-image-o"></i>';
	                                } else if ( get_post_format() == 'quote' ) {
	                                    echo '<i class="fa fa-quote-left"></i>';
	                                } else {
	                                    echo '<i class="fa fa-file-text-o"></i>';
	                                }
	                                echo '</div>';
	                            }?>
                        	</a>
						</div>
					<?php endif; ?>
					<div class="post__content">
						<h3 class="post__title"><a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"><?php echo esc_html( wp_trim_words( get_the_title(), 3, '...' ) ) ?></a>
						</h3>
						<?php if ( $instance['date'] ) {
							rococo_post_info( true, false, false ); // Date, Author, Comments.
						}?>
						<?php if ( $instance['author'] ) {
							rococo_post_info( false, true, false ); // Date, Author, Comments.
						}?>
					</div>
				</div>
			<?php  endwhile; // $query->have_posts()
			wp_reset_postdata();

			echo $args['after_widget'];
		}
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['sort']   = $new_instance['sort'];
		$instance['photo']  = $new_instance['photo'];
		$instance['date']   = $new_instance['date'];
		$instance['author'] = $new_instance['author'];
		$instance['count']  = esc_attr( $new_instance['count'] );

		return $instance;
	}

	/**
	 * Outputs the settings form for the Posts feed widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults = array(
			'title'  => 'Posts',
			'sort'   => 'recent',
			'photo'  => true,
			'date'   => true,
			'author' => true,
			'count'  => 5,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title    = sanitize_text_field( $instance['title'] );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"><?php esc_html_e( 'Title:', 'rococo' ) ?></label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>" value="<?php if ( ! empty( $instance['title'] ) ) {echo esc_attr( $title );} ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'sort' ) ) ?>"><?php esc_html_e( 'Sort:', 'rococo' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'sort' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'sort' ) ) ?>">
				<option value="popular" <?php if ( 'popular' == $instance['sort'] ) {echo 'selected="selected"';} ?>><?php esc_html_e( 'Popular posts', 'rococo' ); ?></option>
				<option value="recent" <?php if ( 'recent' == $instance['sort'] ) {echo 'selected="selected"';} ?>><?php esc_html_e( 'Recent posts', 'rococo' ); ?></option>
				<option value="random" <?php if ( 'random' == $instance['sort'] ) {echo 'selected="selected"';} ?>><?php esc_html_e( 'Random posts', 'rococo' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'photo' ) ) ?>">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'photo' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'photo' ) ) ?>" value="1" <?php if ( isset( $instance['photo'] ) ) {checked( 1, $instance['photo'], true );} ?> />
				<?php esc_html_e( 'Show photo', 'rococo' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'date' ) ) ?>">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'date' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'date' ) ) ?>" value="1" <?php if ( isset( $instance['date'] ) ) {checked( 1, $instance['date'], true );} ?> />
				<?php esc_html_e( 'Show date', 'rococo' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'author' ) ) ?>">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'author' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'author' ) ) ?>" value="1" <?php if ( isset( $instance['author'] ) ) {checked( 1, $instance['author'], true );} ?> />
				<?php esc_html_e( 'Show author name', 'rococo' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ) ?>"><?php esc_html_e( 'Count posts:', 'rococo' ) ?></label>
			<input type="number" min="1" id="<?php echo esc_attr( $this->get_field_id( 'count' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ) ?>" value="<?php echo esc_attr( $instance['count'] ); ?>" style="width: 50px;">
		</p>
	<?php
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_Posts" );' )
);

?>
