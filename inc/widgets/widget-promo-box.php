<?php
/**
 * Widget API: Rococo_Widget_PromoBox class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 *
 */

/**
 * Core class used to implement a Promo Box widget.
 *
 * @see WP_Widget
 */
class Rococo_Widget_PromoBox extends WP_Widget {
	/**
	 * Sets up a new Promo Box widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'promo-box', // Widget ID
			esc_html__( 'Nobrand Promo Box', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'promo-box', // Widget Class
				'description' => esc_html__( 'A widget that displays your promo image.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Promo box widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {

		$image_url   = ( 'upload' == $instance['method'] ) ? $instance['upload_url'] : $instance['string_url'];
		$link_url    = ! empty( $instance['link'] ) ? $instance['link'] : '';
		$widget_text = ! empty( $instance['text'] ) ? $instance['text'] : '';
		$text        = apply_filters( 'widget_text', $widget_text, $instance, $this );

		echo $args['before_widget'];

		if ( $link_url ) {
			echo '<a class="about-me__preview" href="' . esc_url( $link_url ) . '" ' . ( ( esc_attr( $instance['target'] ) ) ? "target='_blank'" : '' ) . '>';
		}
		if ( $image_url ) {
			echo '<img src="' . esc_url( $image_url ) . '" alt="">';
		}
		if ( $link_url ) {
			echo '</a>';
		}
		if ( $text ) {
			echo esc_textarea( $text );
		}

		echo $args['after_widget'];
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['method']     = $new_instance['method'];
		$instance['upload_url'] = esc_url( $new_instance['upload_url'] );
		$instance['string_url'] = esc_url( $new_instance['string_url'] );
		$instance['link']       = esc_url( $new_instance['link'] );
		$instance['target']     = esc_attr( $new_instance['target'] );
		$instance['text']       = esc_textarea( $new_instance['text'] );

		return $instance;

	}

	/**
	 * Outputs the settings form for the Promo box feed widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults = array( 'title' => 'About Me', 'method' => 'upload', 'target' => true );
		$instance = wp_parse_args( (array) $instance, $defaults );?>
		<div class="image">
			<?php esc_html_e( 'Image:', 'rococo' ) ?> <br/>
			<p class="image__get-method">
				<label for="<?php echo esc_attr( $this->get_field_id( 'method_upload' ) ) ?>">
					<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'method_upload' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'method' ) ) ?>" value="upload" <?php if ( isset( $instance['method'] ) ) {checked( 'upload', $instance['method'], true );} ?> />
					<?php esc_html_e( 'Upload image', 'rococo' ) ?>
				</label>
				<label for="<?php echo esc_attr( $this->get_field_id( 'method_url' ) ) ?>">
					<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'method_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'method' ) ) ?>" value="url" <?php if ( isset( $instance['method'] ) ) {checked( 'url', $instance['method'], true );} ?> />
					<?php esc_html_e( 'Specify URL', 'rococo' ) ?>
				</label>
			</p>

			<div class="pb-upload-area" style="<?php if ( 'url' == $instance['method'] ) {echo 'display:none;';} ?>">
				<input class="button" onClick="pb_uploader( this, '<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>' )" value="<?php esc_html_e( 'Choose File', 'rococo' ) ?>"/>
				<div class="<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>">
					<input data-js="pb-upload-input" type="hidden" class="pb-image_url" id="<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'upload_url' ) ) ?>" value="<?php if ( ! empty( $instance['upload_url'] ) ) {echo esc_url( $instance['upload_url'] );}?>">
					<?php if ( ! empty( $instance['upload_url'] ) ) : ?>
						<img data-js="pb-upload-image-preview" src="<?php echo esc_url( $instance['upload_url'] ); ?>" style="width:100%;">
					<?php endif; ?>
				</div>
			</div>

			<div class="pb-image__url-area" style="<?php if ( 'upload' == $instance['method'] ) {echo 'display:none;';} ?>">
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'string_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'string_url' ) ) ?>" value="<?php if ( ! empty( $instance['string_url'] ) ) {echo esc_url( $instance['string_url'] );} ?>">
			</div>
		</div>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ) ?>"><?php esc_html_e( 'Link to page:', 'rococo' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ) ?>" value="<?php if ( ! empty( $instance['link'] ) ) {echo esc_url( $instance['link'] );} ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ) ?>" style="margin-right: 10px;">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'target' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'target' ) ) ?>" value="1" <?php if ( isset( $instance['target'] ) ) {checked( 1, $instance['target'], true );} ?> />
				<?php esc_html_e( 'Open in new page', 'rococo' ) ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>"><?php esc_html_e( 'Text:', 'rococo' ); ?></label>
			<textarea class="widefat" rows="5" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ) ?>"><?php if ( ! empty( $instance['text'] ) ) {echo esc_textarea( $instance['text'] );} ?></textarea>
		</p>
		<script>
			jQuery(document).ready(function ($) {
				window.pb_uploader = function (element, class_name) {

					'use strict';

					wp.media.editor.send.attachment = function (props, attachment) {

						$('.pb-image_url').val(attachment.url);
						$('[data-js="pb-upload-image-preview"]').attr('src', attachment.url);

						$('.' + class_name + ' > img').remove();
						$('.' + class_name).prepend('<img src="' + attachment.url + '" style="width:100%">');
					};

					wp.media.editor.open(this);
					return false;
				};

				$('.widget-liquid-right .image__get-method label').on('click', function () {
					var $this = $(this);
					var $activeInput = $this.find('input:checked');

					if ($activeInput.val() == 'url') {
						$this.parent().siblings('.pb-upload-area').hide(0);
						$this.parent().siblings('.pb-image__url-area').show(0);
					} else {
						$this.parent().siblings('.pb-image__url-area').hide(0);
						$this.parent().siblings('.pb-upload-area').show(0);
					}
				});
			});
		</script>

		<style>
			.image .image__get-method {
				margin: 10px 0 15px;
			}

			.image .image__get-method label {
				display: block;
				margin-bottom: 6px;
			}

			.image .image__get-method input {
				margin-right: 0;
			}

			.pb-upload-area,
			.pb-image__url-area {
				margin-bottom: 20px;
			}

			.pb-upload-area input {
				margin-bottom: 10px !important;
			}
		</style>
	<?php
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_PromoBox" );' )
);

?>
