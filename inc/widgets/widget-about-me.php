<?php
/**
 * Widget API: Rococo_Widget_AboutMe class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 */

/**
 * Core class used to implement a About Me widget.
 *
 * @see WP_Widget
 */
class Rococo_Widget_AboutMe extends WP_Widget {
	/**
	 * Sets up a new About Me widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'about-me', // Widget ID
			esc_html__( 'Nobrand About Me', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'about-me', // Widget Class.
				'description' => esc_html__( 'A widget that displays your personal information.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current About me widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		$image_url   = ( 'upload' == $instance['method'] ) ? $instance['upload_url'] : $instance['string_url'];
		$link_url    = ! empty( $instance['link'] ) ? $instance['link'] : '';
		$widget_text = ! empty( $instance['text'] ) ? $instance['text'] : '';
		$text        = apply_filters( 'widget_text', $widget_text, $instance, $this );

		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		if ( $link_url ) {
			echo '<a class="about-me__preview" href="' . esc_url( $link_url ) . '" ' . ( ( esc_attr( $instance['target'] ) ) ? "target='_blank'" : '' ) . '>';
		}
		if ( $image_url ) {
			echo '<img src="' . esc_url( $image_url ) . '" alt="' . esc_attr( $title ) . '">';
		}
		if ( $link_url ) {
			echo '</a>';
		}
		if ( $text ) {
			echo esc_textarea( $text );
		}

		echo '<ul class="social-list _in-widget">';
		echo ! empty( $instance['facebook'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['facebook'] . '"><i class="fa fa-facebook"></i></a></li>' : '';
		echo ! empty( $instance['twitter'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['twitter'] . '"><i class="fa fa-twitter"></i></a></li>' : '';
		echo ! empty( $instance['instagram'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['instagram'] . '"><i class="fa fa-instagram"></i></a></li>' : '';
		echo ! empty( $instance['pinterest'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['pinterest'] . '"><i class="fa fa-pinterest"></i></a></li>' : '';
		echo ! empty( $instance['google_plus'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['google_plus'] . '"><i class="fa fa-google-plus"></i></a></li>' : '';
		echo ! empty( $instance['youtube'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['youtube'] . '"><i class="fa fa-youtube"></i></a></li>' : '';
		echo ! empty( $instance['rss'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['rss'] . '"><i class="fa fa-rss"></i></a></li>' : '';
		echo ! empty( $instance['flickr'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['flickr'] . '"><i class="fa fa-flickr"></i></a></li>' : '';
		echo ! empty( $instance['tumblr'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['tumblr'] . '"><i class="fa fa-tumblr"></i></a></li>' : '';
		echo ! empty( $instance['linkedin'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['linkedin'] . '"><i class="fa fa-linkedin"></i></a></li>' : '';
		echo '</ul>';

		echo $args['after_widget'];
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 *
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['method']      = $new_instance['method'];
		$instance['upload_url']  = esc_url( $new_instance['upload_url'] );
		$instance['string_url']  = esc_url( $new_instance['string_url'] );
		$instance['link']        = esc_url( $new_instance['link'] );
		$instance['target']      = $new_instance['target'];
		$instance['text']        = esc_textarea( $new_instance['text'] );
		$instance['facebook']    = esc_url( $new_instance['facebook'] );
		$instance['twitter']     = esc_url( $new_instance['twitter'] );
		$instance['instagram']   = esc_url( $new_instance['instagram'] );
		$instance['pinterest']   = esc_url( $new_instance['pinterest'] );
		$instance['google_plus'] = esc_url( $new_instance['google_plus'] );
		$instance['youtube']     = esc_url( $new_instance['youtube'] );
		$instance['rss']         = esc_url( $new_instance['rss'] );
		$instance['flickr']      = esc_url( $new_instance['flickr'] );
		$instance['tumblr']      = esc_url( $new_instance['tumblr'] );
		$instance['linkedin']    = esc_url( $new_instance['linkedin'] );

		return $instance;

	}

	/**
	 * Outputs the settings form for the About me widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults    = array( 'title' => 'About Me', 'method' => 'upload', 'target' => true );
		$instance    = wp_parse_args( (array) $instance, $defaults );
		$title       = sanitize_text_field( $instance['title'] );
		$facebook    = ! empty( $instance['facebook'] ) ? $instance['facebook'] : '';
		$twitter     = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';
		$instagram   = ! empty( $instance['instagram'] ) ? $instance['instagram'] : '';
		$pinterest   = ! empty( $instance['pinterest'] ) ? $instance['pinterest'] : '';
		$google_plus = ! empty( $instance['google_plus'] ) ? $instance['google_plus'] : '';
		$youtube     = ! empty( $instance['youtube'] ) ? $instance['youtube'] : '';
		$rss         = ! empty( $instance['rss'] ) ? $instance['rss'] : '';
		$flickr      = ! empty( $instance['flickr'] ) ? $instance['flickr'] : '';
		$tumblr      = ! empty( $instance['tumblr'] ) ? $instance['tumblr'] : '';
		$linkedin    = ! empty( $instance['linkedin'] ) ? $instance['linkedin'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"><?php esc_html_e( 'Title:', 'rococo' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>" value="<?php if ( ! empty( $instance['title'] ) ) {echo esc_attr( $title ); } ?>">
		</p>
		<div class="image">
			<?php esc_html_e( 'Image:', 'rococo' ) ?> <br/>

			<p class="image__get-method">
				<label for="<?php echo esc_attr( $this->get_field_id( 'method_upload' ) ) ?>">
					<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'method_upload' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'method' ) ) ?>" value="upload" <?php if ( isset( $instance['method'] ) ) {checked( 'upload', $instance['method'], true );} ?> />
					<?php esc_html_e( 'Upload image', 'rococo' ) ?>
				</label>
				<label for="<?php echo esc_attr( $this->get_field_id( 'method_url' ) ) ?>">
					<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'method_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'method' ) ) ?>" value="url" <?php if ( isset( $instance['method'] ) ) {checked( 'url', $instance['method'], true );} ?> />
					<?php esc_html_e( 'Specify URL', 'rococo' ) ?>
				</label>
			</p>

			<div class="am-upload-area" style="<?php if ( 'url' == $instance['method'] ) {echo 'display:none;';} ?>">
				<input class="button" onClick="am_uploader( this, '<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>' )" value="<?php esc_html_e( 'Choose File', 'rococo' ) ?>"/>

				<div class="<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>">
					<input data-js="am-upload-input" type="hidden" class="am-image_url" id="<?php echo esc_attr( $this->get_field_id( 'upload_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'upload_url' ) ) ?>" value="<?php if ( ! empty( $instance['upload_url'] ) ) {echo esc_url( $instance['upload_url'] );} ?>">
					<?php if ( ! empty( $instance['upload_url'] ) ) : ?>
						<img data-js="am-upload-image-preview" src="<?php echo esc_url( $instance['upload_url'] ); ?>" style="width:100%;">
					<?php endif; ?>
				</div>
			</div>

			<div class="am-image__url-area" style="<?php if ( 'upload' == $instance['method'] ) {echo 'display:none;';} ?>">
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'string_url' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'string_url' ) ) ?>" value="<?php if ( ! empty( $instance['string_url'] ) ) {echo esc_url( $instance['string_url'] );} ?>">
			</div>
		</div>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ) ?>"><?php esc_html_e( 'Link to page:', 'rococo' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ) ?>" value="<?php if ( ! empty( $instance['link'] ) ) {echo esc_url( $instance['link'] );} ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ) ?>" style="margin-right: 10px;">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'target' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'target' ) ) ?>" value="1" <?php if ( isset( $instance['target'] ) ) {checked( 1, $instance['target'], true );} ?> />
				<?php esc_html_e( 'Open in new page', 'rococo' ) ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>"><?php esc_html_e( 'Text:', 'rococo' ); ?></label>
			<textarea class="widefat" rows="5" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ) ?>"><?php if ( ! empty( $instance['text'] ) ) {echo esc_textarea( $instance['text'] );} ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ) ?>"><?php esc_html_e( 'Facebook URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ) ?>" value="<?php echo esc_url( $facebook ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ) ?>"><?php esc_html_e( 'Twitter URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ) ?>" value="<?php echo esc_url( $twitter ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ) ?>"><?php esc_html_e( 'Instagram URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ) ?>" value="<?php echo esc_url( $instagram ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ) ?>"><?php esc_html_e( 'Pinterest URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest' ) ) ?>" value="<?php echo esc_url( $pinterest ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ) ?>"><?php esc_html_e( 'Google+ URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'google_plus' ) ) ?>" value="<?php echo esc_url( $google_plus ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ) ?>"><?php esc_html_e( 'YouTube URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ) ?>" value="<?php echo esc_url( $youtube ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'rss' ) ) ?>"><?php esc_html_e( 'RSS URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'rss' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'rss' ) ) ?>" value="<?php echo esc_url( $rss ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ) ?>"><?php esc_html_e( 'Flickr URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'flickr' ) ) ?>" value="<?php echo esc_url( $flickr ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ) ?>"><?php esc_html_e( 'Tumblr URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'tumblr' ) ) ?>" value="<?php echo esc_url( $tumblr )?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ) ?>"><?php esc_html_e( 'LinkedIn URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ) ?>" value="<?php echo esc_url( $linkedin ) ?>">
		</p>

		<script>
			jQuery(document).ready(function ($) {
				window.am_uploader = function (element, class_name) {

					'use strict';

					wp.media.editor.send.attachment = function (props, attachment) {

						$('.am-image_url').val(attachment.url);
						$('[data-js="am-upload-image-preview"]').attr('src', attachment.url);

						$('.' + class_name + ' > img').remove();
						$('.' + class_name).prepend('<img src="' + attachment.url + '" style="width:100%">');
					};

					wp.media.editor.open(this);
					return false;
				};

				$('.widget-liquid-right .image__get-method label').on('click', function () {
					var $this = $(this);
					var $activeInput = $this.find('input:checked');

					if ($activeInput.val() == 'url') {
						$this.parent().siblings('.am-upload-area').hide(0);
						$this.parent().siblings('.am-image__url-area').show(0);
					} else {
						$this.parent().siblings('.am-image__url-area').hide(0);
						$this.parent().siblings('.am-upload-area').show(0);
					}
				});
			});
		</script>

		<style>
			.image .image__get-method {
				margin: 10px 0 15px;
			}

			.image .image__get-method label {
				display: block;
				margin-bottom: 6px;
			}

			.image .image__get-method input {
				margin-right: 0;
			}

			.am-upload-area {
				margin-bottom: 20px;
			}

			.am-upload-area input {
				margin-bottom: 10px !important;
			}
		</style>
	<?php
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_AboutMe" );' )
);

?>
