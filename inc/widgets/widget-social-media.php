<?php
/**
 * Widget API: Rococo_Widget_Social_Media class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 *
 */

/**
 * Core class used to implement a Social Media widget.
 *
 * @see WP_Widget
 */
class Rococo_Widget_Social_Media extends WP_Widget {

	/**
	 * Sets up a new Social Media widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'social-media', // Widget ID
			esc_html__( 'Nobrand Social Media', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'social-media', // Widget Class.
				'description' => esc_html__( 'A widget that displays links to social profiles.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Social media widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo '<ul class="social-list _in-widget">';
		echo ! empty( $instance['facebook'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['facebook'] . '"><i class="fa fa-facebook"></i></a></li>' : '';
		echo ! empty( $instance['twitter'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['twitter'] . '"><i class="fa fa-twitter"></i></a></li>' : '';
		echo ! empty( $instance['instagram'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['instagram'] . '"><i class="fa fa-instagram"></i></a></li>' : '';
		echo ! empty( $instance['pinterest'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['pinterest'] . '"><i class="fa fa-pinterest"></i></a></li>' : '';
		echo ! empty( $instance['google_plus'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['google_plus'] . '"><i class="fa fa-google-plus"></i></a></li>' : '';
		echo ! empty( $instance['youtube'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['youtube'] . '"><i class="fa fa-youtube"></i></a></li>' : '';
		echo ! empty( $instance['rss'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['rss'] . '"><i class="fa fa-rss"></i></a></li>' : '';
		echo ! empty( $instance['flickr'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['flickr'] . '"><i class="fa fa-flickr"></i></a></li>' : '';
		echo ! empty( $instance['tumblr'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['tumblr'] . '"><i class="fa fa-tumblr"></i></a></li>' : '';
		echo ! empty( $instance['linkedin'] ) ? '<li class="social-list__item"><a target="_blank" href="' . $instance['linkedin'] . '"><i class="fa fa-linkedin"></i></a></li>' : '';
		echo '</ul>';

		echo $args['after_widget'];
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['facebook']    = esc_url( $new_instance['facebook'] );
		$instance['twitter']     = esc_url( $new_instance['twitter'] );
		$instance['instagram']   = esc_url( $new_instance['instagram'] );
		$instance['pinterest']   = esc_url( $new_instance['pinterest'] );
		$instance['google_plus'] = esc_url( $new_instance['google_plus'] );
		$instance['youtube']     = esc_url( $new_instance['youtube'] );
		$instance['rss']         = esc_url( $new_instance['rss'] );
		$instance['flickr']      = esc_url( $new_instance['flickr'] );
		$instance['tumblr']      = esc_url( $new_instance['tumblr'] );
		$instance['linkedin']    = esc_url( $new_instance['linkedin'] );

		return $instance;

	}

	/**
	 * Outputs the settings form for the Social media widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Follow me' ) );

		$title       = ! empty( $instance['title'] ) ? sanitize_text_field( $instance['title'] ) : '';
		$facebook    = ! empty( $instance['facebook'] ) ? $instance['facebook'] : '';
		$twitter     = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';
		$instagram   = ! empty( $instance['instagram'] ) ? $instance['instagram'] : '';
		$pinterest   = ! empty( $instance['pinterest'] ) ? $instance['pinterest'] : '';
		$google_plus = ! empty( $instance['google_plus'] ) ? $instance['google_plus'] : '';
		$youtube     = ! empty( $instance['youtube'] ) ? $instance['youtube'] : '';
		$rss         = ! empty( $instance['rss'] ) ? $instance['rss'] : '';
		$flickr      = ! empty( $instance['flickr'] ) ? $instance['flickr'] : '';
		$tumblr      = ! empty( $instance['tumblr'] ) ? $instance['tumblr'] : '';
		$linkedin    = ! empty( $instance['linkedin'] ) ? $instance['linkedin'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"><?php esc_html_e( 'Title:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ) ?>"><?php esc_html_e( 'Facebook URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ) ?>" value="<?php echo esc_attr( $facebook ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ) ?>"><?php esc_html_e( 'Twitter URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ) ?>" value="<?php echo esc_attr( $twitter ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ) ?>"><?php esc_html_e( 'Instagram URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ) ?>" value="<?php echo esc_attr( $instagram ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ) ?>"><?php esc_html_e( 'Pinterest URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest' ) ) ?>" value="<?php echo esc_attr( $pinterest ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ) ?>"><?php esc_html_e( 'Google+ URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google_plus' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'google_plus' ) ) ?>" value="<?php echo esc_attr( $google_plus ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ) ?>"><?php esc_html_e( 'YouTube URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ) ?>" value="<?php echo esc_attr( $youtube ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'rss' ) ) ?>"><?php esc_html_e( 'RSS URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'rss' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'rss' ) ) ?>" value="<?php echo esc_attr( $rss ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ) ?>"><?php esc_html_e( 'Flickr URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'flickr' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'flickr' ) ) ?>" value="<?php echo esc_attr( $flickr ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ) ?>"><?php esc_html_e( 'Tumblr URL:', 'rococo' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tumblr' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'tumblr' ) ) ?>" value="<?php echo esc_attr( $tumblr ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ) ?>"><?php esc_html_e( 'LinkedIn URL:', 'default' ); ?></label><br>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ) ?>" value="<?php echo esc_attr( $linkedin ) ?>">
		</p>
	<?php
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_Social_Media" );' )
);

?>
