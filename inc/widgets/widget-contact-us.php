<?php
/**
 * Widget API: Rococo_Widget_Contact_Us class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 */

/**
 * Core class used to implement a Contact Us widget.
 *
 * @see WP_Widget
 * @since 1.1.0
 */
class Rococo_Widget_Contact_Us extends WP_Widget {
	/**
	 * Sets up a new Contact Us widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'contact-us', // Widget ID
			esc_html__( 'Nobrand Contact Us', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'contact-us', // Widget Class.
				'description' => esc_html__( 'A widget that displays WordPress Contact Form 7 so no shortcodes are needed.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Contact Us widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Contact Us widget instance.
	 */
	public function widget( $args, $instance ) {
		$widget_id = empty($instance['form']) ? '' : stripslashes($instance['form']);

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

        echo apply_filters('widget_text','[contact-form-7 id="' . $widget_id . '"]');
		echo $args['after_widget'];
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 *
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	/**
	 * Outputs the settings form for the Contact Us widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		// Define default values
		$defaults = array( 'title' => 'Contact Us' );
		$instance = wp_parse_args( (array) $instance, $defaults );

		// Define local variables
		$title = sanitize_text_field( $instance['title'] );
		$form = $instance['form'];
		$forms = new WP_Query( array( 'post_type' => 'wpcf7_contact_form' ) );
	    ?>
	    <p>
	        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"><?php esc_html_e( 'Title:', 'rococo' ); ?></label>
	        <input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>" value="<?php if ( ! empty( $instance['title'] ) ) {echo esc_attr( $title ); } ?>">
	    </p>
		<p>
			<label for="<?php echo $this->get_field_id('form'); ?>"><?php esc_html_e( 'Contact Form 7:', 'rococo' ); ?></label>
			<?php
				// If forms found, show into the select
				if ( $forms->have_posts() ) {
			?>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'form' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'form' ) ) ?>" >
				<?php while( $forms->have_posts() ) : $forms->the_post(); ?>
				<option value="<?php the_id(); ?>"<?php selected(get_the_id(), $instance['form']); ?>><?php the_title(); ?></option>
				<?php endwhile; ?>
			<?php
				// If forms not found, disable select
				} else {
			?>
			<select class="widefat" disabled>
				<option disabled="disabled"><?php esc_html_e( 'There are no forms available', 'rococo' ); ?></option>
			<?php }; ?>
			</select>
		</p>
		<?php
	}
}

add_action('widgets_init',
	create_function('', 'return register_widget( "Rococo_Widget_Contact_Us" );')
);

?>
