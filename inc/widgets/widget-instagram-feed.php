<?php
/**
 * Widget API: Rococo_Widget_Instagram_Feed class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 */

/**
 * Core class used to implement a Instagram Feed widget.
 *
 * @see WP_Widget
 * @version 1.1.0
 */
class Rococo_Widget_Instagram_Feed extends WP_Widget {

	/**
	 * Sets up a new Instagram Feed widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'instagram-feed', // Widget ID
			esc_html__( 'Nobrand Instagram Feed', 'rococo' ), // Widget Name.
			array(
				'classname' => 'instagram-feed', // Widget Class.
				'description' => esc_html__( 'A widget that displays your latest Instagram photos.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Instagram feed widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$count = ! empty( $instance['images_count'] ) ? $instance['images_count'] : 9;

		if ( ! empty( $instance['access_token'] ) ) :
			$rsp_array = $this->rococo_get_photos( $instance['access_token'], $count );

			echo $args['before_widget'];

			if ( $title ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}

			if ( $instance['columns'] > 1 ) {
				echo '<ul class="feed-list">';
				foreach ( $rsp_array as $i ) {
					echo '<li class="feed-list__item _col-' . esc_attr( $instance['columns'] ) . '">';
					echo '<a target="_blank" href="' . esc_url( $i['image_page_link'] ) . '"><img src="' . esc_url( $i['image_url'] ) . '" alt=""></a>';
					echo '</li>';
				}
				echo '</ul>';
			} else {
				echo '<div class="feed-slider">';
				foreach ( $rsp_array as $i ) {
					echo '<a target="_blank" href="' . esc_url( $i['image_page_link'] ) . '"><img src="' . esc_url( $i['image_url'] ) . '" alt=""></a>';
				}
				echo '</div>';
				?>
				<script>
					jQuery(document).ready(function ($) {

						'use strict';

						var $slider = $(".feed-slider");
						var totalItems = $slider.find('a img').length;
						var isLooped, isNav, isDrag;

						if (totalItems == 1) {
							isLooped = false;
							isDrag = false;
							isNav = false;
						} else {
							isLooped = true;
							isDrag = true;
							isNav = true;
						}

						if ($slider.length) {
							$slider.owlCarousel({
								items:1,
								loop: isLooped,
								touchDrag: isDrag,
								mouseDrag: isDrag,
								dots: false,
								nav: isNav,
								navText: ["<span class='owl-nav__arrow'></span>", "<span class='owl-nav__arrow'></span>"],
								navContainerClass: 'owl-nav _instagram-feed-nav animate-nav',
								animateOut: 'fadeOut',
								onInitialized: function () {
									$(this.$element[0]).css('height', 'auto');
								}
							});
						}
					});
				</script>
			<?php }
			echo $args['after_widget'];

		endif; // Not empty access_token.
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']        = strip_tags( $new_instance['title'] );
		$instance['access_token'] = esc_html( $new_instance['access_token'] );
		$instance['images_count'] = esc_html( $new_instance['images_count'] );
		$instance['columns']      = esc_html( $new_instance['columns'] );

		return $instance;

	}

	/**
	 * Outputs the settings form for the Instagram feed widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults = array( 'title' => 'Instagram Feed', 'images_count' => 9, 'columns' => 3 );
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title    = sanitize_text_field( $instance['title'] );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>"><?php esc_html_e( 'Title:', 'rococo' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ) ?>" value="<?php if ( ! empty( $instance['title'] ) ) {echo esc_attr( $title );} ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'access_token' ) ) ?>"><?php esc_html_e( 'Access token:', 'rococo' ); ?>
				<a target="_blank" href="http://docs.nobrand.team/instagram-widget/"><?php esc_html_e( 'How to get?', 'rococo' ) ?></a></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'access_token' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'access_token' ) ) ?>" value="<?php if ( ! empty( $instance['access_token'] ) ) {echo esc_attr( $instance['access_token'] );} ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'images_count' ) ) ?>"><?php esc_html__( 'Images count:', 'rococo' ) ?></label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'images_count' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'images_count' ) ) ?>" value="<?php echo esc_attr( $instance['images_count'] ) ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'columns' ) ) ?>"><?php esc_html_e( 'Columns:', 'rococo' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'columns' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'columns' ) ) ?>">
				<option value="1" <?php if ( 1 == $instance['columns'] ) {echo 'selected="selected"';} ?>>1</option>
				<option value="2" <?php if ( 2 == $instance['columns'] ) {echo 'selected="selected"';} ?>>2</option>
				<option value="3" <?php if ( 3 == $instance['columns'] ) {echo 'selected="selected"';} ?>>3</option>
				<option value="4" <?php if ( 4 == $instance['columns'] ) {echo 'selected="selected"';} ?>>4</option>
				<option value="5" <?php if ( 5 == $instance['columns'] ) {echo 'selected="selected"';} ?>>5</option>
			</select>
		</p>
	<?php
	}

	protected function rococo_get_photos( $access_token, $count ) {
		$cached = get_transient( 'feed_instagram_photos' );

		if ( false !== $cached && count( $cached ) == $count ) {
			return $cached;
		}

		$url              = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . trim( $access_token ) . '&count=' . trim( $count );
		$rsp_json         = wp_remote_fopen( $url );
		$rsp_obj          = json_decode( $rsp_json, true );
		$rsp_images_array = array();

		if ( 200 == $rsp_obj['meta']['code'] ) {

			if ( ! empty( $rsp_obj['data'] ) ) {

				foreach ( $rsp_obj['data'] as $data ) {
					array_push( $rsp_images_array, array( 'image_url' => $data['images']['thumbnail']['url'], 'image_page_link' => $data['link'] ) );
				}

				foreach ( $rsp_images_array as $key => $value ) {
					$rsp_images_array[ $key ] = preg_replace( '/s150x150/', 's320x320', $value );
				}

				$ending_array = $rsp_images_array;

				set_transient( 'feed_instagram_photos', $ending_array, 1 * HOUR_IN_SECONDS );

				return $ending_array;
			}
		}
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_Instagram_Feed" );' )
);

?>
