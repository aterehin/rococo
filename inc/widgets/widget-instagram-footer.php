<?php
/**
 * Widget API: Rococo_Widget_Footer_Instagram class
 *
 * @package Nobrand
 * @subpackage Widgets
 * @since 1.0
 *
 * @author nobrandteam http://www.nobrand.team/
 *
 */

/**
 * Core class used to implement a Footer Instagram widget.
 *
 * @see WP_Widget
 * @version 1.1.0
 */
class Rococo_Widget_Footer_Instagram extends WP_Widget {

	/**
	 * Sets up a new Footer Instagram widget instance.
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct(
			'footer-instagram', // Widget ID
			esc_html__( 'Nobrand Instagram Feed *', 'rococo' ), // Widget Name.
			array(
				'classname'   => 'footer-instagram', // Widget Class.
				'description' => esc_html__( 'A widget that displays your latest Instagram photos. Only for "Instagram Feed *" area.', 'rococo' ), // Widget Description.
			)
		);
	}

	/**
	 * Outputs the content for the current Instagram footer widget instance.
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! empty( $instance['access_token'] ) ) :

			$rsp_array = $this->rococo_get_photos( $instance['access_token'] );

			if ( ! $instance['slider'] ) {
				$count = count( $rsp_array['images'] );

				echo '<div class="instagram-grid">';

				if ( ! empty( $instance['follow_text'] ) ) {
					echo '<div class="follow-text">';
					echo '<a href="' . esc_url( 'https://www.instagram.com/' . $rsp_array['link'] ) . '/" target="_blanc" tabindex="-1"><i class="fa fa-instagram"></i>' . esc_textarea( $instance['follow_text'] ) . '</a>';
					echo '</div>';
				}
				echo '<ul>';
				for ( $i = 0; $i < $count; $i ++ ) {
					if ( $rsp_array['images'][ $i ] ) {
						echo '<li>';
						echo '<a target="_blank" href="' . esc_url( $rsp_array['images'][ $i ][1] ) . '" tabindex="-1"><img src="' . esc_url( $rsp_array['images'][ $i ][0] ) . '" alt=""></a>';
						echo '</li>';
					}
				}
				echo '</ul></div>';
				?>
				<script>
					jQuery(document).ready(function ($) {

						'use strict';

						var $rows = <?php echo $instance['first_line'] && $instance['second_line'] ? 2 : 1 ?>;
						var $columns = <?php echo 'big' == $instance['images_size'] ? 7 : 10 ?>;
						var grid = $('.instagram-grid');

						if (grid.length) {
							grid.gridrotator({
								rows: $rows,
								columns: $columns,
								animType: 'fadeInOut',
								animSpeed: 1000,
								interval: 1500,
								step: 1,
								preventClick: false,
								<?php if ( 'big' == $instance['images_size'] ) {
								echo 'w1700 : {
									rows	: $rows,
									columns	: 6
								},
								w1400 : {
									rows	: $rows,
									columns	: 5
								},
								w1024 : {
									rows	: $rows,
									columns	: 4
								},
								w768 : {
									rows	: $rows,
									columns : 3
								},
								w480: {
									rows    : $rows,
									columns : 2
								},
								w320: {
									rows    : $rows,
									columns : 2
								},
								w240: {
									rows    : $rows,
									columns : 2
								}';
								} else {
								echo 'w1700 : {
									rows	: $rows,
									columns	: 9
								},
								w1400 : {
									rows	: $rows,
									columns	: 8
								},
								w1024 : {
									rows	: $rows,
									columns	: 7
								},
								w768 : {
									rows	: $rows,
									columns : 6
								},
								w480: {
									rows    : $rows,
									columns : 5
								},
								w320: {
									rows    : $rows,
									columns : 5
								},
								w240: {
									rows    : $rows,
									columns : 5
								}';
								}?>
							});
						}
					});
				</script>
			<?php
			} else {
				if ( $instance['first_line'] && $instance['second_line'] ) {
					$count = floor( count( $rsp_array['images'] ) / 2 );
				} else {
					$count = count( $rsp_array['images'] );
				}

				if ( $instance['first_line'] ) {
					echo '<div class="footer-instagram__list">';
					for ( $i = 0; $i < $count; $i ++ ) {
						if ( $rsp_array['images'][ $i ] ) {
							echo '<div>';
							echo '<a target="_blank" href="' . esc_url( $rsp_array['images'][ $i ][1] ) . '" tabindex="-1"><img src="' . esc_url( $rsp_array['images'][ $i ][0] ) . '" alt=""></a>';
							echo '</div>';
						}
					}
					echo '</div>';
				}

				if ( ! empty( $instance['follow_text'] ) ) {
					echo '<div class="follow-text">';
					echo '<a href="https://www.instagram.com/' . esc_url( $rsp_array['link'] ) . '/" target="_blanc" tabindex="-1"><i class="fa fa-instagram"></i>' . esc_textarea( $instance['follow_text'] ) . '</a>';
					echo '</div>';
				}

				if ( $instance['second_line'] && ! $instance['first_line'] ) {
					echo '<div class="footer-instagram__list">';
					for ( $i = 0; $i < $count; $i ++ ) {
						if ( $rsp_array['images'][ $i ] ) {
							echo '<div>';
							echo '<a target="_blank" href="' . esc_url( $rsp_array['images'][ $i ][1] ) . '" tabindex="-1"><img src="' . esc_url( $rsp_array['images'][ $i ][0] ) . '" alt=""></a>';
							echo '</div>';
						}
					}
					echo '</div>';
				} else {
					echo '<div class="footer-instagram__list">';
					for ( $i = $count; $i < count( $rsp_array['images'] ); $i ++ ) {
						if ( $rsp_array['images'][ $i ] ) {
							echo '<div>';
							echo '<a target="_blank" href="' . esc_url( $rsp_array['images'][ $i ][1] ) . '" tabindex="-1"><img src="' . esc_url( $rsp_array['images'][ $i ][0] ) . '" alt=""></a>';
							echo '</div>';
						}
					}
					echo '</div>';
				}
				?>
				<script>
					jQuery(document).ready(function ($) {

						'use strict';

						<?php if ( 'big' == $instance['images_size'] ) {
						echo  ' var visibleElement = 7;
								var responsiveArray = {
									0 : {
										items: 2,
										nav: false
									},
									600 : {
										items: 3,
										nav: false
									},
									920 : {
										items: 4,
										nav: false
									},
									1170 : {
										items: 4
									},
									1400 : {
										items: 5
									},
									1650 : {
										items: 6
									},
									1700 : {
										items: 7
									}
								};';
						} else if ( 'small' == $instance['images_size'] ) {
							echo  'var visibleElement = 10;
							var responsiveArray = {
								0 : {
									items: 5,
									nav: false
								},
								600 : {
									items: 6,
									nav: false
								},
								920 : {
									items: 7,
									nav: false
								},
								1170 : {
									items: 8
								},
								1400 : {
									items: 9
								},
								1700 : {
									items: 10
								}
							};';
						}
						?>

						var slider = $('.footer-instagram__list');
						if (slider.length) {
							slider.owlCarousel({
								items: visibleElement,
								loop: true,
								dots: false,
								nav: true,
								navSpeed: 650,
								navText: ["<span class='owl-nav__arrow'></span>", "<span class='owl-nav__arrow'></span>"],
								navContainerClass: 'owl-nav animate-nav _instagram-footer-nav',
								animateOut: 'fadeOut',
								onInitialized: function () {
									$(this.$element[0]).css('height', 'auto');
								},
								responsive: responsiveArray
							});
						}
					});
				</script>
			<?php
			}
		endif;
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['access_token'] = esc_html( $new_instance['access_token'] );
		$instance['images_size']  = esc_attr( $new_instance['images_size'] );
		$instance['follow_text']  = esc_textarea( $new_instance['follow_text'] );
		$instance['first_line']   = $new_instance['first_line'];
		$instance['second_line']  = $new_instance['second_line'];
		$instance['slider']       = $new_instance['slider'];

		return $instance;

	}

	/**
	 * Outputs the settings form for the Instagram feed widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$defaults = array( 'images_size' => 'small', 'first_line' => false, 'second_line' => true, 'slider' => false );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'access_token' ) ) ?>"><?php esc_html_e( 'Access token:', 'rococo' ); ?>
				<a target="_blank" href="http://docs.nobrand.team/instagram-widget/"><?php esc_html_e( 'How to get?', 'rococo' ) ?></a>
			</label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'access_token' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'access_token' ) ) ?>" value="<?php if ( ! empty( $instance['access_token'] ) ) {echo esc_html( $instance['access_token'] );} ?>">
		</p>
		<p>
			<label><?php esc_html_e( 'Image size:', 'rococo' ); ?></label><br/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'big' ) ) ?>" style="margin-right: 10px;">
				<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'big' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'images_size' ) ) ?>" value="big" <?php if ( isset( $instance['images_size'] ) ) {checked( 'big', $instance['images_size'], true );} ?> />
				<?php esc_html_e( 'Big', 'rococo' ) ?>
			</label>
			<label for="<?php echo esc_attr( $this->get_field_id( 'small' ) ) ?>">
				<input type="radio" id="<?php echo esc_attr( $this->get_field_id( 'small' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'images_size' ) ) ?>" value="small" <?php if ( isset( $instance['images_size'] ) ) {checked( 'small', $instance['images_size'], true );} ?> />
				<?php esc_html_e( 'Small', 'rococo' ) ?>
			</label>
		</p>
		<p>
			<label><?php esc_html_e( 'Lines:', 'rococo' ) ?></label> <br/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'first_line' ) ) ?>" style="margin-right: 10px;">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'first_line' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'first_line' ) ) ?>" value="1" <?php if ( isset( $instance['first_line'] ) ) {checked( 1, $instance['first_line'], true );} ?> />
				<?php esc_html_e( 'Top', 'rococo' ); ?>
			</label>
			<label for="<?php echo esc_attr( $this->get_field_id( 'second_line' ) ) ?>">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'second_line' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'second_line' ) ) ?>" value="1" <?php if ( isset( $instance['second_line'] ) ) {checked( 1, $instance['second_line'], true );} ?> />
				<?php esc_html_e( 'Bottom', 'rococo' ); ?>
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'follow_text' ) ) ?>"><?php esc_html_e( 'Label text:', 'rococo' ); ?>
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'follow_text' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'follow_text' ) ) ?>" value="<?php if ( ! empty( $instance['follow_text'] ) ) {echo esc_attr( $instance['follow_text'] );} ?>">
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slider' ) ) ?>">
				<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'slider' ) ) ?>" name="<?php echo esc_attr( $this->get_field_name( 'slider' ) ) ?>" value="1" <?php if ( isset( $instance['slider'] ) ) {checked( 1, $instance['slider'], true );} ?> />
				<?php esc_html_e( 'Slider', 'rococo' ); ?>
			</label>
		</p>
	<?php
	}

	protected function rococo_get_photos( $access_token ) {
		$cached = get_transient( 'footer_instagram_photos' );
		if ( false !== $cached ) {
			return $cached;
		}

		$url              = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . trim( $access_token );
		$rsp_json         = wp_remote_fopen( $url );
		$rsp_obj          = json_decode( $rsp_json, true );
		$rsp_images_array = array();

		if ( 200 == $rsp_obj['meta']['code'] ) {

			if ( ! empty( $rsp_obj['data'] ) ) {

				foreach ( $rsp_obj['data'] as $data ) {
					array_push( $rsp_images_array, array( $data['images']['thumbnail']['url'], $data['link'] ) );
				}

				foreach ( $rsp_images_array as $key => $value ) {
					$rsp_images_array[ $key ] = preg_replace( '/s150x150/', 's320x320', $value );
				}

				$ending_array = array(
					'link'   => $rsp_obj['data'][0]['user']['username'],
					'images' => $rsp_images_array,
				);

				set_transient( 'footer_instagram_photos', $ending_array, 1 * HOUR_IN_SECONDS );

				return $ending_array;
			}
		}
	}
}

add_action( 'widgets_init',
	create_function( '', 'return register_widget( "Rococo_Widget_Footer_Instagram" );' )
);

?>
