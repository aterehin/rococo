<?php
/**
 * Popular posts template
 *
 * @package Nobrand
 */

$args  = array(
	'posts_per_page'      => 3,
	'ignore_sticky_posts' => true,
	'orderby'             => 'comment_count',
	'meta_query'          => array(
		array(
			'key' => '_thumbnail_id',
		),
	),
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) :?>
<div class="popular-posts">
	<div class="row">
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
		<div class="col-sm-4">
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'rococo_medium_thumb' ); ?>
			<div class="popular-post">
				<a class="popular-post__image _dark-overlay" style="background-image: url( '<?php echo esc_url( $image[0] ) ?>' ) " href="<?php echo esc_url( get_permalink() )?>">
				</a>
				<div class="popular-post__content _streamlined">
					<h2 class="popular-post__title"><a href="<?php echo esc_url( get_permalink() ) ?>"><?php the_title(); ?></a></h2>
					<div class="popular-post__date"><a href="<?php echo esc_url( get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ) );?>">
							<?php echo get_the_date();?>
					</a></div>

				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>
</div>
<?php endif; ?>
