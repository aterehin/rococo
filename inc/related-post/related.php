<?php
/**
 * Related posts template
 *
 * @package Nobrand
 * @version 1.1.0
 */

if ( ! function_exists( 'rococo_related_post' ) ) :
function rococo_related_post() {
	global $post;

	$cats_tags_or_taxes = wp_get_object_terms( $post->ID, 'category', array( 'fields' => 'ids' ) );
	$args  = array(
		'posts_per_page' => 3,
		'post__not_in'   => array( $post->ID ),
		'tax_query'      => array(
			array(
				'taxonomy'         => 'category',
				'field'            => 'id',
				'include_children' => false,
				'terms'            => $cats_tags_or_taxes,
				'operator'         => 'IN',
			),
		),
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : ?>
		<div class="related-posts">
			<h4 class="related-posts__title">
				<span class="screen-reader-text"><?php esc_html_e( 'Related Posts', 'rococo' ) ?></span>
				<?php esc_html_e( 'You might also like', 'rococo' ) ?>
			</h4>
			<ul class="related-posts__list">
				<li class="related-posts__list__item">
					<?php $counter = 0;
					while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php $counter ++ ?>
					<div class="related-post">
						<a href="<?php echo esc_url( get_permalink() ) ?>">
							<?php if ( has_post_thumbnail() ) : rococo_post_thumbnail( 'rococo_medium_thumb' );
							else : ?>
								<div class="image-placeholder _in-related-post">
									<img
										src="<?php echo esc_url( get_template_directory_uri() . '/images/bg/img-placeholder.jpg' ) ?>"
										alt="<?php the_title(); ?>"/>
									<?php
									if ( get_post_format() == 'audio' ) {
										echo '<i class="fa fa-file-audio-o"></i>';
									} else if ( get_post_format() == 'video' ) {
										echo '<i class="fa fa-file-video-o"></i>';
									} else if ( get_post_format() == 'gallery' ) {
										echo '<i class="fa fa-file-image-o"></i>';
									} else if ( get_post_format() == 'quote' ) {
										echo '<i class="fa fa-quote-left"></i>';
									} else {
										echo '<i class="fa fa-file-text-o"></i>';
									}
									?>
								</div>
							<?php endif; ?>
						</a>
						<h5 class="post__title">
							<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title() ?></a>
						</h5>
						<?php rococo_post_info( true, false, false ); // Date, Author, Comments. ?>
					</div>
				</li><?php if ( $counter !== $args['posts_per_page'] ) : ?>
				<li class="related-posts__list__item">
					<?php endif ?>
					<?php endwhile;
					wp_reset_postdata();?>
			</ul>
		</div>

	<?php endif;
	wp_reset_postdata();
}
endif;
?>
